/* Notifications */
function shownotificaitons(target) {
    $('#notifications').show();
    $('#tasks').hide();
    $('.item-bullet').addClass("bullet-active");
    $('.item-task').removeClass("notification-item-active");
    $('.item-notification').addClass("notification-item-active");
    $('.i-speaker').addClass("i-speaker-active");
}

function showtasks(target) {
    $('#tasks').show();
    $('#notifications').hide();
    $('.item-bullet').removeClass("bullet-active");
    $('.item-task').addClass("notification-item-active");
    $('.item-notification').removeClass("notification-item-active");
    $('.i-speaker').removeClass("i-speaker-active");
}

function hide(target) {
    document.getElementById(target).style.display = 'none';
    $('.item-bullet').removeClass("bullet-active");
    $('.item-task').removeClass("notification-item-active");
    $('.item-notification').removeClass("notification-item-active");
    $('.i-speaker').removeClass("i-speaker-active");
}

$(document).on('click', function(e) {
    if (!$(e.target).closest(".nav-bar-main").length) {
        $('.notification-container').hide();
        $('.item-bullet').removeClass("bullet-active");
        $('.item-task').removeClass("notification-item-active");
        $('.item-notification').removeClass("notification-item-active");
    }
    e.stopPropagation();
});

/* Date Picker*/
$("#datepicker, #datepicker-1, #datepicker-2, #datepicker-3, #datepicker-4, #datepicker-5, #datepicker-6, #datepicker-7, #datepicker-8, #datepicker-9, #datepicker-10, #datepicker-11, #datepicker-12").datepicker({
    inline: true
});



/* Resize input type text */

(function ($) {
    // DESCRIPTION
    // Use this function if you would like an input element to resize on focus.
    // To use just simply add the attribute "resize-width" with a value that you wish to resize the element to.

    var el;
    var original_width = 2;

    $("input[resize-width]").focus(function () {
        el = $(this);
        original_width = el.width();
        var new_width = el.attr('resize-width');

        // Resize input width.
        el.stop().animate({
            width: new_width
        }, 'fast');
    }).blur(function () {

        // Restore orginal width.
        el.stop().animate({
            width: original_width
        }, 'fast');
    });
})(jQuery);



/* File upload button */

document.getElementById("uploadBtn").onchange = function () {
    document.getElementById("uploadFile").value = this.value;
};


/* BxSlider*/
$(document).ready(function(){
    $('.bxslider').bxSlider({
        pagerCustom: '#bx-pager',
        options: 'fade'
    });
});

