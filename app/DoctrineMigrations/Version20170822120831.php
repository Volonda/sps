<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170822120831 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {

        $this->addSql("
            CREATE OR REPLACE FUNCTION get_close_deadline_task (selected_user_id integer) RETURNS SETOF close_deadline_task_type
            LANGUAGE plpgsql
            AS $$
            DECLARE
              r close_deadline_task_type;
              loop_date date;
              work_days int;
            BEGIN
            
              IF is_day_off(CURRENT_DATE) IS FALSE THEN
                FOR r IN
                SELECT
                    t.id,
                    t.question_id,
                    t.user_id,
                    t.text,
                    t.description,
                    t.start_datetime,
                    t.end_datetime,
                    t.status,
                    t.type,
                    t.deadline_datetime,
                    q.mayor_id
                FROM tasks t
                  JOIN questions q ON q.id = t.question_id
                WHERE
                  (t.user_id = selected_user_id OR q.mayor_id = selected_user_id)
                  AND t.deadline_datetime >= CURRENT_DATE
                  AND t.status != 'completed'::task_status
                LOOP
                  --Задачи у которых срок исполнения сегодня, завтра или через 4 дня
                  IF DATE(r.deadline_datetime) IN (CURRENT_DATE, DATE('tomorrow'), DATE(CURRENT_DATE + INTERVAL '3 days'))  THEN
            
                    RETURN NEXT r;
                  ELSE
                    --Задачи до которых осталось менее 2 или 4 рабочих дня
                    loop_date = DATE(r.deadline_datetime);
                    work_days = 0;
            
                    --Число рабочих дней до завершения
                    WHILE CURRENT_DATE <= loop_date AND work_days <= 4 LOOP
            
                      IF is_day_off(loop_date) IS FALSE THEN
                        work_days = work_days + 1;
                      END IF;
                      loop_date = loop_date - (interval '1' day);
                    END LOOP;
            
                    IF work_days <= 4 AND work_days !=3 THEN
                      RETURN NEXT r;
                    END IF;
            
                  END IF;
            
                END LOOP;
              END IF;
            
            END;
            $$ 
        ");
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->addSql("
            CREATE FUNCTION get_close_deadline_task (selected_user_id integer) RETURNS SETOF close_deadline_task_type
                LANGUAGE plpgsql
            AS $$
            DECLARE
              r close_deadline_task_type;
              loop_date date;
              work_days int;
            BEGIN
            
              IF is_day_off(CURRENT_DATE) IS FALSE THEN
                FOR r IN
                SELECT t.*, q.mayor_id FROM tasks t
                  JOIN questions q ON q.id = t.question_id
                WHERE
                  (t.user_id = selected_user_id OR q.mayor_id = selected_user_id)
                  AND t.deadline_datetime >= CURRENT_DATE
                  AND t.status != 'completed'::task_status
                LOOP
                  --Задачи у которых срок исполнения сегодня и завтра
                  IF DATE(r.deadline_datetime) IN (CURRENT_DATE, DATE('tomorrow'), CURRENT_DATE + INTERVAL '3 days')  THEN
            
                    RETURN NEXT r;
                  ELSE
                    --Задачи до которых осталось менее
                    loop_date = DATE(r.deadline_datetime);
                    work_days = 0;
            
                    --Число рабочих дней до завершения
                    WHILE CURRENT_DATE <= loop_date AND work_days <= 4 LOOP
            
                      IF is_day_off(loop_date) IS FALSE THEN
                        work_days = work_days + 1;
                      END IF;
                      loop_date = loop_date - (interval '1' day);
                    END LOOP;
            
            
                    --Если задача должна быть завершена сегодня, завтра или через 3 дня
                    IF work_days <= 4 AND work_days !=3 THEN
                      RETURN NEXT r;
                    END IF;
            
                  END IF;
            
                END LOOP;
              END IF;
            
            END;
            $$ 
        ");
    }
}
