<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20161221091554 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->addSql('ALTER TABLE meetings
          ADD COLUMN meeting_place_id_updated TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL::timestamp without time zone,
          ADD COLUMN meeting_place_id_last INTEGER NULL,
          ADD COLUMN chairman_id_updated TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL::timestamp without time zone,
          ADD COLUMN chairman_id_last INTEGER NULL,
          ADD COLUMN secretary_id_updated TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL::timestamp without time zone,
          ADD COLUMN secretary_id_last INTEGER NULL,
          ADD COLUMN datetime_updated TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL::timestamp without time zone,
          ADD COLUMN datetime_last TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL::timestamp without time zone,
          ADD COLUMN deleted_at TIMESTAMP(0)  WITHOUT TIME ZONE DEFAULT NULL::timestamp without time zone
        ');

        $this->addSql('ALTER TABLE questions
          ADD COLUMN text_updated TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL::timestamp without time zone,
          ADD COLUMN text_last TEXT NULL,
          ADD COLUMN created TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT CURRENT_TIMESTAMP NOT NULL,
          ADD COLUMN deleted_at TIMESTAMP(0)  WITHOUT TIME ZONE DEFAULT NULL::timestamp without time zone
        ');

        $this->addSql('ALTER TABLE user_questions
          ADD COLUMN created TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT CURRENT_TIMESTAMP NOT NULL
        ');

        $this->addSql('ALTER TABLE meeting_members
            ADD COLUMN created TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT CURRENT_TIMESTAMP NOT NULL,
            ADD COLUMN user_id_last INTEGER NULL,
            ADD COLUMN user_id_updated TIMESTAMP(0)  WITHOUT TIME ZONE DEFAULT NULL::timestamp without time zone,
            ADD COLUMN cause_last TEXT NULL,
            ADD COLUMN cause_updated TIMESTAMP(0)  WITHOUT TIME ZONE DEFAULT NULL::timestamp without time zone,
            ADD COLUMN presence_updated TIMESTAMP(0)  WITHOUT TIME ZONE DEFAULT NULL::timestamp without time zone,
            ADD COLUMN deleted_at TIMESTAMP(0)  WITHOUT TIME ZONE DEFAULT NULL::timestamp without time zone
        ');

        $this->addSql('CREATE TABLE user_questions_deleted(
          question_id INTEGER NOT NULL,
          user_id INTEGER NOT NULL,
          created TIMESTAMP(0)  WITHOUT TIME ZONE DEFAULT NULL::timestamp without time zone,
          deleted_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL DEFAULT CURRENT_TIMESTAMP,
          PRIMARY KEY (question_id, user_id),
          FOREIGN KEY (question_id) REFERENCES questions (id)
          MATCH SIMPLE ON UPDATE NO ACTION ON DELETE CASCADE
        )');

        $this->addSql('CREATE INDEX user_questions_deleted_question_id_index ON user_questions_deleted USING BTREE (question_id)');
        $this->addSql('CREATE INDEX user_questions_deleted_user_id_index ON user_questions_deleted USING BTREE (user_id)');

        $this->addSql('CREATE TABLE question_files_deleted(
          question_id INTEGER NOT NULL,
          file_id INTEGER NOT NULL,
          created TIMESTAMP(0)  WITHOUT TIME ZONE DEFAULT NULL::timestamp without time zone,
          deleted_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL DEFAULT CURRENT_TIMESTAMP,
          PRIMARY KEY (question_id, file_id),
          FOREIGN KEY (question_id) REFERENCES questions (id)
          MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE,
          FOREIGN KEY (file_id) REFERENCES files (id)
          MATCH SIMPLE ON UPDATE NO ACTION ON DELETE NO ACTION
        )');

        $this->addSql('CREATE INDEX question_files_deleted_question_id_index ON question_files_deleted USING BTREE (question_id)');
        $this->addSql('CREATE INDEX question_files_deleted_file_id_index ON question_files_deleted USING BTREE (file_id)');

        $sql=<<<'SQL'
            CREATE FUNCTION meetings_set_updated_field() RETURNS TRIGGER
            AS $$
            BEGIN

              IF OLD.meeting_place_id != NEW.meeting_place_id THEN
                NEW.meeting_place_id_updated = CURRENT_TIMESTAMP;
                NEW.meeting_place_id_last = OLD.meeting_place_id;
              END IF;

              IF OLD.chairman_id != NEW.chairman_id THEN
                NEW.chairman_id_updated = CURRENT_TIMESTAMP;
                NEW.chairman_id_last = OLD.chairman_id;
              END IF;

              IF OLD.secretary_id != NEW.secretary_id THEN
                NEW.secretary_id_updated = CURRENT_TIMESTAMP;
                NEW.secretary_id_last = OLD.secretary_id;
              END IF;

              IF OLD.datetime != NEW.datetime THEN
                NEW.datetime_updated = CURRENT_TIMESTAMP;
                NEW.datetime_last = OLD.datetime;
              END IF;

              RETURN NEW;

            END;
            $$ LANGUAGE plpgsql;

SQL;

        $this->addSql($sql);


        $sql=<<<'SQL'
            CREATE FUNCTION questions_set_updated_field() RETURNS TRIGGER
            AS $$
            BEGIN

              IF OLD.text != NEW.text THEN
                NEW.text_updated = CURRENT_TIMESTAMP;
                NEW.text_last = OLD.text;
              END IF;

              RETURN NEW;

            END;
            $$ LANGUAGE plpgsql;

SQL;

        $this->addSql($sql);

        $sql=<<<'SQL'
            CREATE FUNCTION meeting_members_set_updated_field() RETURNS TRIGGER
            AS $$
            BEGIN

              IF OLD.user_id != NEW.user_id THEN
                NEW.user_id_updated = CURRENT_TIMESTAMP;
                NEW.user_id_last = OLD.user_id;
              END IF;

              IF OLD.cause != NEW.cause THEN
                NEW.cause_updated = CURRENT_TIMESTAMP;
                NEW.cause_last = OLD.cause;
              END IF;

               IF OLD.presence != NEW.presence THEN
                NEW.presence_updated = CURRENT_TIMESTAMP;
              END IF;

              RETURN NEW;

            END;
            $$ LANGUAGE plpgsql;

SQL;

        $this->addSql($sql);

        $sql=<<<'SQL'
            CREATE FUNCTION insert_user_questions_deleted() RETURNS TRIGGER
            AS $$
            BEGIN

              INSERT INTO user_questions_deleted(user_id, question_id, created) VALUES (OLD.user_id, OLD.question_id, OLD.created );

              RETURN NEW;

            END;
            $$ LANGUAGE plpgsql;

SQL;

        $this->addSql($sql);

        $sql=<<<'SQL'
            CREATE FUNCTION insert_question_files_deleted() RETURNS TRIGGER
            AS $$
            BEGIN

              INSERT INTO question_files_deleted(file_id, question_id, created) VALUES (OLD.file_id, OLD.question_id, (SELECT f.created FROM files f WHERE f.id = OLD.file_id ));

              RETURN NEW;

            END;
            $$ LANGUAGE plpgsql;

SQL;

        $this->addSql($sql);


        $this->addSql('
            CREATE TRIGGER field_updated
                BEFORE UPDATE ON meetings FOR EACH ROW EXECUTE PROCEDURE meetings_set_updated_field()');

        $this->addSql('
            CREATE TRIGGER field_updated
                BEFORE UPDATE ON questions FOR EACH ROW EXECUTE PROCEDURE questions_set_updated_field()');

        $this->addSql('
            CREATE TRIGGER field_updated
                BEFORE UPDATE ON meeting_members FOR EACH ROW EXECUTE PROCEDURE meeting_members_set_updated_field()');

        $this->addSql('
            CREATE TRIGGER row_deleted
                AFTER DELETE ON user_questions FOR EACH ROW EXECUTE PROCEDURE insert_user_questions_deleted()');

        $this->addSql('
            CREATE TRIGGER row_deleted
                AFTER DELETE ON question_files FOR EACH ROW EXECUTE PROCEDURE insert_question_files_deleted()');

        $this->addSql('CREATE TABLE meeting_user_notifications_history(
            id BIGSERIAL PRIMARY KEY NOT NULL,
            meeting_id INTEGER,
            user_id INTEGER,
            created TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT CURRENT_TIMESTAMP NOT NULL,
            FOREIGN KEY (meeting_id) REFERENCES meetings (id)
            MATCH SIMPLE ON UPDATE NO ACTION ON DELETE NO ACTION
        )');

        $this->addSql('CREATE INDEX meeting_user_notifications_history_user_id_ix ON meeting_user_notifications_history (user_id);');

        $this->addSql('DROP view user_meetings_view');
        $this->addSql("
            CREATE VIEW user_meetings_view AS SELECT mm.user_id, mm.meeting_id, 'member'::user_meeting_status status FROM meeting_members mm WHERE mm.deleted_at IS NULL
                UNION
                SELECT m1.chairman_id user_id, m1.id  meeting_id, 'chairman'::user_meeting_status status FROM meetings m1 WHERE m1.chairman_id IS NOT NULL AND m1.deleted_at IS NULL
                UNION
                SELECT m2.secretary_id user_id, m2.id  meeting_id, 'secretary'::user_meeting_status status FROM meetings m2 WHERE m2.secretary_id IS NOT NULL AND m2.deleted_at IS NULL
                UNION
                SELECT q.mayor_id user_id, q.meeting_id, 'mayor'::user_meeting_status status FROM questions q WHERE q.mayor_id IS NOT NULL AND q.deleted_at IS NULL
                UNION
                SELECT t.user_id, q.meeting_id, 'user_task'::user_meeting_status status FROM tasks t JOIN questions q ON q.id = t.question_id WHERE t.user_id IS NOT NULL
                UNION
                SELECT uq.user_id, q.meeting_id, 'reporter'::user_meeting_status status FROM user_questions uq JOIN questions q ON q.id = uq.question_id WHERE uq.user_id IS NOT NULL AND q.deleted_at IS NULL
            ;
        ");

        $this->addSql("CREATE VIEW questions_active AS (SELECT * FROM questions WHERE deleted_at IS NULL)");
        $this->addSql("CREATE VIEW meeting_members_active AS (SELECT * FROM meeting_members WHERE deleted_at IS NULL)");
        $this->addSql("CREATE VIEW meetings_active AS (SELECT * FROM meetings WHERE deleted_at IS NULL)");
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->addSql('DROP view user_meetings_view');
        $this->addSql("
            CREATE VIEW user_meetings_view AS SELECT mm.user_id, mm.meeting_id, 'member'::user_meeting_status status FROM meeting_members mm
                UNION
                SELECT m1.chairman_id user_id, m1.id  meeting_id, 'chairman'::user_meeting_status status FROM meetings m1 WHERE m1.chairman_id IS NOT NULL
                UNION
                SELECT m2.secretary_id user_id, m2.id  meeting_id, 'secretary'::user_meeting_status status FROM meetings m2 WHERE m2.secretary_id IS NOT NULL
                UNION
                SELECT q.mayor_id user_id, q.meeting_id, 'mayor'::user_meeting_status status FROM questions q WHERE q.mayor_id IS NOT NULL
                UNION
                SELECT t.user_id, q.meeting_id, 'user_task'::user_meeting_status status FROM tasks t JOIN questions q ON q.id = t.question_id WHERE t.user_id IS NOT NULL
                UNION
                SELECT uq.user_id, q.meeting_id, 'reporter'::user_meeting_status status FROM user_questions uq JOIN questions q ON q.id = uq.question_id WHERE uq.user_id IS NOT NULL
            ;
        ");


        $this->addSql("DROP VIEW questions_active");
        $this->addSql("DROP VIEW meeting_members_active");
        $this->addSql("DROP VIEW meetings_active");

        $this->addSql('ALTER TABLE meetings
            DROP COLUMN meeting_place_id_updated,
            DROP COLUMN meeting_place_id_last,
            DROP COLUMN chairman_id_updated,
            DROP COLUMN chairman_id_last,
            DROP COLUMN secretary_id_updated,
            DROP COLUMN secretary_id_last,
            DROP COLUMN datetime_updated,
            DROP COLUMN datetime_last,
            DROP COLUMN deleted_at
        ');

        $this->addSql('ALTER TABLE questions
            DROP COLUMN text_updated,
            DROP COLUMN text_last,
            DROP COLUMN created,
            DROP COLUMN deleted_at
        ');

        $this->addSql('ALTER TABLE meeting_members
          DROP COLUMN created,
          DROP COLUMN user_id_last,
          DROP COLUMN user_id_updated,
          DROP COLUMN cause_last,
          DROP COLUMN cause_updated,
          DROP COLUMN presence_updated,
          DROP COLUMN deleted_at
        ');

        $this->addSql('ALTER TABLE user_questions DROP COLUMN created');

        $this->addSql('DROP TRIGGER field_updated ON meetings');
        $this->addSql('DROP FUNCTION meetings_set_updated_field()');

        $this->addSql('DROP TRIGGER field_updated ON questions');
        $this->addSql('DROP FUNCTION questions_set_updated_field()');

        $this->addSql('DROP TRIGGER field_updated ON meeting_members');
        $this->addSql('DROP FUNCTION meeting_members_set_updated_field()');

        $this->addSql('DROP TRIGGER row_deleted ON user_questions');
        $this->addSql('DROP FUNCTION insert_user_questions_deleted()');

        $this->addSql('DROP TRIGGER row_deleted ON question_files');
        $this->addSql('DROP FUNCTION insert_question_files_deleted()');

        $this->addSql('DROP TABLE meeting_user_notifications_history');

        $this->addSql('DROP TABLE user_questions_deleted');
        $this->addSql('DROP TABLE question_files_deleted');

    }
}
