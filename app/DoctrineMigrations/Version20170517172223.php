<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170517172223 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->addSql('ALTER TABLE user_questions_deleted DROP CONSTRAINT user_questions_deleted_pkey');
        $this->addSql('ALTER TABLE user_questions_deleted ADD CONSTRAINT user_questions_deleted_pkey PRIMARY KEY (question_id,user_id, created)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->addSql('ALTER TABLE user_questions_deleted DROP CONSTRAINT user_questions_deleted_pkey');
        $this->addSql('ALTER TABLE user_questions_deleted ADD CONSTRAINT user_questions_deleted_pkey PRIMARY KEY (question_id,user_id)');
    }
}
