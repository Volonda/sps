<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170116113116 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->addSql('DROP MATERIALIZED VIEW users;');

        $sql = <<<'SQL'

        CREATE MATERIALIZED VIEW users (id, position, name, email, username, status, roles, department, department_visibility, department_parent, sort_order)
          AS SELECT DISTINCT ON (w.ad)
              w.id,
              w.position,
              w.name,
              (
                CASE w.sromail = '' OR w.sromail IS NULL
                WHEN TRUE THEN w.email
                  ELSE w.sromail
                END
              ) email,
              w.ad username,
              'enabled'::user_status_type status,
              ur.roles,
              id.name department,
              ( CASE WHEN w.departament_status = '1' THEN FALSE ELSE TRUE END ) department_visibility,
              id.name_parent,
              w."order" sort_order
             FROM intranet.workers w
                LEFT JOIN user_roles ur ON ur.user_id = w.id
                LEFT JOIN intranet.intranet_departments id ON id.id = w.department
             WHERE w.ad !='' ORDER BY w.ad ASC, w.vis DESC;

SQL;
        $this->addSql($sql);

        $this->addSql('CREATE UNIQUE INDEX users_uix ON users (username);');

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->addSql('DROP MATERIALIZED VIEW users;');

        $sql = <<<'SQL'

         CREATE MATERIALIZED VIEW users (id, position, name, email, username, status, roles, department, department_visibility, department_parent, sort_order)
          AS SELECT DISTINCT ON (w.ad)
              w.id,
              w.position,
              w.name,
              (
                CASE w.sromail = '' OR w.sromail IS NULL
                WHEN TRUE THEN w.email
                  ELSE w.sromail
                END
              ) email,
              w.ad username,
              (
                CASE w.vis
                  WHEN 0 THEN 'disabled'
                  WHEN 1 THEN 'enabled'
                END
              ) status,
              ur.roles,
              id.name department,
              ( CASE WHEN w.departament_status = '1' THEN FALSE ELSE TRUE END ) department_visibility,
              id.name_parent,
              w."order" sort_order
             FROM intranet.workers w
                LEFT JOIN user_roles ur ON ur.user_id = w.id
                LEFT JOIN intranet.intranet_departments id ON id.id = w.department
             WHERE w.ad !='' ORDER BY w.ad ASC, w.vis DESC;
SQL;

        $this->addSql($sql);

        $this->addSql('CREATE UNIQUE INDEX users_uix ON users (username);');
    }
}
