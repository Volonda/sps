CREATE TYPE task_status  AS ENUM ('created','processing', 'completed', 'approved', 'testing');
CREATE TYPE meeting_status  AS ENUM ('created', 'opened', 'closed');
CREATE TYPE user_meeting_status  AS ENUM ('member', 'chairman', 'secretary','mayor', 'user_task', 'reporter');
CREATE TYPE email_status  AS ENUM ('created', 'success', 'error');
CREATE TYPE task_type  AS ENUM ('finite', 'repeat');
CREATE TYPE task_notification_type  AS ENUM ('single', 'repeat');
CREATE TYPE task_repeat_period_type  AS ENUM ('day', 'week', 'month');
CREATE TYPE user_history_type  AS ENUM ('login');
CREATE TYPE user_status_type  AS ENUM ('enabled', 'disabled');
CREATE SEQUENCE settings_id_seq MINVALUE 0 MAXVALUE 1 START 1;


