--Представление пользователи
CREATE TABLE public.user_roles
(
  id SERIAL PRIMARY KEY,
  user_id int NOT NULL,
  roles text
);

CREATE SCHEMA intranet;
CREATE EXTENSION mysql_fdw WITH SCHEMA intranet;
CREATE SERVER intranet_mysql_server FOREIGN DATA WRAPPER mysql_fdw OPTIONS (host :intranetDbHost, port :intranetDbPort);
CREATE USER MAPPING FOR :dbUser SERVER intranet_mysql_server OPTIONS (username :intranetDbUser, password :intranetDbPassword);

CREATE FOREIGN TABLE intranet.workers(
  id INTEGER,
  department INTEGER,
  position TEXT,
  name TEXT,
  sromail TEXT,
  email TEXT,
  ad TEXT,
  vis INTEGER,
  "order" INTEGER
) SERVER intranet_mysql_server OPTIONS (dbname :intranetDbName, table_name 'workers');

CREATE FOREIGN TABLE intranet.intranet_departments(
  id INTEGER,
  name TEXT,
  name_parent TEXT
) SERVER intranet_mysql_server OPTIONS (dbname :intranetDbName, table_name 'intranet_departments');

CREATE MATERIALIZED VIEW users (id, position, name, email, username, status, roles, department, department_parent, sort_order)
  AS SELECT DISTINCT ON (w.ad)
      w.id,
      w.position,
      w.name,
      (
        CASE w.sromail = '' OR w.sromail IS NULL
        WHEN TRUE THEN w.email
          ELSE w.sromail
        END
      ) email,
      w.ad username,
      (
        CASE w.vis
          WHEN 0 THEN 'disabled'
          WHEN 1 THEN 'enabled'
        END
      ) status,
      ur.roles,
      id.name department,
      id.name_parent,
      w."order" sort_order
     FROM intranet.workers w
        LEFT JOIN user_roles ur ON ur.user_id = w.id
        LEFT JOIN intranet.intranet_departments id ON id.id = w.department
     WHERE w.ad !='' ORDER BY w.ad ASC, w.vis DESC;
CREATE UNIQUE INDEX users_uix ON users (username);

-- Создадим индексы, т.к на вьюху невозможно создать FK
CREATE UNIQUE INDEX user_roles_user_id_uindex ON public.user_roles (user_id);
CREATE INDEX task_user_id_index ON public.tasks (user_id);
CREATE INDEX tasks_planed_user_user_id_index ON public.tasks_planed_user (user_id);
CREATE INDEX user_digest_history_user_id_index ON public.user_digest_history (user_id);
CREATE INDEX user_questions_user_id_index ON public.user_questions (user_id);
CREATE INDEX groups_users_user_id_index ON public.groups_users (user_id);
CREATE INDEX meeting_members_user_id_index ON public.meeting_members (user_id);

-- Представления для отображения отношения юзера к совещанию
CREATE VIEW user_meetings_view AS SELECT mm.user_id, mm.meeting_id, 'member'::user_meeting_status status FROM meeting_members mm
    UNION
    SELECT m1.chairman_id user_id, m1.id  meeting_id, 'chairman'::user_meeting_status status FROM meetings m1 WHERE m1.chairman_id IS NOT NULL
    UNION
    SELECT m2.secretary_id user_id, m2.id  meeting_id, 'secretary'::user_meeting_status status FROM meetings m2 WHERE m2.secretary_id IS NOT NULL
    UNION
    SELECT q.mayor_id user_id, q.meeting_id, 'mayor'::user_meeting_status status FROM questions q WHERE q.mayor_id IS NOT NULL
    UNION
    SELECT t.user_id, q.meeting_id, 'user_task'::user_meeting_status status FROM tasks t JOIN questions q ON q.id = t.question_id WHERE t.user_id IS NOT NULL
    UNION
    SELECT uq.user_id, q.meeting_id, 'reporter'::user_meeting_status status FROM user_questions uq JOIN questions q ON q.id = uq.question_id WHERE uq.user_id IS NOT NULL
;

--Функция определения выходного дня
CREATE OR REPLACE FUNCTION public.is_day_off(date date)
  RETURNS bool
AS
$BODY$
  DECLARE is_day_off BOOLEAN;
BEGIN

  is_day_off = TRUE;

  IF date_part('dow', date) IN (1,2,3,4,5) AND NOT EXISTS(
      SELECT 1 FROM work_calendar c JOIN work_calendar_change cc ON cc.calendar_id = c.id WHERE c.year = date_part('year', date) AND cc.day = DATE_PART('doy', date) AND cc.is_day_off IS TRUE LIMIT 1
  ) THEN
    is_day_off = FALSE;
  ELSE

    IF EXISTS(
        SELECT 1 FROM work_calendar c JOIN work_calendar_change cc ON cc.calendar_id = c.id WHERE c.year = date_part('year', date) AND cc.day = DATE_PART('doy', date) AND cc.is_day_off IS FALSE LIMIT 1
    )
    THEN
      is_day_off = FALSE;
    ELSE
      is_day_off = TRUE;
    END IF;
  END IF;

  RETURN is_day_off;
END;
$BODY$
LANGUAGE plpgsql STABLE;


-- Функция получения задач до которых менее 2 рабочих дней
CREATE TYPE close_deadline_task_type as
(
  id INTEGER,
  question_id INTEGER,
  user_id INTEGER,
  text TEXT,
  description TEXT,
  start_datetime TIMESTAMP(0) WITHOUT TIME ZONE,
  end_datetime TIMESTAMP(0) WITHOUT TIME ZONE,
  status TASK_STATUS,
  type TASK_TYPE,
  deadline_datetime TIMESTAMP(0) WITHOUT TIME ZONE,
  mayor_id INTEGER
);


CREATE OR REPLACE FUNCTION public.get_close_deadline_task(selected_user_id int4)
  RETURNS SETOF close_deadline_task_type
AS
$BODY$
DECLARE
  r close_deadline_task_type;
  loop_date date;
  work_days int;
BEGIN

  IF is_day_off(CURRENT_DATE) IS FALSE THEN
    FOR r IN
    SELECT t.*, q.mayor_id FROM tasks t
      JOIN questions q ON q.id = t.question_id
    WHERE
      (t.user_id = selected_user_id OR q.mayor_id = selected_user_id)
      AND t.deadline_datetime >= CURRENT_DATE
      AND t.status != 'completed'::task_status
    LOOP
      --Задачи у которых срок исполнения сегодня и завтра
      IF DATE(r.deadline_datetime) IN (CURRENT_DATE, DATE('tomorrow'), CURRENT_DATE + INTERVAL '3 days')  THEN

        RETURN NEXT r;
      ELSE
        --Задачи до которых осталось менее
        loop_date = DATE(r.deadline_datetime);
        work_days = 0;

        --Число рабочих дней до завершения
        WHILE CURRENT_DATE <= loop_date AND work_days <= 4 LOOP

          IF is_day_off(loop_date) IS FALSE THEN
            work_days = work_days + 1;
          END IF;
          loop_date = loop_date - (interval '1' day);
        END LOOP;


        --Если задача должна быть завершена сегодня, завтра или через 3 дня
        IF work_days <= 4 AND work_days !=3 THEN
          RETURN NEXT r;
        END IF;

      END IF;

    END LOOP;
  END IF;

END;
$BODY$
LANGUAGE plpgsql VOLATILE;

--Нумерация встреч при добавлении закрытого совещания
CREATE OR REPLACE FUNCTION before_meeting_insert()
  RETURNS trigger AS
$BODY$
BEGIN

  IF NEW.status = 'closed'::public.meeting_status THEN

    SELECT COALESCE(MAX(m.num),0) + 1 FROM meetings m INTO NEW.num;

  END IF;

  RETURN NEW;

END;
$BODY$
LANGUAGE plpgsql VOLATILE;


--Нумерация задачи после закрытия совещания
CREATE OR REPLACE FUNCTION before_update_meetings()
  RETURNS trigger AS
$BODY$
BEGIN
  IF
  OLD.status = 'opened'::public.meeting_status
  AND NEW.status = 'closed'::public.meeting_status
  AND OLD.num IS NULL
  THEN
    --meeting num
    SELECT (COALESCE(MAX(num),0) + 1) num FROM meetings INTO  NEW.num;

  END IF;

  RETURN NEW;
END;
$BODY$
LANGUAGE plpgsql VOLATILE;

--Нумерация встречи при добавлении закрытой встречи
CREATE TRIGGER before_meeting_insert_trigger
BEFORE INSERT
ON meetings
FOR EACH ROW
EXECUTE PROCEDURE before_meeting_insert();

--Нумерация встречи при закрытии встречи
CREATE TRIGGER before_update_meeting_trigger
BEFORE UPDATE
ON meetings
FOR EACH ROW
EXECUTE PROCEDURE before_update_meetings();