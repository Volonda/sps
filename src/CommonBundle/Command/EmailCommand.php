<?php
namespace CommonBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputDefinition;
use CommonBundle\Entity\Email;
use CommonBundle\Manager\EmailManager;

class EmailCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('email:send')
            ->setDescription('Sending emails')
            ->setDefinition(
                new InputDefinition(array(
                    new InputOption('limit', 'l', InputOption::VALUE_REQUIRED),
                ))
            )
            ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var $manager EmailManager */
        $manager = $this->getContainer()->get('common.manager.email_manager');
        $limit = $input->getOption('limit');
        $emails = $manager->getForSend($limit);

        /** @var  $email Email */
        foreach($emails as $email) {
            try {
                $message = \Swift_Message::newInstance()
                    ->setSubject($email->getSubject())
                    ->setFrom($email->getFromEmail())
                    ->setTo($email->getToEmail())
                    ->setBody($email->getBody(), 'text/html');

                $result = $this->getContainer()->get('swiftmailer.mailer.synchronic')->send($message);

                if ($result) {
                    $manager->setSuccess($email);
                } else {

                    throw new \Exception();
                }

            } catch(\Exception $e) {

                $manager->setError($email);
                $this->getContainer()->get('logger')->addError($e->getMessage());

            }
        }

    }
};