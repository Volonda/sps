<?php
namespace CommonBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use CommonBundle\Manager\TaskManager;

class TaskRepeatProlongationCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('task:repeat:prolongation')
            ->setDescription('Generate new deadline for repeat task')
            ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var TaskManager $manager*/
        $manager = $this->getContainer()->get('common.manager.task_manager');

        $manager->prolongation();
    }

};