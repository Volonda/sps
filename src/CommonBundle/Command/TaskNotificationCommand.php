<?php
namespace CommonBundle\Command;

use CommonBundle\Entity\TaskNotificationHistory;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputOption;
use CommonBundle\Entity\TaskSingleNotification;
use CommonBundle\Entity\TaskRepeatNotification;
use CommonBundle\Manager\EmailManager;
use CommonBundle\Manager\TaskNotificationManager;
use CommonBundle\Manager\TaskNotificationHistoryManager;
use Symfony\Component\Console\Input\InputArgument;
use CommonBundle\Entity\Task;

class TaskNotificationCommand extends ContainerAwareCommand
{
    /** @var EmailManager */
    private $emailManager;

    /** @var TaskNotificationManager */
    private $notificationManager;

    /** @var TaskNotificationHistoryManager */
    private $notificationHistoryManager;

    protected function configure()
    {
        $this
            ->setName('task:notification')
            ->setDescription('Generate emails for task notifications')
            ->addArgument('type', InputArgument::REQUIRED, 'Notification type')
            ->addOption('limit','l', InputOption::VALUE_OPTIONAL)
            ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $container = $this->getContainer();
        $context = $container->get('router')->getContext();
        $context->setHost($container->getParameter('base_host'));

        $this->emailManager = $this->getContainer()->get('common.manager.email_manager');
        $this->notificationManager = $this->getContainer()->get('common.manager.task_notification_manager');
        $this->notificationHistoryManager = $this->getContainer()->get('common.manager.task_notification_history_manager');

        $limit = $input->getOption('limit');
        $type = $input->getArgument('type');

        if ($type == TaskNotificationHistory::TASK_HISTORY_NOTIFICATION_TYPE_SINGLE) {

            $this->singleTaskNotifications($limit);

        }elseif ($type == TaskNotificationHistory::TASK_HISTORY_NOTIFICATION_TYPE_REPEAT) {

            $this->repeatTaskNotifications($limit);

        }

    }

    /**
     * Уведомления с датой
     *
     * @param bool $limit
    */
    protected function singleTaskNotifications($limit = false)
    {
        $singleNotifications = $this->notificationManager->getTodaySingleList($limit);

        /** @var TaskSingleNotification $singleNotification*/
        foreach($singleNotifications as $singleNotification) {
            $task = $singleNotification->getTask();

            $this->emailManager->createTaskNotificationEmail($task, $task->getUser());
            $this->notificationHistoryManager->add(
                $singleNotification->getTask(),
                TaskNotificationHistory::TASK_HISTORY_NOTIFICATION_TYPE_SINGLE
            );
        }
    }

    /**
     * Уведомления по дням недели
     *
     * @param bool $limit
     */
    private function repeatTaskNotifications($limit = false)
    {
        $repeatNotifications = $this->notificationManager->getTodayRepeatList($limit);

        /** @var TaskRepeatNotification $repeatNotification*/
        foreach($repeatNotifications as $repeatNotification) {
            $task = $repeatNotification->getTask();

            $this->emailManager->createTaskNotificationEmail($task, $task->getUser());

            $this->notificationHistoryManager->add(
                $repeatNotification->getTask(),
                TaskNotificationHistory::TASK_HISTORY_NOTIFICATION_TYPE_REPEAT
            );
        }
    }

};