<?php
namespace CommonBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputOption;
use CommonBundle\Manager\EmailManager;
use CommonBundle\Manager\DigestManager;
use UserBundle\Entity\User;

class DigestCommand extends ContainerAwareCommand
{
    /** @var EmailManager */
    private $emailManager;

    /** @var DigestManager */
    private $digestManager;

    protected function configure()
    {
        $this
            ->setName('digest:daily')
            ->setDescription('Send digest')
            ->addOption('limit','l', InputOption::VALUE_OPTIONAL)
            ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $container = $this->getContainer();
        $context = $container->get('router')->getContext();
        $context->setHost($container->getParameter('base_host'));

        $this->emailManager = $this->getContainer()->get('common.manager.email_manager');
        $this->digestManager = $this->getContainer()->get('common.manager.digest_manager');

        $limit = $input->getOption('limit');

        $this->dailyDigest($limit);
    }


    protected function dailyDigest($limit = false)
    {
        $users = $this->digestManager->getDailyUsers($limit);

        /**@var User $user*/
        foreach($users as $user) {
            $tasks = $this->digestManager->getCloseDeadLineTasks($user);
            $this->emailManager->createDailyDigestEmail($user, $tasks);

            $this->digestManager->createUserDigestHistory($user);
        }

    }
};