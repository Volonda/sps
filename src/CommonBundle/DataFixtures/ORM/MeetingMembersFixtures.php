<?php

namespace CommonBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use CommonBundle\Entity\MeetingMember;

class MeetingMemberFixtures extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        //meeting 1
        $meetingMember1 = new MeetingMember();
        $meetingMember1->setMeeting($this->getReference('meeting-1'));
        $meetingMember1->setUser($this->getReference('user-1'));
        $meetingMember1->setPresence(true);
        $manager->persist($meetingMember1);

        $meetingMember2 = new MeetingMember();
        $meetingMember2->setMeeting($this->getReference('meeting-1'));
        $meetingMember2->setUser($this->getReference('user-2'));
        $meetingMember2->setPresence(true);
        $manager->persist($meetingMember2);

        //meeting 2
        $meetingMember3 = new MeetingMember();
        $meetingMember3->setMeeting($this->getReference('meeting-2'));
        $meetingMember3->setUser($this->getReference('user-2'));
        $meetingMember3->setPresence(true);
        $manager->persist($meetingMember3);

        $meetingMember4 = new MeetingMember();
        $meetingMember4->setMeeting($this->getReference('meeting-2'));
        $meetingMember4->setUser($this->getReference('user-1'));
        $meetingMember4->setPresence(true);
        $manager->persist($meetingMember4);

        //meeting 3
        $meetingMember5 = new MeetingMember();
        $meetingMember5->setMeeting($this->getReference('meeting-3'));
        $meetingMember5->setUser($this->getReference('user-3'));
        $meetingMember5->setPresence(true);
        $manager->persist($meetingMember5);

        //meeting 4
        $meetingMember6 = new MeetingMember();
        $meetingMember6->setMeeting($this->getReference('meeting-3'));
        $meetingMember6->setUser($this->getReference('user-2'));
        $meetingMember6->setCause('Причина 4');
        $meetingMember6->setPresence(false);
        $manager->persist($meetingMember6);

        $manager->flush();

        $this->addReference('meeting-member-1', $meetingMember1);
        $this->addReference('meeting-member-2', $meetingMember2);
        $this->addReference('meeting-member-3', $meetingMember3);
        $this->addReference('meeting-member-4', $meetingMember4);
        $this->addReference('meeting-member-5', $meetingMember5);
        $this->addReference('meeting-member-6', $meetingMember6);
    }

    public function getOrder()
    {
        return 7;
    }
}