<?php

namespace CommonBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use CommonBundle\Entity\MeetingPlace;

class MeetingPlaceFixtures extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $meetingPlace1 = new MeetingPlace();
        $meetingPlace1->setTitle('Место 1');
        $manager->persist($meetingPlace1);

        $meetingPlace2 = new MeetingPlace();
        $meetingPlace2->setTitle('г.Санкт-Петербург, ул.Чайковского, д.44, эт.3, каб.20');
        $manager->persist($meetingPlace2);

        $manager->flush();

        $this->addReference('meeting-place-1', $meetingPlace1);
        $this->addReference('meeting-place-2', $meetingPlace2);

    }

    public function getOrder()
    {
        return 2;
    }
}