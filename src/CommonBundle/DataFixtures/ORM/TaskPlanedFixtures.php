<?php

namespace CommonBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use CommonBundle\Entity\TaskPlaned;
use CommonBundle\Entity\Question;
use UserBundle\Entity\User;

class TaskPlanedFixtures extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        /**
         * @var Question $question1
         * @var Question $question2
         * @var Question $question3
         * @var Question $question4
         * @var Question $question5
         * @var User $user1
         * @var User $user2
         * @var User $user3
         *
        */
        $question1 = $this->getReference('question-1');
        $question2 = $this->getReference('question-2');
        $question3 = $this->getReference('question-3');
        $question4 = $this->getReference('question-4');
        $question5 = $this->getReference('question-5');

        $user1 = $this->getReference('user-1');
        $user2 = $this->getReference('user-2');
        $user3 = $this->getReference('user-3');


        $taskPlaned1 = new TaskPlaned();
        $taskPlaned1->setQuestion($question1);
        $taskPlaned1->setDeadlineDatetime(new \DateTime('+1 years'));
        $taskPlaned1->setText('Задача 1');
        $taskPlaned1->addUser($user1);
        $taskPlaned1->setType(TaskPlaned::TASK_PLANED_TYPE_FINITE);
        $manager->persist($taskPlaned1);

        $taskPlaned2 = new TaskPlaned();
        $taskPlaned2->setQuestion($question2);
        $taskPlaned2->setDeadlineDatetime(new \DateTime('-1 years'));
        $taskPlaned2->setText('Задача 2');
        $taskPlaned2->addUser($user2);
        $taskPlaned2->setType(TaskPlaned::TASK_PLANED_TYPE_FINITE);
        $manager->persist($taskPlaned2);

        $taskPlaned3 = new TaskPlaned();
        $taskPlaned3->setQuestion($question3);
        $taskPlaned3->setDeadlineDatetime(new \DateTime('+1 days'));
        $taskPlaned3->setText('Задача 3');
        $taskPlaned3->addUser($user3);
        $taskPlaned3->setType(TaskPlaned::TASK_PLANED_TYPE_FINITE);
        $manager->persist($taskPlaned3);

        $taskPlaned4 = new TaskPlaned();
        $taskPlaned4->setQuestion($question4);
        $taskPlaned4->setDeadlineDatetime(new \DateTime('+30 days'));
        $taskPlaned4->setText('Задача 4');
        $taskPlaned4->addUser($user1);
        $taskPlaned4->setType(TaskPlaned::TASK_PLANED_TYPE_FINITE);
        $manager->persist($taskPlaned4);

        $taskPlaned5 = new TaskPlaned();
        $taskPlaned5->setQuestion($question5);
        $taskPlaned5->setDeadlineDatetime(new \DateTime('+30 days'));
        $taskPlaned5->setText('Задача 5');
        $taskPlaned5->addUser($user2);
        $taskPlaned5->setType(TaskPlaned::TASK_PLANED_TYPE_FINITE);
        $manager->persist($taskPlaned5);

        $manager->flush();

        $this->addReference('task-planed-1', $taskPlaned1);
        $this->addReference('task-planed-2', $taskPlaned2);
        $this->addReference('task-planed-3', $taskPlaned3);
        $this->addReference('task-planed-4', $taskPlaned4);
        $this->addReference('task-planed-5', $taskPlaned5);

    }

    public function getOrder()
    {
        return 5;
    }
}