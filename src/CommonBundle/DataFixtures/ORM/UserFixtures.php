<?php

namespace CommonBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use CommonBundle\Entity\Meeting;
use CommonBundle\Entity\MeetingPlace;

class UserFixtures extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $user1 = $manager->getRepository('UserBundle:User')->findOneBy(['username' => 'Belyakova Veronika' ]);
        $user2 = $manager->getRepository('UserBundle:User')->findOneBy(['username' => 'Utkina.Natalia' ]);
        $user3 = $manager->getRepository('UserBundle:User')->findOneBy(['username' => 'karahtanov.aleksey' ]);

        $this->addReference('user-1', $user1);
        $this->addReference('user-2', $user2);
        $this->addReference('user-3', $user3);
    }

    public function getOrder()
    {
        return 1;
    }
}