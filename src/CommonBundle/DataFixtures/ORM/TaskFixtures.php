<?php

namespace CommonBundle\DataFixtures\ORM;

use CommonBundle\Entity\Task;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use CommonBundle\Entity\FiniteTask;
use CommonBundle\Entity\RepeatTask;
use CommonBundle\Entity\Question;
use UserBundle\Entity\User;

class TaskFixtures extends AbstractFixture implements OrderedFixtureInterface
{
    private $finiteTaskCounter = 1;
    private $repeatTaskCounter = 1;

    public function load(ObjectManager $manager)
    {
        /**
         * @var Question $question1
         * @var Question $question2
         * @var Question $question3
         * @var Question $question4
         * @var Question $question5
         * @var User $user1
         * @var User $user2
         * @var User $user3
         *
        */

        $question4 = $this->getReference('question-4');
        $question5 = $this->getReference('question-5');

        $user1 = $this->getReference('user-1');
        $user2 = $this->getReference('user-2');
        $user3 = $this->getReference('user-3');

        ## FiniteTask
        $finiteTask1 = $this->createFiniteTask($user1, $question4, new \DateTime('+1 years'));
        $finiteTask1->setStatus(Task::TASK_STATUS_COMPLETED);
        $manager->persist($finiteTask1);

        $finiteTask2 = $this->createFiniteTask($user2, $question4, new \DateTime('+1 days'), true);
        $finiteTask2->setStatus(Task::TASK_STATUS_APPROVED);
        $manager->persist($finiteTask2);

        $finiteTask3 = $this->createFiniteTask($user3, $question4, new \DateTime('-1 years'));
        $finiteTask3->setStatus(Task::TASK_STATUS_TESTING);
        $manager->persist($finiteTask3);

        $finiteTask4 = $this->createFiniteTask($user1, $question4, new \DateTime('+7 days'));
        $manager->persist($finiteTask4);

        $finiteTask5 = $this->createFiniteTask($user2, $question4, new \DateTime('+7 days'));
        $manager->persist($finiteTask5);

        $finiteTask6 = $this->createFiniteTask($user3, $question4, new \DateTime('+7 days'));
        $manager->persist($finiteTask6);

        ## RepeatTask
        $repeatTask1 = $this->createRepeatTask(
            $user1,
            $question4,
            new \DateTime('+1 days'),
            2,
            RepeatTask::PERIOD_TYPE_DAY
        );
        $manager->persist($repeatTask1);

        $repeatTask2 = $this->createRepeatTask(
            $user1,
            $question4,
            new \DateTime('+1 days'),
            2,
            RepeatTask::PERIOD_TYPE_MOUTH
        );
        $manager->persist($repeatTask2);

        $repeatTask3 = $this->createRepeatTask(
            $user2,
            $question4,
            new \DateTime('+2 days'),
            2,
            RepeatTask::PERIOD_TYPE_MOUTH
        );
        $repeatTask3->setStatus(Task::TASK_STATUS_COMPLETED);
        $manager->persist($repeatTask3);

        $repeatTask4 = $this->createRepeatTask(
            $user3,
            $question4,
            new \DateTime('now'),
            1,
            RepeatTask::PERIOD_TYPE_WEEK
        );
        $manager->persist($repeatTask4);

        $repeatTask5 = $this->createRepeatTask(
            $user1,
            $question5,
            new \DateTime('now'),
            2,
            RepeatTask::PERIOD_TYPE_WEEK
        );
        $manager->persist($repeatTask5);

        $repeatTask6 = $this->createRepeatTask(
            $user2,
            $question5,
            new \DateTime('now'),
            2,
            RepeatTask::PERIOD_TYPE_WEEK
        );
        $repeatTask6->setStatus(Task::TASK_STATUS_COMPLETED);
        $manager->persist($repeatTask6);

        $repeatTask7 = $this->createRepeatTask(
            $user3,
            $question5,
            new \DateTime('+2 days'),
            2,
            RepeatTask::PERIOD_TYPE_DAY
        );
        $manager->persist($repeatTask7);

        $repeatTask8 = $this->createRepeatTask(
            $user1,
            $question5,
            new \DateTime('-1 days'),
            10,
            RepeatTask::PERIOD_TYPE_DAY
        );
        $manager->persist($repeatTask8);

        $repeatTask9 = $this->createRepeatTask(
            $user2,
            $question5,
            new \DateTime('-1 days'),
            10,
            RepeatTask::PERIOD_TYPE_MOUTH
        );
        $manager->persist($repeatTask9);

        $manager->flush();
    }

    /**
     * @param User $user
     * @param Question $question
     * @param \DateTime $deadLine
     * @param bool $private
     * @return FiniteTask
    */
    private function createFiniteTask(User $user, Question $question, \DateTime $deadLine, $private = false)
    {
        $task = new FiniteTask();
        $task->setQuestion($question);
        $task->setDeadlineDatetime($deadLine);
        $task->setText('Задача со сроком ' . $this->finiteTaskCounter);
        $task->setUser($user);

        $this->addReference('finite-task-' . $this->finiteTaskCounter, $task);

        $this->finiteTaskCounter ++;

        return $task;
    }

    private function createRepeatTask(
        User $user,
        Question $question,
        \DateTime $deadLine,
        $period,
        $periodType
    ) {
        $task = new RepeatTask();
        $task->setQuestion($question);
        $task->setText('Постоянная задача ' . $this->repeatTaskCounter);
        $task->setUser($user);
        $task->setDeadlineDatetime($deadLine);
        $task->setPeriod($period);
        $task->setPeriodType($periodType);

        $this->addReference('repeat-task-' . $this->repeatTaskCounter, $task);

        $this->repeatTaskCounter ++;

        return $task;
    }

    public function getOrder()
    {
        return 6;
    }
}