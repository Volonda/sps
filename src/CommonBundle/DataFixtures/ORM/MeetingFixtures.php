<?php

namespace CommonBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use CommonBundle\Entity\Meeting;
use CommonBundle\Entity\MeetingPlace;

class MeetingFixtures extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        /**
         * @var $meetingPlace1 MeetingPlace
         * @var $meetingPlace2 MeetingPlace
        */
        $meetingPlace1 = $this->getReference('meeting-place-1');
        $meetingPlace2 = $this->getReference('meeting-place-2');

        $meeting1 = new Meeting();
        $meeting1->setDatetime(new \DateTime());
        $meeting1->setStatus(Meeting::MEETING_STATUS_OPENED);
        $meeting1->setPlace($meetingPlace1);
        $meeting1->setSecretary($this->getReference('user-2'));
        $meeting1->setChairman($this->getReference('user-1'));
        $manager->persist($meeting1);


        $meeting2 = new Meeting();
        $meeting2->setDatetime(new \DateTime('+1 days'));
        $meeting2->setStatus(Meeting::MEETING_STATUS_CREATED);
        $meeting2->setPlace($meetingPlace2);
        $meeting2->setSecretary($this->getReference('user-2'));
        $meeting2->setChairman($this->getReference('user-1'));
        $manager->persist($meeting2);

        $meeting3 = new Meeting();
        $meeting3->setDatetime(new \DateTime('-1 days'));
        $meeting3->setStatus(Meeting::MEETING_STATUS_CLOSED);
        $meeting3->setPlace($meetingPlace2);
        $meeting3->setSecretary($this->getReference('user-2'));
        $meeting3->setChairman($this->getReference('user-1'));
        $manager->persist($meeting3);

        $manager->flush();

        $this->addReference('meeting-1', $meeting1);
        $this->addReference('meeting-2', $meeting2);
        $this->addReference('meeting-3', $meeting3);

    }

    public function getOrder()
    {
        return 3;
    }
}