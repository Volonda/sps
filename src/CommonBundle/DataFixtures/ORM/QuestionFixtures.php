<?php

namespace CommonBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use CommonBundle\Entity\Question;
use UserBundle\Entity\User;

class QuestionFixtures extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {

      /**
       * @var User $user1
       * @var User $user2
       * @var User $user3
       */

        $user1 = $this->getReference('user-1');
        $user2 = $this->getReference('user-2');
        $user3 = $this->getReference('user-3');

        $question1 = new Question();
        $question1->setStatus(Question::QUESTION_STATUS_CREATED);
        $question1->setAnswer('Ответ 1');
        $question1->setText('Вопрос 1');
        $question1->setMeeting($this->getReference('meeting-1'));
        $question1->setMayor($user1);
        $question1->addUser($user1);
        $question1->addUser($user2);
        $question1->addUser($user3);
        $user2->addQuestion($question1);
        $user3->addQuestion($question1);
        $manager->persist($question1);

        $question2 = new Question();
        $question2->setStatus(Question::QUESTION_STATUS_CREATED);
        $question2->setAnswer('Ответ 2');
        $question2->setText('Вопрос 2');
        $question2->setMeeting($this->getReference('meeting-1'));
        $question2->setMayor($user3);
        $question2->addUser($user2);
        $question2->addUser($user1);
        $user1->addQuestion($question2);
        $user2->addQuestion($question2);
        $manager->persist($question2);

        $question3 = new Question();
        $question3->setStatus(Question::QUESTION_STATUS_CREATED);
        $question3->setAnswer('Ответ 3 - 1');
        $question3->setText('Вопрос 3 -1');
        $question3->setMeeting($this->getReference('meeting-2'));
        $question3->setMayor($user3);
        $question3->addUser($user2);
        $question3->addUser($user1);
        $user3->addQuestion($question3);
        $user2->addQuestion($question3);
        $manager->persist($question3);

        $question4 = new Question();
        $question4->setStatus(Question::QUESTION_STATUS_CREATED);
        $question4->setAnswer('Ответ 3 - 2');
        $question4->setText('Вопрос 3 - 2');
        $question4->setMeeting($this->getReference('meeting-3'));
        $question4->setMayor($user2);
        $question4->addUser($user3);
        $question4->addUser($user1);
        $user3->addQuestion($question4);
        $user1->addQuestion($question4);
        $manager->persist($question4);

        $question5 = new Question();
        $question5->setStatus(Question::QUESTION_STATUS_CREATED);
        $question5->setAnswer('Ответ 3 - 2');
        $question5->setText('Вопрос 3 - 2');
        $question5->setMeeting($this->getReference('meeting-3'));
        $question5->setMayor($user2);
        $question5->addUser($user1);
        $question5->addUser($user3);
        $user3->addQuestion($question5);
        $user2->addQuestion($question5);
        $manager->persist($question5);

        $manager->flush();

        $this->addReference('question-1', $question1);
        $this->addReference('question-2', $question2);
        $this->addReference('question-3', $question3);
        $this->addReference('question-4', $question4);
        $this->addReference('question-5', $question5);
    }

    public function getOrder()
    {
        return 4;
    }
}