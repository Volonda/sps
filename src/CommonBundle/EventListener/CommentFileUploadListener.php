<?php

namespace CommonBundle\EventListener;

use Symfony\Component\HttpFoundation\File\UploadedFile;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use CommonBundle\Entity\TaskLog;
use CommonBundle\FileUploader\CommentFileUploader;

class CommentFileUploadListener
{
    private $uploader;

    public function __construct(CommentFileUploader $uploader)
    {
        $this->uploader = $uploader;
    }

    public function prePersist(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();

        $this->uploadFile($entity);
    }

    public function preUpdate(PreUpdateEventArgs $args)
    {
        $entity = $args->getEntity();

        $this->uploadFile($entity);
    }

    private function uploadFile($entity)
    {

        if (!$entity instanceof TaskLog) {
            return;
        }

        $files = $entity->getFiles();

        foreach($files as $key => $file) {

            if ($file instanceof UploadedFile) {
                $files[$key] = $this->uploader->upload($entity, $file);
            }
        }

        $entity->setFiles($files);
    }
}