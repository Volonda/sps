<?php
namespace CommonBundle\EventListener;

use Symfony\Component\EventDispatcher\Event;
use CommonBundle\Manager\TaskManager;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use CommonBundle\Event\TaskChangedEvent;

class TaskListener
{
    /** @var TaskManager*/
    private $manager;

    /** @var TokenStorage*/
    private $tokenStorage;

    /**
     * @param TaskManager $manager
     * @param TokenStorage  $tokenStorage
     */
    public function __construct(TaskManager $manager, TokenStorage  $tokenStorage)
    {
        $this->manager = $manager;
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * @param TaskChangedEvent $event
     */
    public function onTaskChanged(TaskChangedEvent $event)
    {
        $changes = [];

        foreach($event->getChanges() as $key=>$change)
        {
            switch($key){
                case "deadlineDatetime":
                    $changes[$key] = [
                        ( $change[0]) ? $change[0]->format('d.m.Y H:m') : $change[0],
                        ( $change[1]) ? $change[1]->format('d.m.Y H:m') : $change[1],
                    ];
                    break;
                case "user":
                    $changes[$key] = [
                        ( $change[0]) ? $change[0]->getName() : $change[0],
                        ( $change[1]) ? $change[1]->getName() : $change[1],
                    ];

                    break;
                default:
                    $changes[$key] = $change;
                    break;
            }

        }

        $taskLog = $event->getTaskLog();
        $taskLog->setChanges($changes);
        $task = $event->getTask();

        $this->manager->addTaskLog(
            $this->tokenStorage->getToken()->getUser(),
            $task,
            $taskLog
        );

        $this->manager->generatePopUpNotification($event->getTask());

    }
}