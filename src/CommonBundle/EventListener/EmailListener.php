<?php
namespace CommonBundle\EventListener;

use CommonBundle\Entity\Meeting;
use CommonBundle\Event\TaskChangedEvent;
use CommonBundle\Event\TaskCreateEvent;
use CommonBundle\Event\MeetingNotificationEvent;
use CommonBundle\Event\TaskAddCommentEvent;
use CommonBundle\Event\TaskDeleteCommentEvent;
use CommonBundle\Manager\EmailManager;
use CommonBundle\Manager\UserManager;
use CommonBundle\Manager\TaskManager;
use CommonBundle\Manager\MeetingManager;
use CommonBundle\Manager\QuestionManager;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\DependencyInjection\ContainerInterface;
use UserBundle\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

class EmailListener
{
    /** @var EmailManager*/
    private  $emailManager;

    /** @var UserManager*/
    private  $userManager;

    /** @var MeetingManager*/
    private  $meetingManager;

    /** @var TaskManager*/
    private  $taskManager;

    /** @var QuestionManager*/
    private  $questionManager;

    /** @var TokenStorage*/
    protected $tokenStorage;

    /**
     * @param ContainerInterface $container
    */
    public function  __construct(ContainerInterface $container) {

        $this->emailManager = $container->get('common.manager.email_manager');
        $this->userManager = $container->get('common.manager.user_manager');
        $this->meetingManager = $container->get('common.manager.meeting_manager');
        $this->questionManager = $container->get('common.manager.question_manager');
        $this->taskManager = $container->get('common.manager.task_manager');
        $this->tokenStorage = $container->get('security.token_storage');
    }

    /**
     * @param MeetingNotificationEvent $event
     */
    public function onMeetingNotification(MeetingNotificationEvent $event)
    {
        $meeting = $event->getMeeting();
        $users =  $this->userManager->getById($event->getUsers());

        foreach($users as $user) {
            $lastNotificationDate = null;

            if ($meeting->getStatus() == Meeting::MEETING_STATUS_CREATED) {
                $lastNotificationDate = $this->meetingManager->getLastUserNotification($meeting, $user);
            }

            $email = $this->emailManager->createMeetingEmail($meeting, $user, $lastNotificationDate);

            if ($meeting->getStatus() == Meeting::MEETING_STATUS_CREATED) {
                $this->meetingManager->createUserNotificationHistory($meeting, $user, $email);
            }
        }

        $this->meetingManager->increaseMeetingNotificationCount($meeting);
    }

    /**
     * @param TaskAddCommentEvent $event
    */
    public function onTaskAddComment(TaskAddCommentEvent $event)
    {
        $task = $event->getTask(); 

        $users = $this->questionManager->getUsersForTaskEmail(
            $task->getQuestion()
        );

        /** @var User $user*/
        foreach($users as $user) {
            $this->emailManager->createTaskAddCommentEmail($user, $event->getTask(), $event->getTaskLog());
        }
    }

    /**
     * @param TaskDeleteCommentEvent $event
     */
    public function onTaskDeleteComment(TaskDeleteCommentEvent $event)
    {
        $task = $event->getTaskLog()->getTask();

        $users = $this->questionManager->getUsersForTaskEmail(
            $task->getQuestion()
        );

        /** @var User $user*/
        foreach($users as $user) {
            $this->emailManager->createTaskDeleteCommentEmail($user, $event->getTaskLog());
        }
    }

    /**
     * @param TaskChangedEvent $event
    */
    public function onTaskChanged(TaskChangedEvent $event)
    {
        $task = $event->getTask();
        $changeset = $event->getChanges();

        if(
            isset($changeset['deadlineDatetime'])
            || isset($changeset['user'])
            || isset($changeset['status'])
            || isset($changeset['type'])
        ){

            $users = new ArrayCollection($this->questionManager->getUsersForTaskEmail(
                $task->getQuestion()
            ));

            //Add old user if not exists
            /** @var User $oldUser*/
            if (isset($changeset['user'][0])) {
                $oldUser = $changeset['user'][0];

                if (
                    $users->exists(function ($num, User $changedUser) use ($oldUser) {
                        return $changedUser->getId() == $oldUser->getId();
                    }) == false
                ) {
                    $users->add($oldUser);
                };
            }

            /** @var User $user*/
            foreach($users as $sendToUser) {
                $this->emailManager->createTaskChangedEmail(
                    $sendToUser,
                    $this->tokenStorage->getToken()->getUser(),
                    $event->getTask(),
                    $changeset,
                    $event->getTaskLog()->getText()
                );
            }
        }
    }

    public function onTaskCreate(TaskCreateEvent $event)
    {
        $task = $event->getTask();

        $users = $this->questionManager->getUsersForTaskEmail(
            $task->getQuestion()
        );

        foreach($users as $sendToUser) {
            $this->emailManager->createTaskCreatedEmail(
                $sendToUser,
                $this->tokenStorage->getToken()->getUser(),
                $task
            );
        }
    }
}