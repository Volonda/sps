<?php
/**
 * Генерация task из task_planed
*/
namespace CommonBundle\EventListener;

use Symfony\Component\EventDispatcher\Event;
use CommonBundle\Manager\TaskManager;
use CommonBundle\Event\MeetingClosedEvent;

class MeetingListener
{
    /** @var TaskManager*/
    private $manager;
    /**
     * @param TaskManager $manager
     */
    public function __construct(TaskManager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * @param MeetingClosedEvent $event
     */
    public function onMeetingClosed(MeetingClosedEvent $event)
    {
        $this->manager->createTaskByMeeting($event->getMeeting());
    }
}