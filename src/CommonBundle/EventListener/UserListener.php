<?php

namespace CommonBundle\EventListener;

use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use CommonBundle\Manager\UserHistoryManager;
use CommonBundle\Entity\UserHistory;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;

class UserListener
{
    /** @var UserHistoryManager $userHistoryManager*/
    private $userHistoryManager;

    /**
     * @param UserHistoryManager $userHistoryManager
    */
    public function __construct(UserHistoryManager $userHistoryManager)
    {
        $this->userHistoryManager = $userHistoryManager;
    }

    public function onLogin(InteractiveLoginEvent $event)
    {
        $this->userHistoryManager->create($event->getAuthenticationToken()->getUser(), UserHistory::LOGIN_TYPE);
    }
}