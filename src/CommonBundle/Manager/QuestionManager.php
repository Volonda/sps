<?php
namespace CommonBundle\Manager;

use UserBundle\Entity\User;
use CommonBundle\Entity\Question;
use CommonBundle\Entity\TaskPlaned;
use Doctrine\ORM\EntityManager;
use Knp\Component\Pager\Paginator;
use WebSiteBundle\Filter\Filter;
use Doctrine\ORM\Query\ResultSetMappingBuilder;
use CommonBundle\FileUploader\QuestionFileUploader;
use Symfony\Component\HttpFoundation\File\UploadedFile;


class QuestionManager
{
    /** @var EntityManager $em*/
    private $em;

    /** @var Paginator*/
    private $paginator;

    /** @var QuestionFileUploader*/
    private $questionUploader;

    private $listByQuestionFilters = [
        'm.datetime' => 'date',
        'm.num' => 'int',
        'q.id' => 'int',
        'q.text' => 'like',
        'q.answer' => 'like',
    ];

    private $listByTaskFilters = [
        't.deadline_datetime' => 'date',
        'u.name' => 'like',
        't.text' => 'like',
        'q.answer' => 'like',
        'm.num' => 'int',
        't.id' => 'int',
        'q.id' => 'int'
    ];

    /**
     * @param EntityManager $em
     * @param Paginator $paginator
     * @param QuestionFileUploader $questionUploader
     *
    */
    public function __construct(EntityManager $em, Paginator $paginator, QuestionFileUploader $questionUploader)
    {
        $this->em = $em;
        $this->paginator = $paginator;
        $this->questionUploader = $questionUploader;
    }

    public function save(Question $question)
    {
        $em = $this->em;
        $questionUploader = $this->questionUploader;

        $question->getTasksPlaned()->forAll(function($num, TaskPlaned $task) use($question, $em) {
            if (!$task->getQuestion() instanceof Question) {
                $task->setQuestion($question);
            }
            return true;
        });

        foreach($question->getDeleteFiles() as $key => $file) {
            $question->deleteFileById($file);
        }

        foreach($question->getAddFiles() as $key => $file) {
            if ($file instanceof UploadedFile) {
                $question->addFile($questionUploader->upload($question, $file));
            }
        }

        $em->persist($question);
        $em->flush();
    }

    public function getListByQuestionFilters()
    {
        return $this->listByQuestionFilters;
    }

    public function getListByTaskFilters()
    {
        return $this->listByTaskFilters;
    }

    public function getListByQuestionPaginate($page, $limit, Filter $filter)
    {
        $query = $this->em->getConnection()->createQueryBuilder()
            ->select(
                [
                    'q.id, q.text, q.answer, q.meeting_id',
                    'm.datetime as meeting_datetime','m.num as meeting_num'
                ]
            )
            ->from('questions_active','q')
            ->join('q','meetings_active', 'm', 'm.id = q.meeting_id');

        $filter->addFiltersToDbalQB($query);

        $pagination = $this->paginator->paginate(
            $query,
            $page,
            $limit,
            ['defaultSortFieldName' => 'q.id', 'defaultSortDirection' => 'asc']
        );

        return $pagination;

    }

    public function getListByQuestionUserPaginate(User $user, $page, $limit, Filter $filter)
    {
        $query = $this->em->getConnection()
            ->createQueryBuilder()
            ->select( [
                'q.id, q.text, q.answer, q.meeting_id',
                'm.datetime as meeting_datetime','m.num as meeting_num'
            ])
            ->from('questions_active','q')
            ->join('q','meetings_active', 'm', 'm.id = q.meeting_id')
            ->where('m.id IN (
                SELECT meeting_id FROM user_meetings_view WHERE user_id = :userId
            )')
            ->setParameter('userId', $user->getId());

        $filter->addFiltersToDbalQB($query);

        $pagination = $this->paginator->paginate(
            $query,
            $page,
            $limit,
            ['defaultSortFieldName' => 'q.id', 'defaultSortDirection' => 'asc']
        );

        return $pagination;
    }

    public function getListByTaskPaginate($page, $limit, Filter $filter)
    {
        $query = $this->em->getConnection()->createQueryBuilder()
            ->select(
                [
                    't.id, t.deadline_datetime, t.question_id, t.user_id, t.text, t.period, t.period_type, t.status',
                    'u.name user_name',
                    'q.answer question_answer, q.meeting_id question_meeting_id',
                    'm.num meeting_num'
                ]
            )
            ->from('tasks','t')
            ->join('t','questions','q','q.id = t.question_id')
            ->join('t','users','u', 'u.id = t.user_id')
            ->join('q','meetings', 'm', 'm.id=q.meeting_id')
             ;

        $filter->addFiltersToDbalQB($query);

        $pagination = $this->paginator->paginate(
            $query,
            $page,
            $limit,
            ['defaultSortFieldName' => 't.id', 'defaultSortDirection' => 'asc']
        );

        return $pagination;

    }


    public function getLog(Question $question)
    {

        $query = $this->em->createQueryBuilder()
            ->select('tl')
            ->from('CommonBundle\Entity\TaskLog','tl')
            ->join('tl.task','t')
            ->join('t.question','q')
            ->where('q = :question')
            ->setParameter(':question', $question)
            ->orderBy('tl.datetime')
           ;

        return $query->getQuery()->getResult();
    }

    public function getUsersForTaskEmail(Question $question)
    {
        $rsm = new ResultSetMappingBuilder($this->em);
        $rsm->addRootEntityFromClassMetadata('UserBundle\Entity\User', 'u');

        $sql = "
            SELECT DISTINCT ON(u.id) u.*
            FROM (
                SELECT u.* FROM questions q
                    JOIN tasks t ON t.question_id = q.id
                    JOIN users u ON t.user_id = u.id
                  WHERE
                      q.id = :questionId
                      AND EXISTS (
                          SELECT 1 FROM user_meetings_view umv
                            WHERE
                              umv.user_id = u.id
                              AND umv.meeting_id = q.meeting_id
                              AND umv.status IN ('user_task', 'mayor')
                      )
                      AND u.status = :user_status

                UNION SELECT u.* FROM users u WHERE u.roles LIKE '%ROLE_ADMIN%' AND u.status = :user_status
            ) u
        ";

        $query = $this->em
            ->createNativeQuery($sql,$rsm)
            ->setParameter('questionId', $question->getId())
            ->setParameter('user_status', USER::STATUS_ENABLED)
        ;

        $result = $query->getResult(); 

        return $result;
    }
}