<?php
namespace CommonBundle\Manager;

use CommonBundle\Entity\Task;
use CommonBundle\Entity\TaskNotificationHistory;
use Doctrine\ORM\EntityManager;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class TaskNotificationHistoryManager
{
    /** @var EntityManager*/
    private $em;

    /**
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function add(Task $task, $type)
    {
        $history = new TaskNotificationHistory();
        $history->setTask($task);
        $history->setType($type);

        $this->em->persist($history);
        $this->em->flush($history);
    }

    /**
     * @param Task $task
     * @param \DateTime $date
     * @return bool
    */
    public function isNotified(Task $task, \DateTime $date)
    {
        $sql = "SELECT 1 FROM tasks_notifications_history nh WHERE nh.task_id = :taskId AND date(nh.created) = :date LIMIT 1";
        $stmt = $this->em->getConnection()->prepare($sql);
        $stmt->bindValue('date', $date->format('Y-m-d'));
        $stmt->bindValue('taskId', $task->getId());
        $stmt->execute();
        $result = $stmt->fetchColumn();

        return $result == true;
    }
}