<?php
namespace CommonBundle\Manager;

use CommonBundle\Entity\Email;
use CommonBundle\Entity\Task;
use CommonBundle\Entity\TaskLog;
use Doctrine\ORM\EntityManager;
use UserBundle\Entity\User;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use CommonBundle\Entity\Meeting;
use Twig_Environment;
use CommonBundle\Entity\Settings;

class EmailManager
{
    /** @var EntityManager*/
    private $em;

    /** @var string*/
    private $fromEmail;

    /** @var Settings $settings*/
    private $settings;

    private $cachedBody = [];

    /**
     * @param EntityManager $em
     * @param string $fromEmail
     * @param Twig_Environment $twig
     * @param Settings $settings
     */
    public function __construct(
        EntityManager $em,
        $fromEmail,
        Twig_Environment $twig,
        Settings $settings
    ) {
        $this->em = $em;
        $this->fromEmail = $fromEmail;
        $this->twig = $twig;
        $this->settings = $settings;
    }

    /**
     * @param Meeting $meeting
     * @param User $user
     * @return Email $email
     * @param \DateTime $lastNotification
     */
    public function createMeetingEmail(Meeting $meeting, User $user, \DateTime $lastNotification = null)
    {
        $email = new Email();
        $email->setFromEmail($this->fromEmail);
        $email->setToEmail($user->getEmail());
        $email->setToEmail($user->getEmail());

        $body = $this->twig->render(
            '@Common\Email\meetingEmail.html.twig', [
            'meeting' => $meeting,
            'lastNotification' => $lastNotification
        ]);
      
        $email->setBody($body);

        if ($meeting->getStatus() == Meeting::MEETING_STATUS_CREATED){
            if ($lastNotification){
                $subject = 'Изменения в планируемом совещании';
            } else {
                $subject = 'Планируется совещание';
            }

            $email->setSubject($subject . ' ' . $this->settings->getCompanyName());

        } elseif($meeting->getStatus() == Meeting::MEETING_STATUS_CLOSED) {

            $email->setSubject('Закрыто совещание '. $this->settings->getCompanyName() . ' №'. $meeting->getNum());

        } elseif($meeting->getStatus() == Meeting::MEETING_STATUS_OPENED) {

            $email->setSubject('Протокол на проверку '. $this->settings->getCompanyName());

        } 

        $this->em->persist($email);
        $this->em->flush($email);

        return $email;
    }


    /**
     * @param Task $task
     * @param User $user
    */
    public function createTaskNotificationEmail(Task $task, User $user)
    {
        $email = new Email();
        $email->setFromEmail($this->fromEmail);
        $email->setToEmail($user->getEmail());

        $body = $this->twig->render('@Common\Email\taskNotification.html.twig', [
                'task' => $task
            ]);
        $email->setBody($body);
        $email->setSubject('Напоминание по задаче #' . $task->getId());

        $this->em->persist($email);
        $this->em->flush($email);
    }

    /**
     * @param User $user
     * @param array $tasks
    */
    public function createDailyDigestEmail(User $user, array $tasks)
    {
        if(!empty($tasks)) {
            $now = new \DateTime('now');

            $email = new Email();
            $email->setFromEmail($this->fromEmail);
            $email->setToEmail($user->getEmail());

            $body = $this->twig->render(
                '@Common\Email\dailyDigest.html.twig', [
                'tasks' => $tasks,
                'now' => $now,
                'user' => $user
            ]);

            $email->setBody($body);
            $email->setSubject('Дайджест от ' . $now->format('d.m.Y'));
            $this->em->persist($email);


            $this->em->flush($email);
        }
    }

    /**
     * @param User $sendToUser
     * @param Task $task
     * @param TaskLog $taskLog
    */
    public function createTaskAddCommentEmail(User $sendToUser, Task $task, TaskLog $taskLog)
    {
        $email = new Email();
        $email->setFromEmail($this->fromEmail);
        $email->setToEmail($sendToUser->getEmail());
        $body = $this->getCachedBody(
            'createTaskAddCommentEmail',
            '@Common\Email\taskAddComment.html.twig', [
                'task' => $task,
                'taskLog' => $taskLog
            ]);
        $email->setBody($body);
        $email->setSubject('Новый комментарий к задаче #' . $task->getId() .'. Инициатор: ' . $taskLog->getUser()->getName());

        $this->em->persist($email);
        $this->em->flush($email);
    }

    /**
     * @param User $sendToUser
     * @param TaskLog $taskLog
     */
    public function createTaskDeleteCommentEmail(User $sendToUser, TaskLog $taskLog)
    {
        $task = $taskLog->getTask();

        $email = new Email();
        $email->setFromEmail($this->fromEmail);
        $email->setToEmail($sendToUser->getEmail());
        $body = $this->getCachedBody(
            'createTaskDeleteCommentEmail',
            '@Common\Email\taskDeleteComment.html.twig', [
            'task' => $task,
            'taskLog' => $taskLog
        ]);
        $email->setBody($body);
        $email->setSubject('Удален комментарий к задаче #' . $task->getId() .'. Инициатор: ' . $taskLog->getUser()->getName());

        $this->em->persist($email);
        $this->em->flush($email);
    }

    /**
     * @param User $sendToUser
     * @param Task $task
     * @param User $user
     * @param string $message
     * @param array $changeset
    */
    public function createTaskChangedEmail(User $sendToUser, User $user ,Task $task, array $changeset, $message = '')
    {
        $email = new Email();
        $email->setFromEmail($this->fromEmail);
        $email->setToEmail($sendToUser->getEmail());
        $body = $this->getCachedBody(
            'createTaskChangedEmail',
            '@Common\Email\taskChanged.html.twig', [
                'task' => $task,
                'user' => $user,
                'message' => $message,
                'changeset' => $changeset
            ]);
        $email->setBody($body);

        $email->setSubject('Измененя задачи #' . $task->getId() . '. Инициатор: ' . $user->getName());

        $this->em->persist($email);
        $this->em->flush($email);
    }

    public function createTaskCreatedEmail(User $sendToUser, User $user, Task $task)
    {
        $email = new Email();
        $email->setFromEmail($this->fromEmail);
        $email->setToEmail($sendToUser->getEmail());
        $body = $this->getCachedBody(
            'createTaskChangedEmail',
            '@Common\Email\taskCreated.html.twig', [
            'task' => $task,
            'user' => $user,
            'sendToUser' => $sendToUser
        ]);
        $email->setBody($body);

        if ($sendToUser->getId() == $task->getUser()->getId()) {
            $email->setSubject('У вас новая задача, инициатор: ' . $user->getName());
        } else {
            $email->setSubject('Новая задача, инициатор:  ' . $user->getName());
        }

        $this->em->persist($email);
        $this->em->flush($email);

    }

    /**
     * @param int $limit
     * @return array
     */
    public function getForSend($limit = null)
    {
        $query = $this->em->createQueryBuilder()
            ->select('e')
            ->from('CommonBundle\Entity\Email', 'e')
            ->where('e.status = :status')
            ->setParameter('status', Email::EMAIL_STATUS_CREATED);

        if (!empty($limit)) {
            $query->setMaxResults($limit);
        }

        $result = $query->getQuery()
            ->getResult();

        return $result;
    }

    /**
     * @param Email $email
    */
    public function setSuccess(Email $email)
    {
        $email->setStatus(Email::EMAIL_STATUS_SUCCESS);

        $this->em->persist($email);
        $this->em->flush($email);
    }

    /**
     * @param Email $email
     */
    public function setError(Email $email)
    {
        $email->setStatus(Email::EMAIL_STATUS_ERROR);

        $this->em->persist($email);
        $this->em->flush($email);
    }

    private function getCachedBody($key, $template, array $data)
    {
        if (!isset($this->cachedBody[$key])) {

            $this->cachedBody[$key] = $this->twig->render($template, $data);
        }

        return $this->cachedBody[$key];
    }
}