<?php
namespace CommonBundle\Manager;

use CommonBundle\Entity\Meeting;
use CommonBundle\Entity\RepeatTask;
use CommonBundle\Entity\FiniteTask;
use CommonBundle\Entity\Task;
use CommonBundle\Entity\TaskLog;
use CommonBundle\Entity\TaskPlaned;
use CommonBundle\Event\TaskDeleteCommentEvent;
use UserBundle\Entity\User;
use CommonBundle\Event\TaskAddCommentEvent;
use CommonBundle\Event\TaskTypeChangedEvent;
use CommonBundle\Event\TaskDescriptionChangedEvent;
use CommonBundle\Event\TaskCreateEvent;
use CommonBundle\Event\TaskChangedEvent;
use CommonBundle\Event\TaskStatusChangedEvent;
use CommonBundle\Event\TaskUserChangedEvent;
use CommonBundle\Event\TaskDeadlineChangedEvent;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use Knp\Component\Pager\Paginator;
use CommonBundle\Entity\Question;
use WebSiteBundle\Filter\Filter;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Doctrine\ORM\UnitOfWork;
use WebSiteBundle\Form\Type\TaskType\TaskRepeatEditType;
use WebSiteBundle\Form\Type\TaskType\TaskFiniteEditType;

class TaskManager
{
    /** @var EntityManager */
    private $em;

    /** @var array */
    private $listFilters = [
        'u.name' => 'like',
        't.id' => 'int',
        't.question_id' => 'int',
        'm.num' => 'int',
        't.description' => 'like',
        't.text' => 'like',
        't.deadline_datetime' => 'date',
        't.status' => [
            'type' => 'eq',
            'options' => []
        ],
        't.type' => [
            'type' => 'eq',
            'options' => []
        ]
    ];

    /**@var EventDispatcherInterface*/
    private $dispatcher;

    /** @var Paginator*/
    private $paginator;

    /**
     * @param EntityManager $em
     * @param Paginator $paginator
    */
    public function __construct(EntityManager $em, Paginator $paginator, EventDispatcherInterface $dispatcher)
    {
        $this->em = $em;
        $this->paginator = $paginator;
        $this->dispatcher = $dispatcher;
    }

    /**
     * @return array
    */
    public function getListFilters()
    {
        $this->listFilters['t.status']['options'] =  Task::getStatuses();
        $this->listFilters['t.type']['options'] =  Task::getTypeNames();

        return $this->listFilters;
    }

    /**
     * @return array
     */
    public function getRepeatListFilters()
    {
        $this->listFilters['t.type']['options'] =  Task::getTypeNames();

        return $this->listFilters;
    }

    /**
     * @return array
     */
    public function getFiniteListFilters()
    {
        $statuses = Task::getStatuses();

        $this->listFilters['t.status']['options'] = [
            Task::TASK_STATUS_CREATED => $statuses[Task::TASK_STATUS_CREATED],
            Task::TASK_STATUS_PROCESSING => $statuses[Task::TASK_STATUS_PROCESSING],
            Task::TASK_STATUS_TESTING => $statuses[Task::TASK_STATUS_TESTING],
            Task::TASK_STATUS_APPROVED => $statuses[Task::TASK_STATUS_APPROVED],
        ] ;
        $this->listFilters['t.type']['options'] =  Task::getTypeNames();

        return $this->listFilters;
    }

    /**
     * Список всех задач
     * @param int $page
     * @param int $limit
     * @param Filter $filter
     * @param string $type
     * @return Paginator
     */
    public  function getPaginate($type, $page, $limit, Filter $filter)
    {
        $query = $this->em->getConnection()
            ->createQueryBuilder()
            ->select(
                [
                    't.id, t.user_id, t.question_id, t.text, t.description, t.deadline_datetime, t.status, t.type',
                    't.period, t.period_type',
                    'q.meeting_id','q.mayor_id',
                    'm.num as meeting_num',
                    'u.name as user_name'
                ]
            )
            ->from('tasks', 't')
            ->join('t','users','u', 'u.id = t.user_id')
            ->join('t','questions','q', 'q.id = t.question_id')
            ->join('q','meetings','m', 'q.meeting_id = m.id')
        ;

        if ($type == Task::TASK_TYPE_FINITE || $type == Task::TASK_TYPE_REPEAT) {
            $query->where('t.type = :taskType')
                ->setParameter('taskType', $type);
        }

        $filter->addFiltersToDbalQB($query);

        $pagination = $this->paginator->paginate(
            $query,
            $page,
            $limit,
            ['defaultSortFieldName' =>  't.id', 'defaultSortDirection' => 'DESC']
        );

        return $pagination;
    }


    /**
     * Задачи где ответственный user
     * @param string $type
     * @param User $user
     * @param int $page
     * @param int $limit
     * @param Filter $filter
     * @param array $statuses
     * @return Paginator
     */
    public function getByUserPaginate($type, User $user, $page, $limit, Filter $filter, array $statuses = [])
    {
        $query = $this->em->getConnection()
            ->createQueryBuilder()
            ->select(
                [
                    't.id, t.user_id, t.question_id, t.text, t.description, t.deadline_datetime, t.status, t.type',
                    't.period, t.period_type',
                    'q.meeting_id, q.mayor_id',
                    'm.num as meeting_num',
                    'u.name user_name'
                ]
            )
            ->from('tasks', 't')
            ->join('t','users','u', 'u.id = t.user_id')
            ->join('t','questions','q', 'q.id = t.question_id')
            ->join('q','meetings','m', 'q.meeting_id = m.id')
            ->where('t.user_id = :userId')
            ->setParameter('userId', $user->getId());

        if ($type == Task::TASK_TYPE_FINITE || $type == Task::TASK_TYPE_REPEAT) {
            $query->andWhere('t.type = :taskType')
                ->setParameter('taskType', $type);
        }

        if (sizeof($statuses)) {

            $statuses= array_map(function($value) {
                return "'" . $value . "'";
            }, $statuses);

            $query->andWhere($query->expr()->in('t.status', $statuses))
            ;
        }

        $filter->addFiltersToDbalQB($query);

        $pagination = $this->paginator->paginate(
            $query,
            $page,
            $limit,
            ['defaultSortFieldName' => 't.id', 'defaultSortDirection' => 'DESC']
        );

        return $pagination;
    }

    /**
     * @param Task $task
     * @param TaskLog|null $taskLog
     */
    public function processing(Task $task, TaskLog $taskLog = null)
    {
        $task->setStatus(Task::TASK_STATUS_PROCESSING);

        $this->taskChangeEvents($task, $taskLog);

        $task->setStartDatetime(new \DateTime('now'));

        $this->em->persist($task);
        $this->em->flush($task);
    }

    /**
     * @param Task $task
     */
    public function completed(Task $task)
    {
        $task->setStatus(Task::TASK_STATUS_COMPLETED);

        $this->taskChangeEvents($task);

        $task->setEndDatetime(new \DateTime('now'));

        $this->em->persist($task);
        $this->em->flush($task);
    }

    /**
     * @param Task $task
     */
    public function approved(Task $task)
    {
        $task->setStatus(Task::TASK_STATUS_APPROVED);

        $this->taskChangeEvents($task);

        $this->em->persist($task);
        $this->em->flush($task);
    }

    /**
     * @param Task $task
     * @param TaskLog $taskLog
    */
    public function testing(Task $task, TaskLog $taskLog = null)
    {
        $task->setStatus(Task::TASK_STATUS_TESTING);

        $this->taskChangeEvents($task, $taskLog);

        $this->em->persist($task);
        $this->em->flush($task);
    }

    /**
     * @param Task $task
     * @param string $message
    */
    public function save(Task $task, $message = '')
    {
        $changeset = $this->taskChangeEvents($task, $message);

        if(
            isset($changeset['status'])
            && $changeset['status'][1] == Task::TASK_STATUS_COMPLETED
        ) {
            $task->setEndDatetime(new \DateTime('now'));
        } elseif(
            isset($changeset['status'])
            && $changeset['status'][1] == Task::TASK_STATUS_PROCESSING
        ) {
            $task->setStartDatetime(new \DateTime('now'));
        }

        $this->em->persist($task);
        $this->em->flush($task);
    }

    /**
     * @param User $user
     * @return array
    */
    public function getCountTaskCreatedByUser(User $user)
    {
        $result = $this->em->createQueryBuilder()
            ->select('COUNT(t.id)')
            ->from('CommonBundle:Task','t')
            ->join('t.user','u')
            ->where('u = :user')
            ->andWhere('t.status = :statusCreated')
            ->setParameter('user', $user)
            ->setParameter('statusCreated', Task::TASK_STATUS_CREATED)
            ->getQuery()
            ->getOneOrNullResult();


        if (isset($result[1])) {

            return $result[1];
        }

        return 0;
    }

    /**
     * @param Meeting $meeting
    */
    public function createTaskByMeeting(Meeting $meeting)
    {
        /**
         * @var Question $question
        */
        foreach($meeting->getQuestions() as $question)
        {

            $tasks = [];
            foreach($question->getTasksPlaned() as $planedTask) {

                /** @var $user User*/
                foreach($planedTask->getUsers() as $user) {

                    $task = null;
                    /** @var TAskPlaned $planedTask*/
                    if ($planedTask->getType() == TaskPlaned::TASK_PLANED_TYPE_FINITE) {

                        $task = new FiniteTask();

                    } elseif($planedTask->getType() == TaskPlaned::TASK_PLANED_TYPE_REPEAT) {

                        $task = new RepeatTask();
                        $task->setPeriod($planedTask->getPeriod());
                        $task->setPeriodType($planedTask->getPeriodType());

                    }

                    $task->setUserStatusView(false);
                    $task->setDeadlineDatetime($planedTask->getDeadlineDatetime());
                    $task->setDescription($planedTask->getDescription());
                    $task->setText($planedTask->getText());
                    $task->setStartDatetime($planedTask->getStartDatetime());
                    $task->setQuestion($planedTask->getQuestion());
                    $task->setUser($user);
                    $task->setEndDatetime($planedTask->getEndDatetime());
                    $task->setStartDatetime($planedTask->getStartDatetime());

                    $this->em->persist($task);
                    $tasks[] = $task;

                }
            }
        }

        $this->em->flush();
    }


    /**
     * @param User $user
     * @param Task $task
     * @param TaskLog $taskLog
    */
    public function addTaskLog(User $user, Task $task, TaskLog $taskLog)
    {
        $taskLog->setUser($user);
        $taskLog->setTask($task);
        $task->addTaskLog($taskLog);

        $this->em->persist($taskLog);
        $this->em->flush($taskLog);
    }

    /**
     * @param User $user
     * @param Task $task
     * @param TaskLog $taskLog
     */
    public function addTaskLogComment(User $user, Task $task, TaskLog $taskLog)
    {
        $this->addTaskLog($user, $task, $taskLog);

        $this->dispatcher->dispatch('task_add_comment', new TaskAddCommentEvent($task, $taskLog));
    }

    /**
     * @param User $user
     * @param TaskLog $taskLog
     */
    public function deleteTaskLogComment(User $user, TaskLog $taskLog)
    {
        if ($user->getId() == $taskLog->getUser()->getId() || $user->hasRole('ROLE_ADMIN')){

            $this->dispatcher->dispatch('task_delete_comment', new TaskDeleteCommentEvent($taskLog));

            $this->em->remove($taskLog);
            $this->em->flush($taskLog);
        }
    }

    /**
     * @param array $taskData
     * @param Question $question
     * @param ArrayCollection $users
     *
     */
    public function createTaskListForQuestion(array $taskData, Question $question, ArrayCollection $users)
    {
        $tasks = [];
        foreach($users as $user) {

            $task = null;
            if ($taskData['type'] == TaskPlaned::TASK_PLANED_TYPE_FINITE) {

                $task = new FiniteTask();

            } elseif($taskData['type'] == TaskPlaned::TASK_PLANED_TYPE_REPEAT) {

                $task = new RepeatTask();
                $task->setPeriodType($taskData['periodType']);
                $task->setPeriod($taskData['period']);
            }

            $task->setQuestion($question);
            $task->setUser($user);
            $task->setDeadlineDatetime($taskData['deadlineDatetime']);
            $task->setDescription($taskData['description']);
            $task->setText($taskData['text']);

            $this->generatePopUpNotification($task);

            $this->em->persist($task);

            $tasks[] = $task;

            $this->dispatcher->dispatch('task_create', new TaskCreateEvent($task));
        }

        $this->em->flush();
    }

    /**
     * @param Task $task
     * @throws \Exception
     */
    public function getEditType(Task $task)
    {
        if ($task instanceof FiniteTask) {

            return TaskFiniteEditType::class;

        } elseif($task instanceof RepeatTask) {

            return TaskRepeatEditType::class;
        }

        throw new \Exception('Unknown task type');
    }

    /**
     * @param FiniteTask $finiteTask
     * @param RepeatTask $dataRepeatTask
     * @param $message
    */
    public function saveAsRepeatTask(FiniteTask $finiteTask, RepeatTask $dataRepeatTask, $message = '')
    {
        $connection = $this->em->getConnection();
        $stm = $connection->prepare('UPDATE tasks SET type = :newType  WHERE id = :id');
        $stm->bindValue('newType', Task::TASK_TYPE_REPEAT);
        $stm->bindValue('id', $finiteTask->getId());

        $stm->execute();

        $this->em->clear(Task::class);

        /** @var RepeatTask $repeatTask*/
        $repeatTask = $this->em->getRepository('CommonBundle\Entity\Task')->find($finiteTask->getId());

        //Из формы
        $repeatTask->setStatus($dataRepeatTask->getStatus());
        $repeatTask->setText($dataRepeatTask->getText());
        $repeatTask->setDescription($dataRepeatTask->getDescription());
        $repeatTask->setUser($dataRepeatTask->getUser());
        $repeatTask->setPeriod($dataRepeatTask->getPeriod());
        $repeatTask->setPeriodType($dataRepeatTask->getPeriodType());
        $repeatTask->setDeadlineDatetime($dataRepeatTask->getDeadlineDatetime());

        $changeset['type'] = [
            Task::TASK_TYPE_FINITE,
            Task::TASK_TYPE_REPEAT
        ];

        $this->taskChangeEvents($repeatTask, $message, $changeset);

        $this->em->persist($repeatTask);
        $this->em->flush($repeatTask);
    }

    public function copyForUsers(Task $task, ArrayCollection $users)
    {
        $entityManager = $this->em;
        $dispatcher = $this->dispatcher;

        $users->forAll(function($num, User $user) use ($entityManager, $task, $dispatcher){

            if ($task->getUser()->getId() == $user->getId()) {
                return true;
            }

            $newTask = clone $task;
            $newTask->setUser($user);

            $entityManager->detach($newTask);
            $entityManager->persist($newTask);
            $entityManager->flush($newTask);

            $dispatcher->dispatch('task_create', new TaskCreateEvent($newTask));

            return true;
        });

    }

    public function saveAsFiniteTask(RepeatTask $repeatTask, FiniteTask $dataFiniteTask, $message = '')
    {
        $connection = $this->em->getConnection();
        $stm = $connection->prepare('UPDATE tasks SET type = :newType, period = null, period_type = null WHERE id = :id');
        $stm->bindValue('newType', Task::TASK_TYPE_FINITE);
        $stm->bindValue('id', $repeatTask->getId());

        $stm->execute();

        $this->em->clear(Task::class);

        /** @var FiniteTask $finiteTask*/
        $finiteTask = $this->em->getRepository('CommonBundle\Entity\Task')->find($repeatTask->getId());

        //Из формы
        $finiteTask->setStatus($dataFiniteTask->getStatus());
        $finiteTask->setText($dataFiniteTask->getText());
        $finiteTask->setDescription($dataFiniteTask->getDescription());
        $finiteTask->setDeadlineDatetime($dataFiniteTask->getDeadlineDatetime());
        $finiteTask->setUser($dataFiniteTask->getUser());

        //Из базы
        $finiteTask->setEndDatetime($repeatTask->getEndDatetime());
        $finiteTask->setStartDatetime($repeatTask->getStartDatetime());
        $finiteTask->setQuestion($repeatTask->getQuestion());

        $changeset['type'] = [
            Task::TASK_TYPE_REPEAT,
            Task::TASK_TYPE_FINITE
        ];

        $this->taskChangeEvents($finiteTask, $message, $changeset);

        $this->em->persist($finiteTask);
        $this->em->flush($finiteTask);
    }

    public function prolongation()
    {
        $conn = $this->em->getConnection();
        $stmt = $conn->prepare("
            UPDATE tasks t SET deadline_datetime = (
            t.deadline_datetime +
              (
                  CASE
                      WHEN t.period_type = :period_type_day THEN (t.period || ' days')::INTERVAL
                      WHEN t.period_type = :period_type_week THEN (t.period || ' weeks')::INTERVAL
                      WHEN t.period_type = :period_type_month THEN (t.period || ' months')::INTERVAL
                  END
              )

            )
            WHERE
              t.type = :task_type
              AND DATE(t.deadline_datetime) < CURRENT_DATE
              AND t.status != :status_completed
              AND t.period_type IN (:period_type_day, :period_type_week, :period_type_month)
        ");

        $stmt->bindValue('task_type', Task::TASK_TYPE_REPEAT);
        $stmt->bindValue('period_type_day', RepeatTask::PERIOD_TYPE_DAY);
        $stmt->bindValue('period_type_week', RepeatTask::PERIOD_TYPE_WEEK);
        $stmt->bindValue('period_type_month', RepeatTask::PERIOD_TYPE_MOUTH);
        $stmt->bindValue('status_completed', Task::TASK_STATUS_COMPLETED);


        $stmt->execute();

    }

    /**
     * @param User $user
     * @return array
     */
    public function getUserPopUpNotifications(User $user) {

        $conn = $this->em->getConnection();

        $stmt = $conn->prepare("
            SELECT t.id, t.admin_status_view, t.mayor_status_view, t.user_status_view, t.status FROM tasks t
              JOIN questions q ON t.question_id = q.id
            WHERE
              (t.user_id = :userId1 AND t.user_status_view IS FALSE AND t.status = :statusCreated)
              OR (q.mayor_id = :userId2 AND t.mayor_status_view IS FALSE AND t.status = :statusTesting)
          ")
        ;

        $stmt->bindValue('userId1', $user->getId());
        $stmt->bindValue('userId2', $user->getId());
        $stmt->bindValue('statusCreated', Task::TASK_STATUS_CREATED);
        $stmt->bindValue('statusTesting', Task::TASK_STATUS_TESTING);

        $stmt->execute();

        return $stmt->fetchAll();
    }

    /**
     * @param User $user
     * @return array
    */
    public function getAdminPopUpNotifications(User $user) {

        $conn = $this->em->getConnection();

        $stmt = $conn->prepare("
            SELECT t.id, t.admin_status_view, t.mayor_status_view, t.user_status_view, t.text, t.status FROM tasks t
              JOIN questions q ON t.question_id = q.id
            WHERE
              (t.user_id = :userId1 AND t.user_status_view IS FALSE AND t.status = :statusCreated)
              OR (t.admin_status_view IS FALSE AND t.status = :statusApproved)
              OR (q.mayor_id = :userId2 AND t.mayor_status_view IS FALSE AND t.status = :statusTesting)
          ")
        ;

        $stmt->bindValue('userId1', $user->getId());
        $stmt->bindValue('userId2', $user->getId());
        $stmt->bindValue('statusCreated', Task::TASK_STATUS_CREATED);
        $stmt->bindValue('statusApproved', Task::TASK_STATUS_APPROVED);
        $stmt->bindValue('statusTesting', Task::TASK_STATUS_TESTING);

        $stmt->execute();

        return $stmt->fetchAll();
    }


    public function generatePopUpNotification(Task $task)
    {
        switch ($task->getStatus()) {
            case Task::TASK_STATUS_CREATED:
                $task->setAdminStatusView(true);
                $task->setUserStatusView(false);
                $task->setMayorStatusView(true);
                break;
            case Task::TASK_STATUS_TESTING:
                $task->setAdminStatusView(true);
                $task->setUserStatusView(true);
                $task->setMayorStatusView(false);
                break;
            case Task::TASK_STATUS_APPROVED:
                $task->setAdminStatusView(false);
                $task->setUserStatusView(true);
                $task->setMayorStatusView(true);
                break;
        }
    }

    public function viewed(Task $task, $type = null)
    {
        if (!$type) {
            $task->setAdminStatusView(true);
            $task->setMayorStatusView(true);
            $task->setUserStatusView(true);
        } else {

            switch ($type) {
                case 'admin':
                    $task->setAdminStatusView(true);
                    break;
                case 'user':
                    $task->setUserStatusView(true);
                    break;
                case 'mayor':
                    $task->setMayorStatusView(true);
                    break;
            }
        }

        $this->em->persist($task);
        $this->em->flush($task);
    }

    /**
     * @param Task $task
     * @param TaskLog|string $log
     * @return array
    */
    private function taskChangeEvents(Task $task, $log = '', $addChangeset = null)
    {
        /** @var UnitOfWork $uow */
        $uow = $this->em->getUnitOfWork();
        $uow->computeChangeSets();
        $changeset = $uow->getEntityChangeSet($task);

        if (is_array($addChangeset)) {
            $changeset = array_merge($changeset, $addChangeset);
        }

        if (!$log instanceof TaskLog) {
            $taskLog = new TaskLog();
            $taskLog->setText($log);
        } else {
            $taskLog = $log;
        }

        $this->dispatcher->dispatch('task_changed', new TaskChangedEvent($task, $taskLog, $changeset));

        return $changeset;
    }
}