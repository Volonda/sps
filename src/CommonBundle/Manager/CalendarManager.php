<?php
namespace CommonBundle\Manager;

use Doctrine\ORM\EntityManager;
use UserBundle\Entity\User;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Doctrine\ORM\Query\ResultSetMappingBuilder;

class CalendarManager
{
    /** @var EntityManager*/
    private $em;

    /**
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }


    public function getNextMeetingsByUser(User $user)
    {
        $sql = 'SELECT m.num, m.datetime::date as date  FROM meetings m
                WHERE
                  m.datetime::date >= CURRENT_DATE
                  AND m.deleted_at IS NULL
                  AND EXISTS (
                    SELECT 1 FROM user_meetings_view um WHERE um.meeting_id = m.id AND um.user_id = :userId LIMIT 1
                  )
                ORDER BY m.datetime ASC ';

        $stm = $this->em->getConnection()->prepare($sql);
        $stm->bindValue(':userId', $user->getId());
        $stm->execute();

        return $stm->fetchAll();
    }

    public function getMeetingsByYearAndUser($year, User $user)
    {
        $sql = "SELECT m.num, m.datetime::date as date, m.datetime  FROM meetings m
                   WHERE
                      date_part('year',m.datetime) >= :yearFrom
                      AND date_part('year',m.datetime) < :yearTill
                      AND m.deleted_at IS NULL
                      AND EXISTS (
                        SELECT 1 FROM user_meetings_view um WHERE um.meeting_id = m.id AND um.user_id = :userId LIMIT 1
                      )
                ORDER BY m.datetime ASC";

        $stm = $this->em->getConnection()->prepare($sql);
        $stm->bindValue(':yearFrom', $year);
        $stm->bindValue(':yearTill', ($year + 1));
        $stm->bindValue(':userId', $user->getId());
        $stm->execute();

        return $stm->fetchAll();
    }

    function getMeetingsByDateAndUser(\Datetime $date, User $user)
    {


        $rsm = new ResultSetMappingBuilder($this->em);
        $rsm->addRootEntityFromClassMetadata('CommonBundle\Entity\Meeting', 'm');

        $sql = "SELECT m.*  FROM meetings m
                   WHERE
                      m.datetime >= :dateFrom
                      AND m.datetime <= :dateTill
                      AND m.deleted_at IS NULL
                      AND EXISTS (
                        SELECT 1 FROM user_meetings_view um WHERE um.meeting_id = m.id AND um.user_id = :userId LIMIT 1
                      )
                ORDER BY m.datetime ASC";

        $query = $this->em->createNativeQuery($sql, $rsm);
        $query->setParameter('dateFrom', $date->format('Y-m-d 00:00:00'));
        $query->setParameter('dateTill', $date->format('Y-m-d 23:59:59'));
        $query->setParameter(':userId', $user->getId());

        $meetings = $query->getResult();

        return $meetings;
    }

}