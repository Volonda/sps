<?php
namespace CommonBundle\Manager;

use CommonBundle\Entity\Settings;
use Doctrine\ORM\EntityManager;

use Symfony\Component\EventDispatcher\EventDispatcherInterface;



class SettingsManager
{
    /** @var EntityManager*/
    private $em;

    /**
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * @return Settings
    */
    public function getSettings()
    {
        $settings = $this->em->getRepository('CommonBundle\Entity\Settings')->findBy([],null,1,0);

        if (empty($settings)) {

            return new Settings();
        } else {

            return $settings[0];
        }

    }

    /**
    * @param Settings $settings
    */
    public function saveSettings(Settings $settings)
    {
        $this->em->persist($settings);
        $this->em->flush();
    }

}