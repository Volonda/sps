<?php
namespace CommonBundle\Manager;

use CommonBundle\Entity\UserHistory;
use Doctrine\ORM\EntityManager;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use UserBundle\Entity\User;

class UserHistoryManager
{
    /** @var EntityManager */
    private $em;

    /**
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * @param User $user
     * @param string $type
    */
    public function create(User $user, $type)
    {
        $userHistory = new UserHistory();
        $userHistory->setUser($user);
        $userHistory->setType($type);

        $this->em->persist($userHistory);
        $this->em->flush($userHistory);
    }
}