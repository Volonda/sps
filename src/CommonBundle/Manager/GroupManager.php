<?php
namespace CommonBundle\Manager;

use CommonBundle\Entity\Group;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Knp\Component\Pager\Paginator;
use WebSiteBundle\Filter\Filter;
use Doctrine\Common\Collections\Collection;

class GroupManager
{
    /** @var EntityManager*/
    private $em;

    /** @var Paginator*/
    private $paginator;

    private $listFilters = [
        'g.title' => 'like',
        'g.description' => 'like'
    ];

    /**
     * @param EntityManager $em
     * @param Paginator $paginator
     */
    public function __construct(EntityManager $em, Paginator $paginator)
    {
        $this->em = $em;
        $this->paginator = $paginator;
    }

    public function getListFilters()
    {
        $listFilters = $this->listFilters;

        return $listFilters;
    }

    public function getListPaginate($page, $limit, Filter $filter)
    {
        $query = $this->em->createQueryBuilder()
            ->select('g')
            ->from('CommonBundle\Entity\Group', 'g');

        $filter->addFiltersToQB($query);

        $pagination = $this->paginator->paginate(
            $query,
            $page,
            $limit,
            ['defaultSortFieldName' => 'g.id', 'defaultSortDirection' => 'asc']
        );

        return $pagination;
    }

    public function save(Group $group)
    {
        $this->em->persist($group);
        $this->em->flush();
    }

    public function delete(Group $group)
    {
        $this->em->remove($group);
        $this->em->flush();
    }

    public function createForMembers(Group $group, Collection $members)
    {
        $users = new ArrayCollection();

        foreach($members as $member) {
            $users->add($member->getUser());
        }

        $group->setUsers($users);

        $this->em->persist($group);
        $this->em->flush();
    }
}