<?php
namespace CommonBundle\Manager;

use CommonBundle\Entity\Task;
use Doctrine\ORM\EntityManager;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use CommonBundle\Entity\TaskNotification;
use UserBundle\Entity\User;

class TaskNotificationManager
{
    /** @var EntityManager*/
    private $em;

    /**
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * @param Task $task
     */
    public function save(Task $task)
    {
        $task->getNotifications()->forAll(function($num, TaskNotification $notification) use ($task){

            $notification->setTask($task);

            return true;
        });

        $this->em->persist($task);
        $this->em->flush();
    }

    /**
     * @param bool|int $limit
     * @return array
     */
    public function getTodaySingleList($limit = false)
    {
        $sub = $this->em->createQueryBuilder();
        $sub->select('tnh');
        $sub->from('CommonBundle\Entity\TaskNotificationHistory', 'tnh')
            ->where('tnh.task = tn.task AND date(tnh.created) = CURRENT_DATE()');

        $qb = $this->em->createQueryBuilder();
        $query = $qb->select('tn, t')
            ->from('CommonBundle\Entity\TaskSingleNotification',' tn')
            ->join('tn.task', 't')
            ->join('t.user', 'u')
            ->where('tn.date = CURRENT_DATE()')
            ->andWhere('u.status = :status_enabled')
            ->andWhere($qb->expr()->not($qb->expr()->exists($sub->getDQL())))
            ->setParameter('status_enabled', User::STATUS_ENABLED)
        ;

        if ($limit) {
            $query->setMaxResults($limit);
        }

        return $query->getQuery()
            ->getResult();
    }

    /**
     * @param bool|int $limit
     * @return array
    */
    public function getTodayRepeatList($limit = false)
    {
        $now = (new \DateTime('now'));

        $sub = $this->em->createQueryBuilder();
        $sub->select('tnh');
        $sub->from('CommonBundle\Entity\TaskNotificationHistory', 'tnh')
            ->where('tnh.task = tn.task AND date(tnh.created) = CURRENT_DATE()');

        $qb = $this->em->createQueryBuilder();
        $query = $qb->select('tn, t')
            ->from('CommonBundle\Entity\TaskRepeatNotification',' tn')
            ->join('tn.task', 't')
            ->join('t.user', 'u')
            ->add('where', $qb->expr()->in('t.status', [
                Task::TASK_STATUS_PROCESSING,
                Task::TASK_STATUS_CREATED,
                Task::TASK_STATUS_APPROVED,
                Task::TASK_STATUS_TESTING
            ]))
            ->andWhere('u.status = :status_enabled')
            ->andWhere("tn.days LIKE :day")
            ->andWhere($qb->expr()->not($qb->expr()->exists($sub->getDQL())))
            ->setParameter('day','%' . $now->format('l') . '%')
            ->setParameter('status_enabled', User::STATUS_ENABLED)
             ;

        if ($limit) {
            $query->setMaxResults($limit);
        }

        return $query->getQuery()
            ->getResult();
    }
}