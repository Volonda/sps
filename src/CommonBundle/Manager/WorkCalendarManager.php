<?php
namespace CommonBundle\Manager;

use CommonBundle\Entity\TaskComment;
use CommonBundle\Entity\WorkCalendar;
use CommonBundle\Entity\WorkCalendarChange;
use Doctrine\ORM\EntityManager;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class WorkCalendarManager
{
    /** @var EntityManager */
    private $em;

    /**
     * @param EntityManager $em
    */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * @param integer $year
     * @return WorkCalendar
    */
    public function getForYear($year = null)
    {
        if ((int)$year) {
            $calendar = $this->em->createQueryBuilder()
                ->select('wk')
                ->from('CommonBundle\Entity\WorkCalendar', 'wk')
                ->join('wk.changes', 'ch')
                ->where('wk.year = :year')
                ->setParameter('year', (int)$year)
                ->orderBy('wk.id')
                ->getQuery()
                ->getOneOrNullResult();

            if ($calendar) {

                return $calendar;
            }
        }

        $calendar = new WorkCalendar();
        $calendar->setYear($year);

        return $calendar;
    }

    /**
     * @param WorkCalendar $calendar
    */
    public function save(WorkCalendar $calendar)
    {
        $calendar->getChanges()->forAll(function($num, WorkCalendarChange $change) use ($calendar){
            $change->setCalendar($calendar);

            return true;
        });

        $this->em->persist($calendar);
        $this->em->flush($calendar);
    }
}