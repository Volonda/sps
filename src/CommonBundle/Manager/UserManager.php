<?php
namespace CommonBundle\Manager;

use CommonBundle\Entity\Question;
use UserBundle\Entity\User;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Knp\Component\Pager\Paginator;
use WebSiteBundle\Filter\Filter;

class UserManager
{
    /** @var EntityManager*/
    private $em;

    /** @var Paginator*/
    private $paginator;

    private $listFilters = [
        'u.name' => 'like',
        'ud.position' => 'like',
        'u.status' => [
            'type' => 'eq',
            'options' => []
        ],
        'u.roles' => [
            'type' => 'like',
            'options' => []
        ],
    ];

    /**
     * @param EntityManager $em
     * @param Paginator $paginator
     */
    public function __construct(EntityManager $em, Paginator $paginator)
    {
        $this->em = $em;
        $this->paginator = $paginator;
    }

    public function getListFilters()
    {
        $listFilters = $this->listFilters;

        $roles = User::getRolesName();

        $listFilters['u.roles']['options'] = [
            'ROLE_ADMIN' => $roles['ROLE_ADMIN'],
            'ROLE_SECRETARY' => $roles['ROLE_SECRETARY'],
            'ROLE_CHAIRMAN' => $roles['ROLE_CHAIRMAN'],
        ];

        $listFilters['u.status']['options'] = User::getStatusNames();

        return $listFilters;
    }

    public function getListPaginate($page, $limit, Filter $filter)
    {
        $query = $this->em->createQueryBuilder()
            ->select('u')
            ->from('UserBundle\Entity\User', 'u')
            ->leftJoin('u.data', 'ud')
        ;

        $filter->addFiltersToQB($query);

        $pagination = $this->paginator->paginate(
            $query,
            $page,
            $limit,
            ['defaultSortFieldName' => 'u.name', 'defaultSortDirection' => 'ASC']
        );

        return $pagination;
    }

    /**
     * @param array|int $id
     * @return array
     */
    public  function getById($id, $single = false) {
        $id = (array) $id;

        $qb = $this->em->createQueryBuilder();
        $qb->select('u');
        $qb->from('UserBundle\Entity\User', 'u');
        $qb->where($qb->expr()->in('u.id', $id));

        if  ($single) {

            $result = $qb->getQuery()->getSingleResult();
        } else {

            $result = $qb->getQuery()->getResult();
        }
        return $result;
    }

    /**
     * @param Question $question
     * @return array
    */
    public function getQuestionMayorChoices(Question $question)
    {
        $result = [];

        $connection = $this->em->getConnection();

        $sql = "
            SELECT u.id FROM user_questions uq
              JOIN users u ON u.id = uq.user_id
            WHERE uq.question_id = :questionId

            UNION
            SELECT u.id FROM task_planed tp
              JOIN tasks_planed_user tpu ON tpu.task_planed_id = tp.id
              JOIN users u ON u.id = tpu.user_id
            WHERE tp.question_id = :questionId

          ORDER BY id ASC
        ";

        $stm = $connection->prepare($sql);
        $stm->bindValue('questionId', $question->getId());
        $stm->execute();

        $usersId = array_column($stm->fetchAll(), 'id');

        if (count($usersId) == 0) {
            return $result;
        }

        return $this->em->createQuery('SELECT u FROM UserBundle\Entity\User u WHERE u.id IN (:userId) ORDER BY u.id')
            ->setParameter('userId', $usersId)
            ->getResult();

    }
}