<?php
namespace CommonBundle\Manager;

use CommonBundle\Entity\Task;
use CommonBundle\Entity\TaskComment;
use Doctrine\ORM\EntityManager;
use Knp\Component\Pager\Paginator;
use WebSiteBundle\Filter\Filter;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Doctrine\ORM\QueryBuilder;

class ReportManager
{
    /** @var EntityManager $em*/
    private $em;

    /** @var Paginator*/
    private $paginator;

    private $listTaskFilters = [
        'u.name' => 'like',
        'q.id' => 'int',
        't.id' => 'int',
        't.status' => [
            'type' => 'closure',
            'options' => []
        ],
        'q.text' => 'like',
        'ma.name' => 'like',
        'q.answer' => 'like',
        'm.num' => 'int',
    ];

    /**
     * @param EntityManager $em
     * @param Paginator $paginator
    */
    public function __construct(EntityManager $em, Paginator $paginator)
    {
        $this->em = $em;
        $this->paginator = $paginator;
    }

    public function getListTaskFilters()
    {
        $statuses = Task::getStatuses();
        $statuses[Task::PSEUDO_STATUS_NOT_COMPLETED ] = 'Не завершена';

        $filters = $this->listTaskFilters;
        $filters['t.status']['options'] = $statuses;
        $filters['t.status']['closure'] = function(QueryBuilder $qb, $filter, $key) {

            if (in_array($filter['value'], array_keys(Task::getStatuses()))) {
                $qb
                    ->andWhere('t.status = :task_status')
                    ->setParameter('task_status', $filter['value'])
                ;
            } elseif($filter['value'] == Task::PSEUDO_STATUS_NOT_COMPLETED) {
                $qb
                    ->andWhere('t.status != :task_pseudo_status')
                    ->setParameter('task_pseudo_status',  Task::TASK_STATUS_COMPLETED)
                ;
            }

        };


        return $filters;
    }

    public function getListTaskPaginate($page, $limit, Filter $filter)
    {
        $query = $this->em->createQueryBuilder()
            ->select('t')
            ->from('CommonBundle\Entity\Task', 't')
            ->join('t.question', 'q')
            ->join('t.user', 'u')
            ->join('q.mayor', 'ma')
            ->join('q.meeting', 'm')
        ;

        $filter->addFiltersToQB($query);

        $pagination = $this->paginator->paginate(
            $query,
            $page,
            $limit,
            ['defaultSortFieldName' => 't.id', 'defaultSortDirection' => 'asc']
        );

        return $pagination;
    }

    public function getListTask(Filter $filter)
    {
        $query = $this->em->createQueryBuilder()
            ->select('t')
            ->from('CommonBundle\Entity\Task', 't')
            ->join('t.question', 'q')
            ->join('t.user', 'u')
            ->join('q.mayor', 'ma')
            ->join('q.meeting', 'm')
            ->orderBy('t.id')
        ;

        $filter->addFiltersToQB($query);

        return $query->getQuery()
            ->getResult();
    }
}