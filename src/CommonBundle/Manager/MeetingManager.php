<?php
namespace CommonBundle\Manager;

use CommonBundle\Entity\Meeting;
use CommonBundle\Entity\Question;
use CommonBundle\Entity\MeetingMember;
use Doctrine\ORM\EntityManager;
use UserBundle\Entity\User;
use Knp\Component\Pager\Paginator;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use CommonBundle\Event\MeetingClosedEvent;
use WebSiteBundle\Filter\Filter;
use CommonBundle\FileUploader\QuestionFileUploader;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use CommonBundle\Entity\Email;

class MeetingManager
{
    /** @var EntityManager*/
    private $em;

    /** @var Paginator*/
    private $paginator;

    /**@var EventDispatcherInterface*/
    private $dispatcher;

    /**@var QuestionFileUploader*/
    private $questionUploader;

    private $listFilters = [
        'm.num' => 'eq',
        's.name' => 'like',
        'c.name' => 'like',
        'm.datetime' => 'date',
        'm.status' => [
            'type' => 'eq',
            'options' => []
        ]
    ];

    /**
     * @param EntityManager $em
     * @param Paginator $paginator
     * @param EventDispatcherInterface $dispatcher
     * @param QuestionFileUploader $questionUploader
     */
    public function __construct(
        EntityManager $em,
        Paginator $paginator,
        EventDispatcherInterface $dispatcher,
        QuestionFileUploader $questionUploader
    ) {
        $this->em = $em;
        $this->paginator = $paginator;
        $this->dispatcher = $dispatcher;
        $this->questionUploader = $questionUploader;
    }

    public function getListFilters()
    {
        $this->listFilters['m.status']['options'] = Meeting::getStatuses();

        return $this->listFilters;
    }

    public function getListPaginate($page, $limit, Filter $filter)
    {
        $query = $this->em->getConnection()
            ->createQueryBuilder()
            ->select(
                [
                    'm.id, m.datetime, m.chairman_id, m.secretary_id, m.status, m.notifications_count, m.num',
                    'c.name chairman_name, s.name secretary_name'
                ]
             )
            ->from('meetings', 'm')
            ->join('m','users','c','m.chairman_id = c.id')
            ->join('m','users','s','m.secretary_id = s.id')
            ->where('m.deleted_at IS NULL')
            ;

        $filter->addFiltersToDbalQB($query);

        $pagination = $this->paginator->paginate(
            $query,
            $page,
            $limit,
            ['defaultSortFieldName' => 'm.status', 'defaultSortDirection' => 'asc']
        );

        return $pagination;
    }

    public function getListUserPaginate(User $user, $page, $limit, Filter $filter)
    {
        $query = $this->em->getConnection()
            ->createQueryBuilder()
            ->select(
                [
                    'm.id, m.datetime, m.chairman_id, m.secretary_id, m.status, m.num',
                    'c.name chairman_name, s.name secretary_name'
                ]
            )
            ->from('meetings', 'm')
            ->join('m','users','c','m.chairman_id = c.id')
            ->join('m','users','s','m.secretary_id = s.id')
            ->where('m.deleted_at IS NULL')
            ->where('m.id IN (
                SELECT um.meeting_id FROM user_meetings_view um WHERE um.user_id = :userId
            )')
            ->setParameter('userId', $user->getId());

        $filter->addFiltersToDbalQB($query);

        $pagination = $this->paginator->paginate(
            $query,
            $page,
            $limit,
            ['defaultSortFieldName' => 'm.status', 'defaultSortDirection' => 'asc']
        );

        return $pagination;

    }

    public function create(Meeting $meeting)
    {
        $em = $this->em;
        $questionUploader = $this->questionUploader;

        $meeting->getMembers()->forAll(function($num, MeetingMember $member) use ($meeting) {
            $member->setMeeting($meeting);

            return true;
        });

        $meeting->getQuestions()->forAll(function($num, Question $question)use ($meeting, $em, $questionUploader) {
            $question->setMeeting($meeting);

            foreach($question->getDeleteFiles() as $key => $file) {
                $question->deleteFileById($file);
            }

            foreach($question->getAddFiles() as $key => $file) {
                if ($file instanceof UploadedFile) {
                    $question->addFile($questionUploader->upload($question, $file));
                }
            }

            return true;
        });


        $this->em->persist($meeting);
        $this->em->flush();
    }

    public function addQuestionToOpenMeeting(Meeting $meeting, Question $question)
    {
        if ($meeting->getStatus() != Meeting::MEETING_STATUS_OPENED) {
            throw new \Exception('Wrong status meeting');
        }

        foreach ($question->getTasksPlaned() as $task)
        {
            $task->setQuestion($question);
        }
        $question->setMeeting($meeting);

        $this->em->persist($question);
        $this->em->flush($question);
    }

    public function start(Meeting $meeting)
    {
        if ($meeting->getStatus() == Meeting::MEETING_STATUS_CREATED) {
            $meeting->setStatus(Meeting::MEETING_STATUS_OPENED);

            $this->em->persist($meeting);
            $this->em->flush();

        } else {
            throw new \Exception('Wrong status meeting');
        }
    }

    public function close(Meeting $meeting)
    {
        if ($meeting->getStatus() == Meeting::MEETING_STATUS_OPENED) {
            $meeting->setStatus(Meeting::MEETING_STATUS_CLOSED);

            $this->dispatcher->dispatch('meeting_closed', new MeetingClosedEvent($meeting));

            $this->em->persist($meeting);
            $this->em->flush();

        } else {
            throw new \Exception('Wrong status meeting');
        }
    }

    public  function save(Meeting $meeting)
    {
        $this->em->persist($meeting);
        $this->em->flush();
    }

    public function delete(Meeting $meeting)
    {
        if (
            $meeting->getStatus() == Meeting::MEETING_STATUS_CREATED
            || $meeting->getStatus() == Meeting::MEETING_STATUS_OPENED
        ) {
            $this->em->remove($meeting);
            $this->em->flush();
        } else {
            throw new \Exception('Invalid meeting status');
        }
    }

    public function isMeetingUser(User $user,Meeting $meeting)
    {
        $stm = $this->em->getConnection()->prepare('SELECT TRUE FROM user_meetings_view WHERE user_id = :userId AND meeting_id = :meetingId');
        $stm->bindValue('userId', $user->getId());
        $stm->bindValue('meetingId', $meeting->getId());
        $stm->execute();
        $data = $stm->fetchColumn();

        return $data;
    }

    /**
     * @param int $id
     * @return Meeting|null
    */
    public function getById($id)
    {
        if ((int)$id) {
            $meeting = $this->em->getRepository('CommonBundle:Meeting')->find($id);

            if ($meeting) {
                return $meeting;
            }
        }

        return null;
    }

    /**
     * @param Meeting $meeting
     * @return array
    */
    public function getUsers(Meeting $meeting)
    {
        $sql="
          SELECT
            u.name,
            u.email,
            u.id,
            umv.status
          FROM user_meetings_view umv
           JOIN users u ON u.id = umv.user_id
          WHERE umv.meeting_id = :meetingId";

        $stm = $this->em
            ->getConnection()
            ->prepare($sql);

        $stm->bindValue('meetingId', $meeting->getId());
        $stm->execute();
        return  $stm->fetchAll();
    }

    /**
     * @param Meeting $meeting
    */
    public function increaseMeetingNotificationCount(Meeting $meeting)
    {
        $meeting->increaseNotificationsCount();

        $this->em->persist($meeting);
        $this->em->flush($meeting);
    }

    /**
     * @param Meeting $meeting
     * @param boolean|null $presence
     * @return array
    */
    public function getOrderedMembers(Meeting $meeting, $presence = null)
    {
        $qb = $this->em->createQueryBuilder()
            ->select('mm, u')
            ->from('CommonBundle:MeetingMember','mm')
            ->join('mm.user','u')
            ->where('mm.meeting = :meeting')
            ->setParameter('meeting', $meeting)
            ->orderBy('u.sortOrder','ASC')
            ->addOrderBy('u.name','ASC')
        ;

        if ($presence !== null) {

           $qb->andWhere('mm.presence = :member_presence')
               ->setParameter('member_presence', $presence)
               ;

        }

        $result = $qb->getQuery()
            ->getResult()
            ;

        return $result;
    }

    public function createUserNotificationHistory(Meeting $meeting, User $user, Email $email)
    {
        $stmt = $this->em->getConnection()->prepare('INSERT INTO meeting_user_notifications_history (meeting_id, user_id, created) VALUES (:meetingId, :userId, :created)');
        $stmt->bindValue('userId', $user->getId());
        $stmt->bindValue('meetingId', $meeting->getId());
        $stmt->bindValue('created', $email->getCreated()->format('Y-m-d H:i:s'));

        $stmt->execute();
    }

    public function getLastUserNotification(Meeting $meeting, User $user)
    {
        $stmt = $this->em->getConnection()->prepare('SELECT MAX(created) FROM meeting_user_notifications_history WHERE user_id = :userId AND meeting_id = :meetingId');
        $stmt->bindValue('userId', $user->getId());
        $stmt->bindValue('meetingId', $meeting->getId());
        $stmt->execute();

        $lastNotificationDate = $stmt->fetchColumn();

        if ($lastNotificationDate) {
            $lastNotificationDate = new \DateTime($lastNotificationDate);
        }

        return $lastNotificationDate;
    }
}