<?php
namespace CommonBundle\Manager;

use CommonBundle\Entity\TaskComment;
use UserBundle\Entity\User;
use Doctrine\ORM\EntityManager;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use CommonBundle\Entity\UserDigestHistory;

class DigestManager
{
    /** @var EntityManager */
    private $em;

    /**
     * @param EntityManager $em
    */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * @param int|bool $limit
     * @return array
    */
    public function getDailyUsers($limit = false)
    {
        $now = new \DateTime('now');
        $qb = $this->em->createQueryBuilder();

        $sub = $this->em->createQueryBuilder()
            ->select('udh')
            ->from('CommonBundle\Entity\UserDigestHistory', 'udh')
            ->where('udh.user = u')
            ->andWhere('DATE(udh.created) = :current_date');

        $query = $qb->select('u')
            ->from('UserBundle\Entity\User', 'u')
            ->where($qb->expr()->not($qb->expr()->exists($sub)))
            ->andWhere('u.status = :status_enabled')
            ->setParameter('current_date', $now->format('Y-m-d'))
            ->setParameter('status_enabled', User::STATUS_ENABLED)
            ;

        if ($limit) {
            $query->setMaxResults($limit);
        }

        $result = $query->getQuery()
        ->getResult();

       return $result;
    }

    /**
     * Задачи за 1 и 2 рабочих дня до дедлайна
     * @param User $user
     * @return array
    */
    public function getCloseDeadLineTasks(User $user)
    {
        $conn = $this->em->getConnection();
        $stmt = $conn->prepare('SELECT * FROM get_close_deadline_task(:user)');
        $stmt->bindValue('user', $user->getId());
        $stmt->execute();
        $tasks = $stmt->fetchAll();

        return $tasks;

    }

    public function createUserDigestHistory(User $user)
    {
        $history = new UserDigestHistory();
        $history->setUser($user);

        $this->em->persist($history);
        $this->em->flush($history);
    }
}