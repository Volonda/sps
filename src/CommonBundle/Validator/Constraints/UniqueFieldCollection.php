<?php
namespace CommonBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class UniqueFieldCollection extends Constraint
{
    public $field = [];
    public $message = "Значение поля не уникально";
}
