<?php
namespace CommonBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use CommonBundle\Entity\TaskPlaned;

class TaskPlanedCollectionValidator extends ConstraintValidator
{
    public function validate($values, Constraint $constraint)
    {
        /** @var TaskPlaned $taskPlaned*/
        foreach($values as $key => $taskPlaned) {

            $groups = $constraint->groups;

            if ($taskPlaned->getType() == TaskPlaned::TASK_PLANED_TYPE_FINITE) {
                $groups[] = 'TaskFiniteType';
            }

            $violations = $this->context->getValidator()->validate($taskPlaned, null, $groups);

            foreach ($violations as $violation) {

                $this->context->buildViolation($violation->getMessage())
                    ->setParameters($violation->getParameters())
                    ->setCode($violation->getCode())
                    ->setCause($violation->getCause())
                    ->setPlural($violation->getPlural())
                    ->setInvalidValue($violation->getInvalidValue())
                    ->atPath('[' . $key . '].' . $violation->getPropertyPath())
                    ->addViolation();
            }
        }
    }
}