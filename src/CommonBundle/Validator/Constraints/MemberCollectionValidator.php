<?php
namespace CommonBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use CommonBundle\Entity\MeetingMember;

class MemberCollectionValidator extends ConstraintValidator
{
    public function validate($values, Constraint $constraint)
    {
        /** @var MeetingMember $meetingMember*/
        foreach($values as $key => $meetingMember) {

            $groups = $constraint->groups;

            if ($meetingMember->getPresence() == false) {
                $groups[] = 'NonPresence';
            }

            $violations = $this->context->getValidator()->validate($meetingMember, null, $groups);

            foreach ($violations as $violation) {

                $this->context->buildViolation($violation->getMessage())
                    ->setParameters($violation->getParameters())
                    ->setCode($violation->getCode())
                    ->setCause($violation->getCause())
                    ->setPlural($violation->getPlural())
                    ->setInvalidValue($violation->getInvalidValue())
                    ->atPath('[' . $key . '].' . $violation->getPropertyPath())
                    ->addViolation();
            }
        }
    }
}