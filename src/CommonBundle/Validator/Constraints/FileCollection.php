<?php
namespace CommonBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class FileCollection extends Constraint
{
    public $mimeTypes = array();
    public $mimeTypesMessage = "";
    public $maxSize = "";
}