<?php
namespace CommonBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class TaskEditMessage extends Constraint
{
    public $message = "Значение не должно быть пустым.";
}
