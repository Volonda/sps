<?php
namespace CommonBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Constraints\File;

class FileCollectionValidator extends ConstraintValidator
{
    public function validate($files, Constraint $constraint)
    {
        foreach($files as $file) {

            $violations = $this->context->getValidator()->validate($file, new File([
                'mimeTypes' => $constraint->mimeTypes,
                'mimeTypesMessage' => $constraint->mimeTypesMessage,
                'maxSize' => $constraint->maxSize,
            ]));

            foreach ($violations as $key=>$violation) {

                $this->context->buildViolation($violation->getMessage())
                    ->setParameters($violation->getParameters())
                    ->setCode($violation->getCode())
                    ->setCause($violation->getCause())
                    ->setPlural($violation->getPlural())
                    ->setInvalidValue($violation->getInvalidValue())
                    ->atPath('[' . $key . ']' )
                    ->addViolation();
            }

        }
    }
}