<?php
namespace CommonBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\Constraints\Valid;

class ValidGroupAwareValidator extends ConstraintValidator
{
    public function validate($value, Constraint $constraint)
    {
        if (!$constraint instanceof ValidGroupAware) {
            throw new UnexpectedTypeException($constraint, __NAMESPACE__.'\ValidGroupAware');
        }

        $violations = $this->context->getValidator()->validate($value, [new Valid()], $constraint->validation_groups );

        /** @var ConstraintViolation[] $violations */
        foreach ($violations as $violation) {
            $this->context->buildViolation($violation->getMessage())
                ->setParameters($violation->getParameters())
                ->setCode($violation->getCode())
                ->setCause($violation->getCause())
                ->setPlural($violation->getPlural())
                ->setInvalidValue($violation->getInvalidValue())
                ->atPath($violation->getPropertyPath())
                ->addViolation();
        }
    }
}