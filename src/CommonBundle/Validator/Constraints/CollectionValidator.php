<?php
namespace CommonBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use CommonBundle\Entity\MeetingMember;

class CollectionValidator extends ConstraintValidator
{
    public function validate($values, Constraint $constraint)
    {
        foreach($values as $key => $value) {

            $violations = $this->context->getValidator()->validate($value, null, $constraint->groups);

            foreach ($violations as $violation) {

                $this->context->buildViolation($violation->getMessage())
                    ->setParameters($violation->getParameters())
                    ->setCode($violation->getCode())
                    ->setCause($violation->getCause())
                    ->setPlural($violation->getPlural())
                    ->setInvalidValue($violation->getInvalidValue())
                    ->atPath('[' . $key . '].' . $violation->getPropertyPath())
                    ->addViolation();
            }
        }
    }
}