<?php
namespace CommonBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\PropertyAccess\PropertyAccess;

class UniqueFieldCollectionValidator extends ConstraintValidator
{
    public function validate($items, Constraint $constraint)
    {
        $accessor = PropertyAccess::createPropertyAccessor();

        $values = [];
        foreach($items as $key => $item)
        {
            $value = $accessor->getValue($item, $constraint->field);
            if(in_array($value, $values)) {

                $this->context->buildViolation($constraint->message)
                    ->atPath('[' . $key .'].date')
                    ->addViolation();

            } else {
                $values[] = $accessor->getValue($item, $constraint->field);
            }
        }

    }
}