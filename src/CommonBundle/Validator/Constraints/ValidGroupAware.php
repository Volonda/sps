<?php
namespace CommonBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 * @Target({"PROPERTY", "METHOD", "ANNOTATION"})
 */
class ValidGroupAware extends Constraint
{
    public $validation_groups = ['Default'];
}