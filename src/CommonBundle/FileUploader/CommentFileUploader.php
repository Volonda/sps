<?php
namespace CommonBundle\FileUploader;

use Symfony\Component\HttpFoundation\File\UploadedFile;
use CommonBundle\Entity\File;
use CommonBundle\Entity\TaskLog;
use Symfony\Bridge\Monolog\Logger;

class CommentFileUploader
{
    /**@var Logger*/
    private $logger;

    public function __construct(Logger $logger)
    {
        $this->logger = $logger;
    }

    public function upload(TaskLog $logComment, UploadedFile $uploadedFile)
    {
        $targetDir = File::FILE_ROOT_DIR. '/' . $logComment->getDirPath();
        try {
            if (is_dir($targetDir) || mkdir($targetDir)) {
                $fileName = md5(uniqid()) . '.' . $uploadedFile->guessExtension();

                $file = new File();
                $file->setMimeType($uploadedFile->getClientMimeType());
                $file->setOriginalName($uploadedFile->getClientOriginalName());
                $file->setOriginalExtension($uploadedFile->getClientOriginalExtension());
                $file->setSize($uploadedFile->getSize());
                $file->setName($fileName);
                $file->setPath($logComment->getDirPath());

                $uploadedFile->move($targetDir, $fileName);
                return $file;
            } else {
                throw new \Exception('Unable to make dir ' . $targetDir);
            }
        } catch(\Exception $e) {
            $this->logger->addError($e->getMessage());
        }

    }
}