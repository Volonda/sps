<?php
namespace CommonBundle\Services;


use CommonBundle\Entity\Question;
use Doctrine\ORM\EntityManager;


class QuestionManager
{
    /**
     *
     * @var EntityManager
     */
    protected $em;

    public function __construct(EntityManager $entityManager)
    {
        $this->em = $entityManager;
    }


    public function getList($isArray = false)
    {
        $hydrationMode = \Doctrine\ORM\Query::HYDRATE_OBJECT;
        if($isArray) {
            $hydrationMode = \Doctrine\ORM\Query::HYDRATE_ARRAY;
        }

        return $this->em->getRepository('CommonBundle:Question')->listAll($hydrationMode);

    }
}