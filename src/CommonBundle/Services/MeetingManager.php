<?php
namespace CommonBundle\Services;


use CommonBundle\Entity\Meeting;
use UserBundle\Entity\Users;
use CommonBundle\Entity\Meetingtatus;
use Doctrine\ORM\EntityManager;


class MeetingManager
{
    /**
     *
     * @var EntityManager
     */
    protected $em;

    public function __construct(EntityManager $entityManager)
    {
        $this->em = $entityManager;
    }
    public function addMeeting(
        Meeting $meeting, Users $chainman, Users $secretary,
        Meetingtatus $status, $place
    ) {
        $meeting->setPlace($place);
        $meeting->setChairman($chainman);
        $meeting->setSecretary($secretary);
        $meeting->setStatus($status);
       // $meeting->setDatetime();
       // $meeting->setNomer();

        $this->em->flush();

    }

    public function getList($isArray = false)
    {
        $hydrationMode = \Doctrine\ORM\Query::HYDRATE_OBJECT;
        if($isArray) {
            $hydrationMode = \Doctrine\ORM\Query::HYDRATE_ARRAY;
        }

        return $this->em->getRepository('CommonBundle:Meeting')->listAll($hydrationMode);

    }


}