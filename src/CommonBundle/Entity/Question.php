<?php

namespace CommonBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use CommonBundle\Validator\Constraints as CommonAssert;
use UserBundle\Entity\User;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;


/**
 * Question
 *
 * @ORM\Table(name="questions")
 * @ORM\Entity(repositoryClass="CommonBundle\Repository\QuestionRepository")
 * @Gedmo\SoftDeleteable()
 */
class Question
{
    use SoftDeleteableEntity;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="questions_id_seq", allocationSize=1, initialValue=1)
     */
    private $id;

    /**
     * @var string
     * @Assert\NotBlank(groups={"MeetingCreateForm","QuestionEditForm"})
     * @ORM\Column(name="text", type="text", nullable=false)
     */
    private $text;

    /**
     * @var string
     *
     * @ORM\Column(name="text_last", type="text", nullable=true)
     */
    private $textLast;

    /**
     * @var \DateTime
     * @ORM\Column(name="text_updated", type="datetime", nullable=true)
     */
    private $textUpdated;

    /**
     * @var integer
     *
     * @ORM\Column(name="status", type="integer", nullable=false)
     */
    private $status;

    /**
     * @var string
     * @ORM\Column(name="answer", type="text", nullable=true)
     */
    private $answer;

    /**
     * @var string
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;


    /**
     * @var Meeting
     *
     * @ORM\ManyToOne(targetEntity="Meeting", inversedBy="questions")
     * @ORM\JoinColumn(name="meeting_id", referencedColumnName="id")
     */
    private $meeting;

    /**
     * @var Collection
     *
     * @ORM\ManyToMany(targetEntity="UserBundle\Entity\User", inversedBy="questions")
     * @ORM\JoinTable(name="user_questions")
     * @ORM\OrderBy({"sortOrder"="ASC"})
     * @Assert\Count(
     *      min = "1",
     *      minMessage = "Должен быть хотя бы 1 докладчик",
     *      groups={"MeetingCreateForm", "QuestionEditForm"}
     * )
     */
    private $users;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="CommonBundle\Entity\Task", mappedBy="question", cascade={"all"})
     * @ORM\OrderBy({"id" = "ASC"})
     */
    private $tasks;

    /**
     * @var ArrayCollection
     * @CommonAssert\TaskPlanedCollection(groups={"QuestionEditForm"})
     * @ORM\OneToMany(targetEntity="CommonBundle\Entity\TaskPlaned", mappedBy="question", cascade={"all"}, orphanRemoval=true)
     * @ORM\OrderBy({"id" = "ASC"})
     */
    private $tasksPlaned;

    /**
     * @var User
     *
     * @Assert\NotBlank(groups={"QuestionEditForm"})
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\User")
     * @ORM\JoinColumn(name="mayor_id", referencedColumnName="id", nullable=true)
     */
    private $mayor;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="CommonBundle\Entity\File", cascade={"persist"})
     * @ORM\JoinTable(name="question_files",
     *      joinColumns={@ORM\JoinColumn(name="question_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="file_id", referencedColumnName="id")}
     *      )
     */

    private $files;

    /**
     * @var \DateTime $created
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime", name="created", nullable=false)
     */
    private $created;

    private $dirPath =  'uploads/question';

    private $addFiles;

    private $deleteFiles;

    const QUESTION_STATUS_CREATED = 1;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->setUsers(new ArrayCollection());
        $this->setStatus(self::QUESTION_STATUS_CREATED);
        $this->setFiles(new ArrayCollection());
        $this->setAddFiles(new ArrayCollection());
        $this->setDeleteFiles(new ArrayCollection());
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set text
     *
     * @param string $text
     *
     * @return Question
     */
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * Get text
     *
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * Set status
     *
     * @param integer $status
     *
     * @return Question
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set answer
     *
     * @param string $answer
     *
     * @return Question
     */
    public function setAnswer($answer)
    {
        $this->answer = $answer;

        return $this;
    }

    /**
     * Get answer
     *
     * @return string
     */
    public function getAnswer()
    {
        return $this->answer;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Question
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set meeting
     *
     * @param Meeting $meeting
     *
     * @return Question
     */
    public function setMeeting(Meeting $meeting = null)
    {
        $this->meeting = $meeting;

        return $this;
    }

    /**
     * Get meeting
     *
     * @return Meeting
     */
    public function getMeeting()
    {
        return $this->meeting;
    }

    /**
     * Add user
     *
     * @param User $user
     *
     * @return Question
     */
    public function addUser(User $user)
    {
        $this->getUsers()->add($user);

        return $this;
    }

    /**
     * Remove user
     *
     * @param User $user
     */
    public function removeUser(User $user)
    {
        $this->getUsers()->removeElement($user);
    }

    /**
     * Get users
     *
     * @return ArrayCollection
     */
    public function getUsers()
    {
        return $this->users;
    }

    /**
     * Set users
     *
     * @param ArrayCollection $users
     */
    public function setUsers(ArrayCollection $users)
    {
        $this->users = $users;
    }

    /**
     * @return ArrayCollection
     */
    public function getTasks()
    {
        return $this->tasks;
    }

    /**
     * @param ArrayCollection $tasks
     */
    public function setTasks($tasks)
    {
        $this->tasks = $tasks;
    }

    /**
     * @param Task $task
     */
    public function addTask(Task $task)
    {
        $this->getTasks()->add($task);
    }

    /**
     * @return User
     */
    public function getMayor()
    {
        return $this->mayor;
    }

    /**
     * @param User $mayor
     */
    public function setMayor(User $mayor = null)
    {
        $this->mayor = $mayor;
    }

    /**
     * @return ArrayCollection
     */
    public function getTasksPlaned()
    {
        return $this->tasksPlaned;
    }

    /**
     * @param ArrayCollection $tasksPlaned
     */
    public function setTasksPlaned($tasksPlaned)
    {
        $this->tasksPlaned = $tasksPlaned;
    }

    /**
     * @return ArrayCollection
     */
    public function getFiles()
    {
        return $this->files;
    }

    /**
     * @param ArrayCollection $files
     */
    public function setFiles($files) {
        if (!$files instanceof Collection) {
            $files = new ArrayCollection($files);
        }

        $this->files = $files->filter(function($value){
            return !empty($value);
        });
    }

    public function getDirPath()
    {
        return  $this->dirPath  . '/' . $this->getId();
    }

    public function getAddFiles()
    {
        $files = $this->addFiles;

        if (!$files) {
            $files = new ArrayCollection();
        }

        return $files;
    }

    public function setAddFiles($addFiles)
    {
        $this->addFiles = $addFiles;
    }

    public function addFile(File $file)
    {
        $this->getFiles()->add($file);
    }

    /**
     * @return ArrayCollection
     */
    public function getDeleteFiles()
    {
        $files = $this->deleteFiles;

        if(!$files) {
            $files = new ArrayCollection();
        }

        return $files;
    }

    /**
     * @param mixed $deleteFiles
     */
    public function setDeleteFiles($deleteFiles)
    {
        $this->deleteFiles = $deleteFiles;
    }

    /**
     * @param integer $id
    */
    public function deleteFileById($id)
    {
        $files = $this->getFiles();

        $files->forAll(function($key, $file) use ($files, $id){
            if ($file instanceof File) {
                if ($file->getId() == $id) {
                    $files->remove($key);

                    return false;
                }
            }
            return true;
        });
    }

    /**
     * @return string
     */
    public function getTextLast()
    {
        return $this->textLast;
    }

    /**
     * @param string $textLast
     */
    public function setTextLast($textLast)
    {
        $this->textLast = $textLast;
    }

    /**
     * @return \DateTime
     */
    public function getTextUpdated()
    {
        return $this->textUpdated;
    }

    /**
     * @param \DateTime $textUpdated
     */
    public function setTextUpdated($textUpdated)
    {
        $this->textUpdated = $textUpdated;
    }

    /**
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

}
