<?php
namespace CommonBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\Criteria;
use UserBundle\Entity\User;

/**
 * Task
 *
 * @ORM\Table(name="tasks")
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="type", type="task_type")
 * @ORM\DiscriminatorMap({"finite" = "CommonBundle\Entity\FiniteTask", "repeat" = "CommonBundle\Entity\RepeatTask"})
 * @ORM\Entity(repositoryClass="CommonBundle\Repository\TaskRepository")
 */
abstract class Task
{
    const TASK_STATUS_PROCESSING = 'processing';
    const TASK_STATUS_COMPLETED = 'completed';
    const TASK_STATUS_APPROVED = 'approved';
    const TASK_STATUS_CREATED = 'created';
    const TASK_STATUS_TESTING = 'testing';

    const PSEUDO_STATUS_NOT_COMPLETED = 'not_completed';

    const TASK_TYPE_FINITE = 'finite';
    const TASK_TYPE_REPEAT = 'repeat';

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="task_id_seq", allocationSize=1, initialValue=1)
     */
    private $id;

    /**
     * @var string
     * @Assert\NotBlank(groups={"Default", "AdminEdit"})
     * @ORM\Column(name="text", type="text", nullable=true)
     */
    private $text;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="start_datetime", type="datetime", nullable=true)
     */
    private $startDatetime;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="end_datetime", type="datetime", nullable=true)
     */
    private $endDatetime;

    /**
     * @var string
     * @Assert\NotBlank(groups={"Default", "AdminEdit"})
     * @ORM\Column(name="status", type="task_status", nullable=false)
     */
    private $status;

    /**
     * @var Question
     * @ORM\ManyToOne(targetEntity="CommonBundle\Entity\Question", inversedBy="tasks")
     * @ORM\JoinColumn(name="question_id", referencedColumnName="id", onDelete="RESTRICT", nullable=false)
     */
    private $question;


    /**
     * @var Collection
     * @Assert\NotBlank(groups={"Default", "AdminEdit"})
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\User", inversedBy="tasks", cascade={"persist"})
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", onDelete="RESTRICT", nullable=false)
     *
     */
    private $user;

    /**
     * @ORM\OneToMany(targetEntity="CommonBundle\Entity\TaskLog", mappedBy="task", cascade={"all"})
     */
    private $log;

    /**
     * @ORM\OneToMany(targetEntity="CommonBundle\Entity\TaskNotification", mappedBy="task", cascade={"all"}, orphanRemoval=true)
     */
    private $notifications;

    /**
     * @var \DateTime
     * @Assert\NotBlank(groups={"Default", "AdminEdit"})
     * @Assert\GreaterThan("+0 hours", groups={"Default"})
     * @ORM\Column(name="deadline_datetime", type="datetime", nullable=false)
     */
    private $deadlineDatetime;

    /**
     * @var boolean
     *
     * @ORM\Column(name="admin_status_view", type="boolean", nullable=false)
     */
    private $adminStatusView;

    /**
     * @var boolean
     *
     * @ORM\Column(name="mayor_status_view", type="boolean", nullable=false)
     */
    private $mayorStatusView;

    /**
     * @var boolean
     *
     * @ORM\Column(name="user_status_view", type="boolean", nullable=false)
     */
    private $userStatusView;



    static function getFiniteStatuses()
    {
        return [
            self::TASK_STATUS_CREATED => 'Новая',
            self::TASK_STATUS_PROCESSING => 'В работе',
            self::TASK_STATUS_TESTING => 'Тестирование',
            self::TASK_STATUS_APPROVED => 'Утверждена',
            self::TASK_STATUS_COMPLETED => 'Завершена'
        ];
    }

    static function getStatuses(Task $task = null)
    {
        if ( $task instanceof FiniteTask) {

            return self::getFiniteStatuses();

        } elseif( $task instanceof RepeatTask) {

            return self::getRepeatStatuses();

        } else {
            return [
                self::TASK_STATUS_CREATED => 'Новая',
                self::TASK_STATUS_PROCESSING => 'В работе',
                self::TASK_STATUS_TESTING => 'Тестирование',
                self::TASK_STATUS_APPROVED => 'Утверждена',
                self::TASK_STATUS_COMPLETED => 'Завершена'
            ];
        }
    }

    static function getRepeatStatuses()
    {
        return [
            self::TASK_STATUS_PROCESSING => 'В работе',
            self::TASK_STATUS_COMPLETED => 'Завершена'
        ];
    }

    static function getMayorStatuses()
    {
        return [
            self::TASK_STATUS_CREATED => 'Новая',
            self::TASK_STATUS_APPROVED => 'Утверждена',
        ];
    }
    public abstract function getType();

    public static function getTypeNames()
    {
        return [
            self::TASK_TYPE_REPEAT => 'Постоянная задача',
            self::TASK_TYPE_FINITE => 'Задача со сроком исполнения'
        ];
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->setNotifications( new ArrayCollection());
        $this->setMayorStatusView(true);
        $this->setUserStatusView(true);
        $this->setAdminStatusView(true);
    }

    public function __clone()
    {
        $this->id = null;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set text
     *
     * @param string $text
     *
     * @return Task
     */
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * Get text
     *
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Task
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set startDatetime
     *
     * @param \DateTime $startDatetime
     *
     * @return Task
     */
    public function setStartDatetime($startDatetime)
    {
        $this->startDatetime = $startDatetime;

        return $this;
    }

    /**
     * Get startDatetime
     *
     * @return \DateTime
     */
    public function getStartDatetime()
    {
        return $this->startDatetime;
    }

    /**
     * Set status
     *
     * @param string $status
     *
     * @return Task
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set question
     *
     * @param Question $question
     *
     * @return Task
     */
    public function setQuestion(Question $question)
    {
        $this->question = $question;

        return $this;
    }

    /**
     * Get question
     *
     * @return Question
     */
    public function getQuestion()
    {
        return $this->question;
    }

    /**
     * Get user
     *
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser(User $user)
    {
        $this->user = $user;
    }

    public  function  statusName()
    {
        return self::getStatuses($this)[$this->status];
    }

    /**
     * @return ArrayCollection
     */
    public function getLog()
    {
        return $this->log;
    }

    /**
     * @param mixed $log
     */
    public function setLog(ArrayCollection $log)
    {
        $this->log = $log;
    }

    public function addTaskLog(TaskLog $log)
    {
        $this->getLog()->add($log);
    }

    public function getStatusName()
    {
        return self::getStatuses($this)[$this->getStatus()];
    }

    public function getComments()
    {
        return $this->getLog()->filter(function(TaskLog $log){

            return $log->getText();
        });
    }

    /**
     * @return Collection
     */
    public function getNotifications()
    {
        return $this->notifications;
    }

    /**
     * @param Collection $notifications
     */
    public function setNotifications(Collection $notifications)
    {
        $this->notifications = $notifications;
    }

    public function getSingleNotifications()
    {
        $sort = Criteria::create();
        $sort->orderBy(Array(
            'date' => Criteria::ASC
        ));

        return $this->getNotifications()->filter(function($notification){
            return $notification instanceof TaskSingleNotification;
        })->matching($sort);
    }

    public function setSingleNotifications(ArrayCollection $singleNotifications)
    {
        $repeatNotification = $this->getRepeatNotifications();

        if ($repeatNotification) {
            $notifications = new ArrayCollection([$this->getRepeatNotifications()]);
        } else {
            $notifications = new ArrayCollection([]);
        }

        $singleNotifications->forAll(function ($num, TaskSingleNotification $singleNotification) use ($notifications) {
            $notifications->add($singleNotification);

            return true;
        });

        $this->setNotifications($notifications);
    }

    public function setRepeatNotifications(TaskRepeatNotification $repeatNotifications)
    {
        $notifications = $this->getSingleNotifications();

        if ($repeatNotifications) {
            $notifications->add($repeatNotifications);
        }

        $this->setNotifications($notifications);
    }

    public function getRepeatNotifications()
    {
        $notification = $this->getNotifications()->filter(function($notification){
            return $notification instanceof TaskRepeatNotification;
        })->first();

       if ($notification){
            return $notification;
       }

        return null;
    }

    /**
     * Set endDatetime
     *
     * @param \DateTime $endDatetime
     *
     * @return Task
     */
    public function setEndDatetime($endDatetime)
    {
        $this->endDatetime = $endDatetime;

        return $this;
    }

    /**
     * Get endDatetime
     *
     * @return \DateTime
     */
    public function getEndDatetime()
    {
        return $this->endDatetime;
    }


    /**
     * @return \DateTime
     */
    public function getDeadlineDatetime()
    {
        return $this->deadlineDatetime;
    }

    /**
     * @param \DateTime $deadlineDatetime
     */
    public function setDeadlineDatetime($deadlineDatetime)
    {
        $this->deadlineDatetime = $deadlineDatetime;
    }

    /**
     * @return boolean
     */
    public function isAdminStatusView()
    {
        return $this->adminStatusView;
    }

    /**
     * @param boolean $adminStatusView
     */
    public function setAdminStatusView($adminStatusView)
    {
        $this->adminStatusView = $adminStatusView;
    }

    /**
     * @return boolean
     */
    public function isMayorStatusView()
    {
        return $this->mayorStatusView;
    }

    /**
     * @param boolean $mayorStatusView
     */
    public function setMayorStatusView($mayorStatusView)
    {
        $this->mayorStatusView = $mayorStatusView;
    }

    /**
     * @return boolean
     */
    public function isUserStatusView()
    {
        return $this->userStatusView;
    }

    /**
     * @param boolean $userStatusView
     */
    public function setUserStatusView($userStatusView)
    {
        $this->userStatusView = $userStatusView;
    }

}