<?php

namespace CommonBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use CommonBundle\Validator\Constraints as CommonAssert;
use UserBundle\Entity\User;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;

/**
 * Meeting
 *
 * @ORM\Table(name="meetings")
 * @ORM\Entity(repositoryClass="CommonBundle\Repository\MeetingRepository")
 * @Gedmo\SoftDeleteable()
 */
class Meeting
{
    use SoftDeleteableEntity;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="meetings_id_seq", allocationSize=1, initialValue=1)
     */
    private $id;

    /**
     * @var \DateTime
     * @Assert\NotBlank(groups={"MeetingCreateForm"})
     * @ORM\Column(name="datetime", type="datetime", nullable=true)
     */
    private $datetime;

    /**
     * @var \DateTime
     * @ORM\Column(name="datetime_updated", type="datetime", nullable=false)
     */
    private $datetimeUpdated;

    /**
     * @var \DateTime
     * @ORM\Column(name="datetime_last", type="datetime", nullable=true)
     */
    private $datetimeLast;

    /**
     * @var MeetingPlace
     * @ORM\ManyToOne(targetEntity="CommonBundle\Entity\MeetingPlace", cascade={"persist"})
     * @ORM\JoinColumn(name="meeting_place_id", referencedColumnName="id")
     */
    private $place;

    /**
     * @var MeetingPlace
     * @ORM\ManyToOne(targetEntity="CommonBundle\Entity\MeetingPlace")
     * @ORM\JoinColumn(name="meeting_place_id_last", referencedColumnName="id")
     */
    private $placeLast;

    /**
     * @var \DateTime
     * @ORM\Column(name="meeting_place_id_updated", type="datetime", nullable=true)
    */
    private $placeUpdated;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="meeting_status", nullable=false)
     */
    private $status;

    /**
     * @var User
     * @Assert\NotBlank(groups={"MeetingCreateForm"})
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="chairman_id", referencedColumnName="id", nullable=false)
     * })
     */
    private $chairman;

    /**
     * @var User
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="chairman_id_last", referencedColumnName="id", nullable=false)
     * })
     */
    private $chairmanLast;

    /**
     * @var \DateTime
     * @ORM\Column(name="chairman_id_updated", type="datetime", nullable=true)
     */
    private $chairmanUpdated;

    /**
     * @var User
     * @Assert\NotBlank(groups={"MeetingCreateForm"})
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="secretary_id", referencedColumnName="id", nullable=false)
     * })
     */
    private $secretary;

    /**
     * @var User
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="secretary_id_last", referencedColumnName="id", nullable=false)
     * })
     */
    private $secretaryLast;

    /**
     * @var \DateTime
     * @ORM\Column(name="secretary_id_updated", type="datetime", nullable=true)
     */
    private $secretaryUpdated;

    /**
     * @var ArrayCollection
     * @CommonAssert\Collection(groups={"MeetingCreateForm"})
     * @ORM\OneToMany(targetEntity="CommonBundle\Entity\Question", mappedBy="meeting", cascade={"all"}, orphanRemoval=true)
     * @ORM\OrderBy({"id" = "ASC"})
     */
    private $questions;

    /**
     * @var ArrayCollection
     * @CommonAssert\MemberCollection(groups={"MeetingCreateForm","MeetingMembersEditFrom"})
     * @Assert\Count(
     *     min = "1",
     *     minMessage = "Должен быть хотябы 1 участник",
     *     groups={"MeetingCreateForm"}
     * )
     * @ORM\OneToMany(targetEntity="CommonBundle\Entity\MeetingMember", mappedBy="meeting", cascade={"all"}, orphanRemoval=true)
     * @ORM\OrderBy({"id" = "ASC"})
     */
    private $members;

    /**
     * @var integer
     *
     * @ORM\Column(name="notifications_count", type="integer", nullable=false)
     */
    private $notificationsCount;

    /**
     * @var string
     *
     * @ORM\Column(name="num", type="integer", nullable=true)
     */
    private $num;

    const MEETING_STATUS_CREATED = 'created';
    const MEETING_STATUS_OPENED = 'opened';
    const MEETING_STATUS_CLOSED = 'closed';

    const MEETING_USER_STATUS_REPORTER = 'reporter';
    const MEETING_USER_STATUS_USER_TASK = 'user_task';
    const MEETING_USER_STATUS_USER_MAYOR = 'mayor';
    const MEETING_USER_STATUS_USER_SECRETARY = 'secretary';
    const MEETING_USER_STATUS_USER_CHAIRMAN = 'chairman';
    const MEETING_USER_STATUS_USER_MEMBER = 'member';

    public function __construct()
    {
        $this->setQuestions(new ArrayCollection());
        $this->setMembers(new ArrayCollection());
        $this->setStatus(self::MEETING_STATUS_CREATED);
        $this->setNotificationsCount(0);

    }

    public static function getStatuses()
    {
        return [
            self::MEETING_STATUS_CREATED => 'Планируется',
            self::MEETING_STATUS_OPENED => 'Открыто',
            self::MEETING_STATUS_CLOSED => 'Закрыто'
        ];
    }

    public static function getUserStatuses()
    {
        return [
            self::MEETING_USER_STATUS_REPORTER => 'Докладчик',
            self::MEETING_USER_STATUS_USER_TASK => 'Ответственный за задачу',
            self::MEETING_USER_STATUS_USER_MAYOR => 'Мэр',
            self::MEETING_USER_STATUS_USER_SECRETARY => 'Секретарь',
            self::MEETING_USER_STATUS_USER_CHAIRMAN => 'Председатель',
            self::MEETING_USER_STATUS_USER_MEMBER => 'Участник',
        ];
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set datetime
     *
     * @param \DateTime $datetime
     *
     * @return Meeting
     */
    public function setDatetime($datetime)
    {
        $this->datetime = $datetime;

        return $this;
    }

    /**
     * Get datetime
     *
     * @return \DateTime
     */
    public function getDatetime()
    {
        return $this->datetime;
    }

    /**
     * Set place
     *
     * @param MeetingPlace $place
     *
     * @return Meeting
     */
    public function setPlace(MeetingPlace $place)
    {
        $this->place = $place;

        return $this;
    }

    /**
     * Get place
     *
     * @return MeetingPlace
     */
    public function getPlace()
    {
        return $this->place;
    }


    /**
     * Set status
     *
     * @param string $status
     *
     * @return Meeting
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set chairman
     *
     * @param User $chairman
     *
     * @return Meeting
     */
    public function setChairman(User $chairman = null)
    {
        $this->chairman = $chairman;

        return $this;
    }

    /**
     * Get chairman
     *
     * @return User
     */
    public function getChairman()
    {
        return $this->chairman;
    }

    /**
     * Set secretary
     *
     * @param User $secretary
     *
     * @return Meeting
     */
    public function setSecretary(User $secretary = null)
    {
        $this->secretary = $secretary;

        return $this;
    }

    /**
     * Get secretary
     *
     * @return User
     */
    public function getSecretary()
    {
        return $this->secretary;
    }

    /**
     * @return ArrayCollection
     */
    public function getQuestions()
    {
       return $this->questions;
    }

    /**
     * @param ArrayCollection $questions
     */
    public function setQuestions(ArrayCollection $questions)
    {
        $this->questions = $questions;
    }

    /**
     * @return ArrayCollection
     */
    public function getMembers()
    {
        return $this->members;
    }

    /**
     * @param ArrayCollection $members
     */
    public function setMembers(ArrayCollection $members)
    {
        $this->members = $members;
    }

    public function getStatusName()
    {
        $statuses = self::getStatuses();
        return $statuses[$this->status];
    }

    /**
     * @return integer
     */
    public function getNotificationsCount()
    {
        return $this->notificationsCount;
    }

    /**
     * @param integer $notificationsCount
     */
    public function setNotificationsCount($notificationsCount)
    {
        $this->notificationsCount = $notificationsCount;
    }

    public function increaseNotificationsCount()
    {
        $this->notificationsCount ++ ;
    }

    /**
     * @return string
     */
    public function getNum()
    {
        return $this->num;
    }

    /**
     * @param string $num
     */
    public function setNum($num)
    {
        $this->num = $num;
    }

    /**
     * @return string
    */
    public function getDatetimeString()
    {
        return $this->datetime->format('d.m.Y H:i');
    }

    /**
     * @return MeetingPlace
     */
    public function getPlaceLast()
    {
        return $this->placeLast;
    }

    /**
     * @param MeetingPlace $placeLast
     */
    public function setPlaceLast($placeLast)
    {
        $this->placeLast = $placeLast;
    }

    /**
     * @return User
     */
    public function getChairmanLast()
    {
        return $this->chairmanLast;
    }

    /**
     * @param User $chairmanLast
     */
    public function setChairmanLast($chairmanLast)
    {
        $this->chairmanLast = $chairmanLast;
    }

    /**
     * @return User
     */
    public function getSecretaryLast()
    {
        return $this->secretaryLast;
    }

    /**
     * @param User $secretaryLast
     */
    public function setSecretaryLast($secretaryLast)
    {
        $this->secretaryLast = $secretaryLast;
    }

    /**
     * @return mixed
     */
    public function getPlaceUpdated()
    {
        return $this->placeUpdated;
    }

    /**
     * @param mixed $placeUpdated
     */
    public function setPlaceUpdated($placeUpdated)
    {
        $this->placeUpdated = $placeUpdated;
    }

    /**
     * @return \DateTime
     */
    public function getChairmanUpdated()
    {
        return $this->chairmanUpdated;
    }

    /**
     * @param \DateTime $chairmanUpdated
     */
    public function setChairmanUpdated($chairmanUpdated)
    {
        $this->chairmanUpdated = $chairmanUpdated;
    }

    /**
     * @return \DateTime
     */
    public function getSecretaryUpdated()
    {
        return $this->secretaryUpdated;
    }

    /**
     * @param \DateTime $secretaryUpdated
     */
    public function setSecretaryUpdated($secretaryUpdated)
    {
        $this->secretaryUpdated = $secretaryUpdated;
    }

    /**
     * @return \DateTime
     */
    public function getDatetimeUpdated()
    {
        return $this->datetimeUpdated;
    }

    /**
     * @param \DateTime $datetimeUpdated
     */
    public function setDatetimeUpdated($datetimeUpdated)
    {
        $this->datetimeUpdated = $datetimeUpdated;
    }

    /**
     * @return \DateTime
     */
    public function getDatetimeLast()
    {
        return $this->datetimeLast;
    }

    /**
     * @param \DateTime $datetimeLast
     */
    public function setDatetimeLast($datetimeLast)
    {
        $this->datetimeLast = $datetimeLast;
    }

}