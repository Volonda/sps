<?php

namespace CommonBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * TaskNotificationHistory
 *
 * @ORM\Table(name="tasks_notifications_history")
 * @ORM\Entity()
 */
class TaskNotificationHistory
{
    const TASK_HISTORY_NOTIFICATION_TYPE_SINGLE = 'single';
    const TASK_HISTORY_NOTIFICATION_TYPE_REPEAT = 'repeat';

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime $created
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime", name="created", nullable=false)
     */
    private $created;

    /**
     * @var string
     *
     * @ORM\Column(type="string", name="type", nullable=false)
     */
    private $type;


    /**
     * @ORM\ManyToOne(targetEntity="CommonBundle\Entity\Task")
     * @@ORM\JoinColumn(name="task_id", referencedColumnName="id")
     */
    private $task;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param \DateTime $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getTask()
    {
        return $this->task;
    }

    /**
     * @param mixed $task
     */
    public function setTask($task)
    {
        $this->task = $task;
    }
}