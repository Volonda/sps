<?php
namespace CommonBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TaskSingleNotification
 *
 * @ORM\Entity()
 */
class TaskSingleNotification extends TaskNotification
{
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="date", nullable=true)
     */
    private $date;

    /**
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param \DateTime $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }
}