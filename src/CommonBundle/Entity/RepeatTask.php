<?php
namespace CommonBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * RepeatTask
 *
 * @ORM\Entity(repositoryClass="CommonBundle\Repository\TaskRepository")
 */
class RepeatTask extends Task
{
    const PERIOD_TYPE_DAY = 'day';
    const PERIOD_TYPE_WEEK = 'week';
    const PERIOD_TYPE_MOUTH = 'month';

    /**
     * @var string
     * @Assert\NotBlank(groups={"Default", "AdminEdit"})
     * @ORM\Column(name="period_type", type="task_repeat_period_type", nullable=true)
     */
    private $periodType;

    /**
     * @var integer
     *
     * @Assert\Type("integer", message="Введите число", groups={"Default", "AdminEdit"})
     * @Assert\NotBlank(groups={"Default", "AdminEdit"})
     * @Assert\GreaterThanOrEqual("1", groups={"Default", "AdminEdit"})
     * @Assert\LessThanOrEqual("100", groups={"Default", "AdminEdit"})
     * @ORM\Column(name="period", type="integer", nullable=true)
     */
    private $period;

    public static function getPeriodTypeChoices()
    {
        return [
            self::PERIOD_TYPE_DAY => 'дн.',
            self::PERIOD_TYPE_WEEK => 'нед.',
            self::PERIOD_TYPE_MOUTH => 'мес.'
        ];
    }

    public function __construct()
    {
        parent::__construct();

        $this->setStatus(self::TASK_STATUS_PROCESSING);
    }

    public function getType()
    {
        return Task::TASK_TYPE_REPEAT;
    }

    public function getTypeName()
    {
        $statuses = Task::getTypeNames();

        return $statuses[$this->getType()];
    }

    /**
     * @return string
     */
    public function getPeriodType()
    {
        return $this->periodType;
    }

    /**
     * @param string $periodType
     */
    public function setPeriodType($periodType)
    {
        $this->periodType = $periodType;
    }

    /**
     * @return integer
     */
    public function getPeriod()
    {
        return $this->period;
    }

    /**
     * @param integer $period
     */
    public function setPeriod($period)
    {
        $this->period = $period;
    }

    public function getPeriodTypeChoiceName()
    {
        $choices = self::getPeriodTypeChoices();

        if( isset($choices[$this->periodType])) {

            return $choices[$this->periodType];
        }

        return $this->periodType;
    }

}