<?php
namespace CommonBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TaskSingleNotification
 *
 * @ORM\Entity()
 */
class TaskRepeatNotification extends TaskNotification
{
    /**
     * @var string
     *
     * @ORM\Column(name="days", type="string", nullable=true)
     */
    private $days;

    /**
     * @return array
     */
    public function getDays()
    {
        if (!empty($this->days)) {

            return unserialize($this->days);
        } else {
            return [];
        }
    }

    /**
     * @param array $days
     */
    public function setDays(array $days)
    {
        $this->days = serialize($days);
    }
}