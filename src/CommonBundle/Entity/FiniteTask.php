<?php
namespace CommonBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * FiniteTask
 *
 * @ORM\Entity()
 */
class FiniteTask extends Task
{
    public function __construct()
    {
        parent::__construct();

        $this->setStatus(self::TASK_STATUS_CREATED);
    }

    public function getType()
    {
        return Task::TASK_TYPE_FINITE;
    }

    public function getTypeName()
    {
        $statuses = Task::getTypeNames();

        return $statuses[$this->getType()];
    }
}