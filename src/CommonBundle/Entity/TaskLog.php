<?php

namespace CommonBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use UserBundle\Entity\User;
use Doctrine\Common\Collections\ArrayCollection;
use CommonBundle\Validator\Constraints as CommonAssert;

/**
 * TaskLog
 *
 * @ORM\Table(name="task_log")
 * @ORM\Entity
 */

class TaskLog
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="task_comment_id_seq", allocationSize=1, initialValue=1)
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="text", type="text", nullable=true)
     * @Assert\NotBlank(groups={"TaskLogComment"})
     */
    private $text;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="datetime", type="datetime", nullable=true)
     */
    private $datetime;

    /**
     * @var Task

     * @ORM\ManyToOne(targetEntity="CommonBundle\Entity\Task", inversedBy="log")
     * @ORM\JoinColumn(name="task_id", referencedColumnName="id")
     */
    private $task;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * })
     */
    private $user;

    /**
     * @var string
     *
     * @ORM\Column(name="changes", type="string", nullable=true)
    */
    private $changes;

    private $dirPath =  'uploads/comment';

    /**
     * @Assert\Count(
     *     max = "3",
     *     maxMessage = "Вы можете загрузить не более 3 файлов",
     *     groups={"TaskLogComment"}
     * )
     *
     * @CommonAssert\FileCollection(
     *     groups={"TaskLogComment"},
     *     mimeTypes={
     *     "application/vnd.oasis.opendocument.text",
     *     "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
     *     "application/vnd.ms-excel",
     *     "application/vnd.oasis.opendocument.spreadsheet",
     *     "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
     *     "application/vnd.ms-office",
     *     "application/pdf",
     *     "application/zip",
     *     "application/gzip",
     *     "application/x-rar-compressed",
     *     "image/jpeg",
     *     "image/png",
     *     "image/bmp",
     *     "image/pjpeg",
     *     "image/tif",
     *     "image/gif",
     *     "image/vnd.wap.wbmp",
     *     "application/msword",
     *     "image/svg+xml",
     *     "text/plain",
     *     "text/csv",
     *     "text/html",
     *     "text/xml"
     *    },
     *     maxSize="10M",
     *     mimeTypesMessage="Неверный формат файла"
     * )
     *
     * @ORM\ManyToMany(targetEntity="CommonBundle\Entity\File", cascade={"persist"})
     * @ORM\JoinTable(name="tasks_log_files",
     *      joinColumns={@ORM\JoinColumn(name="file_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="task_log_id", referencedColumnName="id")}
     *      )
     */
    private $files;

    public function __construct()
    {
        $this->datetime = new \DateTime('now');

        $this->setFiles(new ArrayCollection());
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set text
     *
     * @param string $text
     *
     * @return TaskComment
     */
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * Get text
     *
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * Set datetime
     *
     * @param \DateTime $datetime
     *
     * @return TaskComment
     */
    public function setDatetime($datetime)
    {
        $this->datetime = $datetime;

        return $this;
    }

    /**
     * Get datetime
     *
     * @return \DateTime
     */
    public function getDatetime()
    {
        return $this->datetime;
    }

    /**
     * Set user
     *
     * @param User $user
     *
     * @return TaskLog
     */
    public function setUser(User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @return Task
     */
    public function getTask()
    {
        return $this->task;
    }

    /**
     * @param Task $task
     */
    public function setTask(Task $task)
    {
        $this->task = $task;
    }

    /**
     * @return string
     */
    public function getChanges()
    {
        return (array)json_decode($this->changes);
    }

    /**
     * @param string $changes
     */
    public function setChanges($changes)
    {
        $this->changes = json_encode($changes);
    }


    /**
     * @return ArrayCollection
     */
    public function getFiles()
    {
        return $this->files;
    }

    /**
     * @param ArrayCollection $files
     */
    public function setFiles($files)
    {
        $this->files = $files;
    }

    public function getDirPath()
    {
        return  $this->dirPath  . '/' . $this->getUser()->getId();
    }

}