<?php

namespace CommonBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * WorkCalendar
 *
 * @ORM\Table(name="work_calendar")
 * @ORM\Entity()
 */
class WorkCalendar
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="year", type="integer", nullable=false)
     */
    private $year;

    /**
     * @var WorkCalendarChange
     *
     * @ORM\OneToMany(targetEntity="CommonBundle\Entity\WorkCalendarChange", mappedBy="calendar", cascade={"all"}, orphanRemoval=true)
     */
    private $changes;

    public function __construct()
    {
        $this->setChanges(new ArrayCollection());
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getYear()
    {
        return $this->year;
    }

    /**
     * @param int $year
     */
    public function setYear($year)
    {
        $this->year = $year;
    }

    /**
     * @return ArrayCollection
     */
    public function getChanges()
    {
        return $this->changes;
    }

    /**
     * @param ArrayCollection $changes
     */
    public function setChanges(ArrayCollection $changes)
    {
        $this->changes = $changes;
    }
}

