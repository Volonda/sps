<?php

namespace CommonBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use UserBundle\Entity\User;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;


/**
 * MeetingMember
 *
 * @ORM\Table(name="meeting_members")
 * @ORM\Entity
 * @Gedmo\SoftDeleteable()
 */
class MeetingMember
{
    use SoftDeleteableEntity;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="meeting_members_id_seq", allocationSize=1, initialValue=1)
     */
    private $id;

    /**
     * @var boolean
     *
     * @ORM\Column(name="presence", type="boolean", nullable=false)
     */
    private $presence;

    /**
     * @var \DateTime
     * @ORM\Column(name="presence_updated", type="datetime", nullable=true)
     */
    private $presenceUpdated;

    /**
     * @var string
     * @Assert\NotBlank(groups={"NonPresence"})
     * @ORM\Column(name="cause", type="text", nullable=true)
     */
    private $cause;

    /**
     * @var string
     * @ORM\Column(name="cause_last", type="text", nullable=true)
     */
    private $causeLast;

    /**
     * @var \DateTime
     * @ORM\Column(name="cause_updated", type="datetime", nullable=true)
     */
    private $causeUpdated;

    /**
     * @var Meeting
     *
     * @ORM\ManyToOne(targetEntity="Meeting", inversedBy="members")
     * @ORM\JoinColumn(name="meeting_id", referencedColumnName="id", nullable=false, onDelete="RESTRICT")
     */
    private $meeting;

    /**
     * @var User
     * @Assert\NotBlank(groups={"MeetingCreateForm"})
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\User", inversedBy="meetings")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=false, onDelete="RESTRICT")
     */
    private $user;

    /**
     * @var \DateTime
     * @ORM\Column(name="user_id_updated", type="datetime", nullable=true)
     */
    private $userUpdated;

    /**
     * @var User
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\User", inversedBy="meetings")
     * @ORM\JoinColumn(name="user_id_last", referencedColumnName="id", nullable=false, onDelete="RESTRICT")
     */
    private $userLast;

    /**
     * @var \DateTime $created
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime", name="created", nullable=false)
     */
    private $created;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set presence
     *
     * @param boolean $presence
     *
     * @return MeetingMember
     */
    public function setPresence($presence)
    {
        $this->presence = $presence;

        return $this;
    }

    /**
     * Get presence
     *
     * @return boolean
     */
    public function getPresence()
    {
        return $this->presence;
    }

    /**
     * Set cause
     *
     * @param string $cause
     *
     * @return MeetingMember
     */
    public function setCause($cause)
    {
        $this->cause = $cause;

        return $this;
    }

    /**
     * Get cause
     *
     * @return string
     */
    public function getCause()
    {
        return $this->cause;
    }

    /**
     * Set meeting
     *
     * @param Meeting $meeting
     *
     * @return MeetingMember
     */
    public function setMeeting(Meeting $meeting = null)
    {
        $this->meeting = $meeting;

        return $this;
    }

    /**
     * Get meeting
     *
     * @return Meeting
     */
    public function getMeeting()
    {
        return $this->meeting;
    }

    /**
     * Set user
     *
     * @param User $user
     *
     * @return MeetingMember
     */
    public function setUser(User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @return \DateTime
     */
    public function getPresenceUpdated()
    {
        return $this->presenceUpdated;
    }

    /**
     * @return string
     */
    public function getCauseLast()
    {
        return $this->causeLast;
    }

    /**
     * @return \DateTime
     */
    public function getCauseUpdated()
    {
        return $this->causeUpdated;
    }

    /**
     * @return \DateTime
     */
    public function getUserUpdated()
    {
        return $this->userUpdated;
    }

    /**
     * @return User
     */
    public function getUserLast()
    {
        return $this->userLast;
    }

    /**
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

}