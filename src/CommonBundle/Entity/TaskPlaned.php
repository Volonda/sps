<?php
namespace CommonBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use UserBundle\Entity\User;

/**
 * TaskPlaned
 *
 * @ORM\Table(name="task_planed")
 * @ORM\Entity(repositoryClass="CommonBundle\Repository\TaskPlanedRepository")
 */
class TaskPlaned
{
    const TASK_PLANED_TYPE_FINITE = 'finite';
    const TASK_PLANED_TYPE_REPEAT = 'repeat';
    const PERIOD_TYPE_DAY = 'day';
    const PERIOD_TYPE_WEEK = 'week';
    const PERIOD_TYPE_MOUTH = 'month';

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="task_planed_id_seq", allocationSize=1, initialValue=1)
     */
    private $id;

    /**
     * @var string
     * @Assert\NotBlank(groups={"QuestionEditForm"})
     * @ORM\Column(name="text", type="text", nullable=true)
     */
    private $text;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="end_datetime", type="datetime", nullable=true)
     */
    private $endDatetime;

    /**
     * @var \DateTime
     * @Assert\GreaterThan("+0 hours", groups={"QuestionEditForm"})
     * @ORM\Column(name="deadline_datetime", type="datetime", nullable=false)
     */
    private $deadlineDatetime;


    /**
     * @var \DateTime
     *
     * @ORM\Column(name="start_datetime", type="datetime", nullable=true)
     */
    private $startDatetime;

    /**
     * @var Question
     * @ORM\ManyToOne(targetEntity="CommonBundle\Entity\Question", inversedBy="tasksPlaned")
     * @ORM\JoinColumn(name="question_id", referencedColumnName="id", onDelete="RESTRICT", nullable=false)
     */
    private $question;

    /**
     * @Assert\Count(
     *     min = "1",
     *     minMessage = "Должен быть хотябы 1 ответственный",
     *     groups={"QuestionEditForm"}
     * )
     * @ORM\OrderBy({"sortOrder"="ASC", "name"="ASC"})
     * @ORM\ManyToMany(targetEntity="UserBundle\Entity\User")
     * @ORM\JoinTable(name="tasks_planed_user",
     *      joinColumns={@ORM\JoinColumn(name="task_planed_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")}
     *      )
     */
    private $users;

    /**
     * @var string
     * @Assert\NotBlank(groups={"TaskRepeatType"})
     * @ORM\Column(name="period_type", type="task_repeat_period_type", nullable=true)
     */
    private $periodType;

    /**
     * @var integer
     * @Assert\NotBlank(groups={"TaskRepeatType"})
     * @ORM\Column(name="period", type="integer", nullable=true)
     */
    private $period;

    /**
     * @var string
     * @Assert\NotBlank(groups={"QuestionEditForm"})
     * @ORM\Column(name="type", type="task_type", nullable=false)
     */
    private $type;

    public static function getPeriodTypeChoices()
    {
        return [
            self::PERIOD_TYPE_DAY => 'дн.',
            self::PERIOD_TYPE_WEEK => 'нед.',
            self::PERIOD_TYPE_MOUTH => 'мес.'
        ];
    }


    public static function getTypeNames()
    {
        return [
            self::TASK_PLANED_TYPE_FINITE => 'Задача со сроком исполнения',
            self::TASK_PLANED_TYPE_REPEAT => 'Постоянная задача'
        ];
    }


    public function __construct()
    {
        $this->setUsers(new ArrayCollection());
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set text
     *
     * @param string $text
     *
     * @return TaskPlaned
     */
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * Get text
     *
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return TaskPlaned
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set endDatetime
     *
     * @param \DateTime $endDatetime
     *
     * @return TaskPlaned
     */
    public function setEndDatetime($endDatetime)
    {
        $this->endDatetime = $endDatetime;

        return $this;
    }

    /**
     * Get endDatetime
     *
     * @return \DateTime
     */
    public function getEndDatetime()
    {
        return $this->endDatetime;
    }

    /**
     * Set startDatetime
     *
     * @param \DateTime $startDatetime
     *
     * @return TaskPlaned
     */
    public function setStartDatetime($startDatetime)
    {
        $this->startDatetime = $startDatetime;

        return $this;
    }

    /**
     * Get startDatetime
     *
     * @return \DateTime
     */
    public function getStartDatetime()
    {
        return $this->startDatetime;
    }

    /**
     * Set question
     *
     * @param Question $question
     *
     * @return TaskPlaned
     */
    public function setQuestion(Question $question)
    {
        $this->question = $question;

        return $this;
    }

    /**
     * Get question
     *
     * @return Question
     */
    public function getQuestion()
    {
        return $this->question;
    }

    /**
     * Get users
     *
     * @return ArrayCollection
     */
    public function getUsers()
    {
        return $this->users;
    }

    /**
     * @param ArrayCollection $users
     */
    public function setUsers(ArrayCollection $users)
    {
        $this->users = $users;
    }

    /**
     * @return \DateTime
     */
    public function getDeadlineDatetime()
    {
        return $this->deadlineDatetime;
    }

    /**
     * @param \DateTime $deadlineDatetime
     */
    public function setDeadlineDatetime($deadlineDatetime)
    {
        $this->deadlineDatetime = $deadlineDatetime;
    }

    /**
     * @param User $user
     */
    public function addUser(User $user)
    {
        $this->getUsers()->add($user);
    }

    public function removeUser(User $user)
    {
        $this->getUsers()->remove($user);
    }

    /**
     * @return string
     */
    public function getPeriodType()
    {
        return $this->periodType;
    }

    /**
     * @param string $periodType
     */
    public function setPeriodType($periodType)
    {
        $this->periodType = $periodType;
    }

    /**
     * @return int
     */
    public function getPeriod()
    {
        return $this->period;
    }

    /**
     * @param int $period
     */
    public function setPeriod($period)
    {
        $this->period = $period;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    public function getTypeName()
    {
        $types = self::getTypeNames();

        if (isset($types[$this->type])) {

            return $types[$this->type];
        } else {

            return $this->type;
        }
    }

    public function getPeriodTypeChoiceName()
    {
        $choices = self::getPeriodTypeChoices();

        if( isset($choices[$this->periodType])) {

            return $choices[$this->periodType];
        }

        return $this->periodType;
    }
}