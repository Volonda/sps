<?php

namespace CommonBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ScheduleChange
 *
 * @ORM\Table(name="work_calendar_change")
 * @ORM\Entity()
 */
class WorkCalendarChange
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="day", type="integer", nullable=false)
     */
    private $day;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_day_off", type="boolean", nullable=false)
     */
    private $dayOff;

    /**
     * @ORM\ManyToOne(targetEntity="CommonBundle\Entity\WorkCalendar", inversedBy="changes")
     * @ORM\JoinColumn(name="calendar_id", referencedColumnName="id", nullable=false)
     */
    private $calendar;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return WorkCalendar
     */
    public function getCalendar()
    {
        return $this->calendar;
    }

    /**
     * @param WorkCalendar $calendar
     */
    public function setCalendar(WorkCalendar $calendar)
    {
        $this->calendar = $calendar;
    }

    /**
     * @return string
     */
    public function getDay()
    {
        return $this->day;
    }

    /**
     * @param string $day
     */
    public function setDay($day)
    {
        $this->day = $day;
    }

    /**
     * @return boolean
     */
    public function isDayOff()
    {
        return $this->dayOff;
    }

    /**
     * @param boolean $dayOff
     */
    public function setDayOff($dayOff)
    {
        $this->dayOff = $dayOff;
    }
}

