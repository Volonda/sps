<?php

namespace CommonBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * TaskRepository
 *
 * This class was generated by the PhpStorm "Php Annotations" Plugin. Add your own custom
 * repository methods below.
 */
class TaskRepository extends EntityRepository
{
    public function listAll($hydrationMode = \Doctrine\ORM\Query::HYDRATE_OBJECT)
    {
        $qb = $this->createQueryBuilder('t');
        $qb ->select('t','q')
            ->leftJoin('t.question', 'q');

        return $qb->getQuery()->getResult($hydrationMode);

    }
}
