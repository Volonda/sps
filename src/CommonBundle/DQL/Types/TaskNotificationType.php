<?php
namespace CommonBundle\DQL\Types;

use Doctrine\DBAL\Types\Type;
use Doctrine\DBAL\Platforms\AbstractPlatform;

class TaskNotificationType extends Type
{
    const MYTYPE = 'task_notification_type';

    public function getSQLDeclaration(array $fieldDeclaration, AbstractPlatform $platform)
    {
        return self::MYTYPE;
    }

    public function getName()
    {
        return self::MYTYPE;
    }
}