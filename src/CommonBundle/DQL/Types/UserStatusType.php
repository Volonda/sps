<?php
namespace CommonBundle\DQL\Types;

use Doctrine\DBAL\Types\Type;
use Doctrine\DBAL\Platforms\AbstractPlatform;

class UserStatusType extends Type
{
    const MYTYPE = 'user_status_type';

    public function getSQLDeclaration(array $fieldDeclaration, AbstractPlatform $platform)
    {
        return self::MYTYPE;
    }

    public function getName()
    {
        return self::MYTYPE;
    }
}