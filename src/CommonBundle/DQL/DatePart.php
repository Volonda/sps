<?php
namespace CommonBundle\DQL;

use Doctrine\ORM\Query\AST\Functions\FunctionNode;
use Doctrine\ORM\Query\Lexer;
use Doctrine\ORM\Query\SqlWalker;
use Doctrine\ORM\Query\Parser;

class DatePart extends FunctionNode
{
    private $field;
    private $source;

    public function parse(Parser $parser)
    {
        $parser->match(Lexer::T_IDENTIFIER);
        $parser->match(Lexer::T_OPEN_PARENTHESIS);
        $this->field = $parser->StringExpression();
        $parser->match(Lexer::T_COMMA);
        $this->source = $parser->StringExpression();
        $parser->match(Lexer::T_CLOSE_PARENTHESIS);

    }

    public function getSql(SqlWalker $sqlWalker)
    {
        return sprintf(
            "date_part(%s, %s)",
            $sqlWalker->walkStringPrimary($this->field),  $sqlWalker->walkStringPrimary($this->source)
        );
    }
}
