<?php

namespace CommonBundle\Event;

use CommonBundle\Entity\Task;
use Symfony\Component\EventDispatcher\Event;

class TaskCreateEvent extends Event
{
    /** @var Task $task*/
    private $task;

    /**
     * @param Task $task
     */
    public function __construct(Task $task)
    {
        $this->task = $task;
    }

    /**
     * @return Task
     */
    public function getTask()
    {
        return $this->task;
    }
}