<?php
namespace CommonBundle\Event;

use CommonBundle\Entity\TaskLog;
use Symfony\Component\EventDispatcher\Event;
use CommonBundle\Entity\Task;

class TaskChangedEvent extends Event
{
    /** @var TaskLog*/
    private $taskLog;

    /**@var string*/
    private $task;

    /**@var array*/
    private $changes;

    /**
     * @param Task $task
     * @param TaskLog $taskLog
     * @param array $changes
     */
    public function __construct(Task $task, TaskLog $taskLog, array $changes)
    {
        $this->task = $task;
        $this->taskLog = $taskLog;
        $this->changes = $changes;
    }

    /**
     * @return Task
     */
    public function getTask()
    {
        return $this->task;
    }

    /**
     * @return TaskLog
     */
    public function getTaskLog()
    {
        return $this->taskLog;
    }

    /**
     * @return array
     */
    public function getChanges()
    {
        return $this->changes;
    }
}