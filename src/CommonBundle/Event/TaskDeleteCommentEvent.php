<?php

namespace CommonBundle\Event;

use CommonBundle\Entity\TaskLog;
use Symfony\Component\EventDispatcher\Event;
use CommonBundle\Entity\Task;

class TaskDeleteCommentEvent extends Event
{
    /** @var TaskLog*/
    protected $taskLog;

    /**
     * @param TaskLog $taskLog
     */
    public function __construct(TaskLog $taskLog)
    {
        $this->taskLog = $taskLog;
    }

    /**
     * @return TaskLog
     */
    public function getTaskLog()
    {
        return $this->taskLog;
    }
}