<?php
/**
 * Отправлено уведомление о встрече
*/
namespace CommonBundle\Event;

use Symfony\Component\EventDispatcher\Event;
use CommonBundle\Entity\Meeting;

class MeetingNotificationEvent extends Event
{
    /** @var Meeting*/
    protected $meeting;

    /** @var array*/
    protected $users;

    /**
     * @param Meeting $meeting
     * @param array $users
     */
    public function __construct(Meeting $meeting, array $users)
    {
        $this->meeting = $meeting;
        $this->users = $users;
    }

    /**
     * @return Meeting
     */
    public function getMeeting()
    {
        return $this->meeting;
    }

    /**
     * @return array
     */
    public function getUsers()
    {
        return $this->users;
    }
}