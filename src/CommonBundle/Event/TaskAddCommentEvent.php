<?php

namespace CommonBundle\Event;

use CommonBundle\Entity\TaskLog;
use Symfony\Component\EventDispatcher\Event;
use CommonBundle\Entity\Task;

class TaskAddCommentEvent extends Event
{
    /** @var Task*/
    protected $task;

    /** @var TaskLog*/
    protected $taskLog;

    /**
     * @param Task $task
     * @param TaskLog $taskLog
     */
    public function __construct(Task $task, TaskLog $taskLog)
    {
        $this->task = $task;
        $this->taskLog = $taskLog;
    }

    /**
     * @return Task
     */
    public function getTask()
    {
        return $this->task;
    }

    /**
     * @return TaskLog
     */
    public function getTaskLog()
    {
        return $this->taskLog;
    }
}