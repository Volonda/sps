<?php
namespace CommonBundle\Event;

use Symfony\Component\EventDispatcher\Event;
use CommonBundle\Entity\Meeting;

class MeetingClosedEvent extends Event
{
    /** @var Meeting*/
    protected $meeting;

    /**
     * @param Meeting $meeting
     */
    public function __construct(Meeting $meeting)
    {
        $this->meeting = $meeting;
    }

    /**
     * @return Meeting
     */
    public function getMeeting()
    {
        return $this->meeting;
    }
}