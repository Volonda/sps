<?php
namespace CommonBundle\Twig;

use CommonBundle\Entity\Email;
use CommonBundle\Entity\MeetingMember;
use CommonBundle\Entity\Question;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\PropertyAccess\PropertyAccess;
use CommonBundle\Entity\Meeting;
use Doctrine\ORM\Query\ResultSetMappingBuilder;
use Symfony\Component\PropertyAccess\PropertyAccessor;
use UserBundle\Entity\User;

class EmailExtension extends \Twig_Extension
{
    private $accessor = null;

    private $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function getFilters()
    {
        return [
            new \Twig_SimpleFilter('email_mayor_tasks', [$this, 'getMayorFilteredTasks']),
            new \Twig_SimpleFilter('email_user_tasks', [$this, 'getUserFilteredTasks']),
        ];
    }

    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction('emailFieldUpdated', array($this, 'isFieldUpdated')),
            new \Twig_SimpleFunction('emailPropertyAccessor', array($this, 'getAccessor')),
            new \Twig_SimpleFunction('emailMeetingUserQuestions', array($this, 'getMeetingUserQuestions')),
            new \Twig_SimpleFunction('emailMeetingQuestions', array($this, 'getMeetingQuestions')),
            new \Twig_SimpleFunction('emailMeetingOrderedMembers', array($this, 'getEmailMeetingOrderedMembers')),
            new \Twig_SimpleFunction('emailIsRowCreated', array($this, 'isRowCreated')),
            new \Twig_SimpleFunction('emailIsRowDeleted', array($this, 'isRowDeleted')),
            new \Twig_SimpleFunction('emailDeletedQuestionFiles', array($this,'getDeletedQuestionFiles')),
            new \Twig_SimpleFunction('emailDeletedUserQuestions', array($this, 'getDeletedUserQuestions')),
            new \Twig_SimpleFunction('emailIsShowQuestionChanges', array($this, 'isShowQuestionChanges')),
            new \Twig_SimpleFunction('emailIsShowMeetingChanges', array($this, 'isShowMeetingChanges')),
        );
    }

    /**
     * @param $entity
     * @param $key
     * @param \DateTime|null $lastUserNotification
     * @return boolean
     */
    public function isFieldUpdated($entity, $key, \DateTime $lastUserNotification = null)
    {
        $accessor = $this->getAccessor();

        if ($accessor->isReadable($entity, $key . 'Last')) {
            $last = $accessor->getValue($entity, $key . 'Last');
        }

        $current = $accessor->getValue($entity, $key);
        $updated = $accessor->getValue($entity, $key . 'Updated');

        if ($updated && (!isset($last) || $last != $current)) {

            if (!$lastUserNotification || $lastUserNotification < $updated) {

                return true;
            }
        }

        return false;
    }

    /**
     * @param Meeting $meeting
     * @param $entity
     * @param \DateTime|null $lastNotification
     * @return boolean
     */
    public function isRowDeleted(Meeting $meeting, $entity, $lastNotification)
    {
        $status = $this->getRowStatus($meeting, $entity, $lastNotification);

        return $status == Email::ROW_STATUS_DELETED;
    }

    /**
     * @param Meeting $meeting
     * @param $entity
     * @param \DateTime|null $lastNotification
     * @return string
     */
    public function isRowCreated(Meeting $meeting, $entity, $lastNotification)
    {
        $status = $this->getRowStatus($meeting, $entity, $lastNotification);

        return $status == Email::ROW_STATUS_CREATED;
    }

    /**
     * @param Meeting $meeting
     * @param \DateTime|null $lastNotification
     * @return string
     */
    public function getMeetingUserQuestions(Meeting $meeting, \DateTime $lastNotification)
    {
        $created = [];

        $stmt = $this->em->getConnection()->prepare('SELECT q.id, uq.user_id, uq.question_id FROM questions q JOIN user_questions uq ON q.id = uq.question_id WHERE q.meeting_id = :meetingId AND  uq.created > :created');
        $stmt->bindValue('created', $lastNotification->format('Y-m-d H:i:s'));
        $stmt->bindValue('meetingId', $meeting->getId());
        $stmt->execute();

        foreach($stmt->fetchAll() as $userQuestion){
            $created[$userQuestion['question_id']][] = $userQuestion['user_id'];
        }

        return $created;

    }

    /**
     * @return PropertyAccessor
    */
    public function getAccessor()
    {
        if(!$this->accessor){
            $this->accessor = PropertyAccess::createPropertyAccessor();
        }

        return $this->accessor;
    }

    /**
     * @param Meeting $meeting
     * @param \DateTime|null $lastNotification
     * @return array
     */
    public function getMeetingQuestions(Meeting $meeting, \DateTime $lastNotification = null) {

        if ($meeting->getStatus() == Meeting::MEETING_STATUS_CREATED) {

            /** @var \Gedmo\SoftDeleteable\Filter\SoftDeleteableFilter $filter */
            $filter=$this->em->getFilters()->getFilter('softdeleteable');
            $filter->disableForEntity('CommonBundle\Entity\Question');

            $qb = $this->em->createQueryBuilder();

            $qb
                ->select('q')
                ->from('CommonBundle:Question', 'q')
                ->where('q.meeting = :meeting')
                ->setParameter('meeting', $meeting);

            if ($lastNotification instanceof \DateTime) {
                $qb->andWhere(
                    $qb->expr()->orX(
                        $qb->expr()->andX('q.deletedAt IS NULL'),
                        $qb->expr()->andX(
                            $qb->expr()->andX('q.deletedAt > :lastNotification'),
                            $qb->expr()->andX('q.created  < :lastNotification')
                        )
                    )
                );

                $qb->setParameter('lastNotification', $lastNotification);
            } else {
                $qb->andWhere('q.deletedAt IS NULL');
            };

            $result = $qb->getQuery()->getResult();

            $filter->enableForEntity('CommonBundle\Entity\Question');

            return $result;
        }

        return $meeting->getQuestions();

    }

    /**
     * @param Question $meeting
     * @param \DateTime|null $lastNotification
     * @param $presence
     * @return array
     */
    public function getEmailMeetingOrderedMembers($meeting, $presence, \DateTime $lastNotification = null)
    {
        /** @var \Gedmo\SoftDeleteable\Filter\SoftDeleteableFilter $filter */
        $filter=$this->em->getFilters()->getFilter('softdeleteable');
        $filter->disableForEntity('CommonBundle\Entity\MeetingMember');

        $qb = $this->em->createQueryBuilder()
            ->select('mm, u')
            ->from('CommonBundle:MeetingMember','mm')
            ->join('mm.user','u')
            ->where('mm.meeting = :meeting')
            ->setParameter('meeting', $meeting)
            ->orderBy('u.sortOrder','ASC')
            ->addOrderBy('u.name','ASC')
        ;

        if ($presence !== null) {

            $qb->andWhere('mm.presence = :member_presence')
                ->setParameter('member_presence', $presence)
            ;

        }

        if ($lastNotification instanceof \DateTime) {
            $qb->andWhere(
                $qb->expr()->orX(
                    $qb->expr()->andX('mm.deletedAt IS NULL'),
                    $qb->expr()->andX(
                        $qb->expr()->andX('mm.deletedAt > :lastNotification'),
                        $qb->expr()->andX('mm.created  < :lastNotification')
                    )
                )
            )->setParameter('lastNotification', $lastNotification)
            ;

        } else {
            $qb->andWhere('mm.deletedAt IS NULL');
        };

        $result = $qb->getQuery()
            ->getResult()
        ;

        $filter->enableForEntity('CommonBundle\Entity\MeetingMember');

        return $result;
    }

    /**
     * @param Question $question
     * @param \DateTime|null $lastNotification
     * @return array
     */
    public function getDeletedUserQuestions(Question $question, \DateTime $lastNotification)
    {
        $result = [];

        if ($lastNotification instanceof \DateTime) {
            $rsm = new ResultSetMappingBuilder($this->em);
            $rsm->addRootEntityFromClassMetadata('UserBundle:User', 'u');

            $query = $this->em->createNativeQuery('SELECT u.* FROM users u
              WHERE EXISTS (
                SELECT 1 FROM user_questions_deleted uqd 
                  WHERE 
                    uqd.user_id = u.id 
                    AND uqd.deleted_at > :lastNotification 
                    AND uqd.question_id = :questionId 
                    AND uqd.created < :lastNotification
              )
            ', $rsm);

            $query->setParameter('questionId', $question->getId());
            $query->setParameter('lastNotification', $lastNotification->format('Y-m-d H:i:s'));

            $result = $query->getResult();
        }

        return $result;
    }

    /**
     * @param Question $question
     * @param \DateTime|null $lastNotification
     * @return array
     */
    public function getDeletedQuestionFiles(Question $question, \DateTime $lastNotification = null)
    {
        $result = [];

        if ($lastNotification instanceof \DateTime) {
            $rsm = new ResultSetMappingBuilder($this->em);
            $rsm->addRootEntityFromClassMetadata('CommonBundle:File', 'f');

            $query = $this->em->createNativeQuery('SELECT f.* FROM files f JOIN question_files_deleted qfd ON qfd.file_id = f.id WHERE qfd.deleted_at > :lastNotification AND qfd.question_id = :questionId AND qfd.created < :lastNotification
            ', $rsm);

            $query->setParameter('questionId', $question->getId());
            $query->setParameter('lastNotification', $lastNotification->format('Y-m-d H:i:s'));

            $result = $query->getResult();
        }

        return $result;
    }

    /**
     * @param Question $question
     * @param $lastNotification
     * @return boolean
     */
    public function isShowQuestionChanges(Question $question, $lastNotification)
    {
        if (
            $question->getMeeting()->getStatus() == Meeting::MEETING_STATUS_CREATED
            && $lastNotification instanceof \DateTime
            && $this->isRowNoStatus($question->getMeeting(), $question, $lastNotification)
        ) {
            return true;
        }

        return false;
    }

    /**
     * @param Meeting $meeting
     * @param $lastNotification
     * @return boolean
     */
    public function isShowMeetingChanges(Meeting $meeting, $lastNotification)
    {
        if (
            $meeting->getStatus() == Meeting::MEETING_STATUS_CREATED
            && $lastNotification instanceof \DateTime
        ) {
            return true;
        }

        return false;
    }

    /**
     * @param $tasks
     * @param User $user
     * @return array
    */
    public function getMayorFilteredTasks(array $tasks, User $user)
    {
        $result = [];
        foreach($tasks as $task) {
            if ($task['user_id'] != $user->getId() && $task['mayor_id'] == $user->getId()) {
                $result[] = $task;
            }
        }

        return $result;
    }

    /**
     * @param $tasks
     * @param User $user
     * @return array
     */
    public function getUserFilteredTasks(array $tasks, User $user)
    {
        $result = [];
        foreach($tasks as $task) {
            if ($task['user_id'] == $user->getId()) {
                $result[] = $task;
            }
        }

        return $result;
    }

    /**
     * @param Meeting $meeting
     * @param $entity
     * @param $lastNotification
     * @return string|null
     */
    private function getRowStatus(Meeting $meeting, $entity, $lastNotification)
    {
        if (
            $meeting->getStatus() == Meeting::MEETING_STATUS_CREATED
            && (
                $entity instanceof Question
                || $entity instanceof MeetingMember
            )
        ) {
            if($entity->isDeleted()){

                return Email::ROW_STATUS_DELETED;
            } elseif(!empty($lastNotification) && $lastNotification < $entity->getCreated()) {

                return Email::ROW_STATUS_CREATED;
            }
        }
    }

    /**
     * @param Meeting $meeting
     * @param $entity
     * @param $lastNotification
     * @return boolean
     */
    private function isRowNoStatus(Meeting $meeting, $entity, $lastNotification)
    {
        $status = $this->getRowStatus($meeting, $entity, $lastNotification);

        return $status != Email::ROW_STATUS_CREATED && $status != Email::ROW_STATUS_DELETED;
    }

    /**
     * @return string
    */
    public function getName()
    {
        return 'email_extension';
    }
}