<?php
namespace WebSiteBundle\MenuBuilder;

use UserBundle\Entity\User;

class MenuBuilderFactory
{
    public function getMenuBuilder(User $user)
    {
        if ($user->hasRole('ROLE_ADMIN')) {

            return new AdminMenuBuilder();

        }  else {

            return new UserMenuBuilder();
        }
    }
}