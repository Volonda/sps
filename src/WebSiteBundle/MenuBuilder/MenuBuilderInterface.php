<?php
namespace WebSiteBundle\MenuBuilder;

interface MenuBuilderInterface
{
    public function buildMenu();

    public function buildLeftMenu();

}