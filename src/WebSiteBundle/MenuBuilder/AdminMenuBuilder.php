<?php
namespace WebSiteBundle\MenuBuilder;

class AdminMenuBuilder implements MenuBuilderInterface
{
    public function buildMenu()
    {
        return [
            [
                'icon' => 'i-note-round',
                'title' => 'Список совещаний',
                'route' => 'web_site_meetings_list'
            ],
            [
                'icon' => 'i-pencil-round',
                'title' => 'Список задач',
                'route' => 'web_site_tasks_all'
            ],
            [
                'icon' => 'i-check-round',
                'title' => 'Список решений',
                'route' => 'web_site_questions_by_question_list'
            ]
        ];
    }

    public function buildLeftMenu()
    {
        return [
            [
                'icon' => 'i-pensil',
                'title' => 'Cистема протоколирования совещаний',
                'route' => 'web_site_index'
            ],
            [
                'icon' => 'i-user',
                'title' => 'Пользователи и группы',
                'route' => 'web_site_users_index'
            ],
            [
                'icon' => 'i-main',
                'title' => 'Настройки',
                'route' => 'web_site_settings'
            ],
            [
                'icon' => 'i-main',
                'title' => 'FAQ',
                'route' => 'web_site_faq_task'
            ]
        ];
    }
}