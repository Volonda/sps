<?php
namespace WebSiteBundle\Controller;

use CommonBundle\Entity\FiniteTask;
use CommonBundle\Entity\Meeting;
use CommonBundle\Entity\RepeatTask;
use CommonBundle\Entity\Task;
use CommonBundle\Entity\TaskLog;
use Dompdf\Exception;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use UserBundle\Entity\User;
use WebSiteBundle\Form\Type\TaskLogCommentType;
use WebSiteBundle\Filter\Filter;
use CommonBundle\Manager\TaskManager;
use CommonBundle\Manager\TaskNotificationManager;
use WebSiteBundle\Form\Type\TaskNotificationType\TaskNotificationType;
use WebSiteBundle\Form\Type\TaskType\ToTaskRepeatType;
use WebSiteBundle\Form\Type\TaskType\ToTaskFiniteType;
use CommonBundle\Manager\TaskHistoryNotificationManager;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * @Route("/tasks")
 * @Security("has_role('ROLE_USER')")
 */
class TaskController extends Controller
{
    const PAGE_LIMIT = 20;

    /**
     * @Route("/all", name="web_site_tasks_all")
     * @param Request $request
     * @Template()
     * @return array
     */
    public function allAction(Request $request)
    {
        /** @var  TaskManager $manager*/
        $manager = $this->get('common.manager.task_manager');

        /* @var $filter Filter */
        $filter = $this->get('web_site.filter')->getFilter($request, $manager->getListFilters());


        $pagination = $manager->getPaginate(
            null,
            $request->query->get('page', 1),
            self::PAGE_LIMIT, $filter
        );

        return [
            'pagination' => $pagination,
            'filters' => $filter->getFiltersValue(),
        ];
    }

    /**
     * @Route("/repeat", name="web_site_tasks_repeat")
     * @param Request $request
     * @Template()
     * @return array
     */
    public function repeatAction(Request $request)
    {
        /** @var  TaskManager $manager*/
        $manager = $this->get('common.manager.task_manager');
        $user = $this->getUser();

        /* @var $filter Filter */
        $filter = $this->get('web_site.filter')->getFilter($request, $manager->getRepeatListFilters());

            $pagination = $manager->getByUserPaginate(
                Task::TASK_TYPE_REPEAT,
                $user,
                $request->query->get('page', 1),
                self::PAGE_LIMIT,
                $filter,
                [Task::TASK_STATUS_PROCESSING]
            );

        return [
            'pagination' => $pagination,
            'filters' => $filter->getFiltersValue(),
        ];
    }

    /**
     * @Route("/finite", name="web_site_tasks_finite")
     * @param Request $request
     * @Template()
     * @return array
     */
    public function finiteAction(Request $request)
    {
        /** @var  TaskManager $manager*/
        $manager = $this->get('common.manager.task_manager');
        $user = $this->getUser();

        /* @var $filter Filter */
        $filter = $this->get('web_site.filter')->getFilter($request, $manager->getFiniteListFilters());

        $pagination = $manager->getByUserPaginate(
            Task::TASK_TYPE_FINITE,
            $user,
            $request->query->get('page', 1),
            self::PAGE_LIMIT,
            $filter,
            [Task::TASK_STATUS_CREATED, Task::TASK_STATUS_PROCESSING, Task::TASK_STATUS_TESTING, Task::TASK_STATUS_APPROVED]
        );

        return [
            'pagination' => $pagination,
            'filters' => $filter->getFiltersValue(),
        ];
    }

    /**
     * @Route("/view/{id}", name="web_site_tasks_view")
     * @param Task $task
     * @param Request $request
     * @ParamConverter("id", class="CommonBundle:Task")
     * @Template()
     * @return array|RedirectResponse
     */
    public function viewAction(Request $request,Task $task)
    {

        /**@var $manager TaskManager*/
        $manager = $this->get('common.manager.task_manager');

        $form = $this->createForm(TaskLogCommentType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $manager->addTaskLogComment($this->getUser(), $task, $form->getData());

            $url = $this->generateUrl('web_site_tasks_view', ['id' => $task->getId()]);

            return $this->redirect($url.'#comments');
        }

        return [
            'task' => $task,
            'form' => $form->createView()
        ];
    }

    /**
     * @Route("/comment/delete/{id}", name="web_site_tasks_comment_delete")
     * @param TaskLog $taskLog
     * @ParamConverter("id", class="CommonBundle:TaskLog")
     * @return array|RedirectResponse
     */
    public function commentDeleteAction(TaskLog $taskLog)
    {
        /**@var $manager TaskManager*/
        $manager = $this->get('common.manager.task_manager');

        $manager->deleteTaskLogComment($this->getUser(), $taskLog);

        $url = $this->generateUrl('web_site_tasks_view', ['id' => $taskLog->getTask()->getId()]);

        return $this->redirect($url . '#comments');
    }

    /**
     * @Route("/processing/{id}", name="web_site_tasks_processing", requirements={
     *     "id": "\d+"
     * })
     * @param Task $task
     * @param Request $request
     * @ParamConverter("id", class="CommonBundle:Task")
     * @Template()
     * @return array|RedirectResponse
     * @throws \Exception
     */
    public function processing(Request $request, Task $task)
    {
        $this->checkUserMeeting($task->getQuestion()->getMeeting());

        if (
            $task->getStatus() != Task::TASK_STATUS_CREATED
            || $task instanceof RepeatTask
        ) {
            throw new \Exception('Wrong status task');
        }

        /** @var  TaskManager $manager*/
        $manager = $this->get('common.manager.task_manager');
        $manager->processing($task);

        return $this->redirect($request->server->get('HTTP_REFERER'));
    }

    /**
     * @Route("/completed/{id}", name="web_site_tasks_completed")
     * @param Task $task
     * @param Request $request
     * @ParamConverter("id", class="CommonBundle:Task")
     * @Template()
     * @return array|RedirectResponse
     * @throws \Exception
     */
    public function completed(Request $request, Task $task)
    {
        $this->checkUserMeeting($task->getQuestion()->getMeeting());

        /** @var User $user */
        $user = $this->getUser();

        if (
            ($task instanceof FiniteTask &&  $task->getStatus() == Task::TASK_STATUS_APPROVED)
            || ($task instanceof RepeatTask &&  $task->getStatus() == Task::TASK_STATUS_PROCESSING)
            || $user->hasRole('ROLE_ADMIN')
        ) {

            /** @var  TaskManager $manager*/
            $manager = $this->get('common.manager.task_manager');
            $manager->completed($task);

            return $this->redirect($request->server->get('HTTP_REFERER'));

        } else {
            throw new \Exception('Wrong status task');
        }
    }

    /**
     * @Route("/testing/{id}", name="web_site_tasks_testing")
     * @param Task $task
     * @param Request $request
     * @ParamConverter("id", class="CommonBundle:Task")
     * @Template()
     * @return array|RedirectResponse
     * @throws \Exception
     */
    public function testing(Request $request, Task $task)
    {
        $this->checkUserMeeting($task->getQuestion()->getMeeting());

        if (
            $task->getStatus() != Task::TASK_STATUS_PROCESSING
            || $task instanceof RepeatTask
        ) {
            throw new \Exception('Wrong status task');
        }

        $from = $this->createForm(TaskLogCommentType::class);

        $from->handleRequest($request);

        if ($from->isSubmitted() && $from->isValid()) {

            /** @var $manager TaskManager*/
            $manager = $this->get('common.manager.task_manager');
            $manager->testing($task, $from->getData());

            $this->addFlash('notice','NOTICE_SUCCESS_TASK_TESTING');

            return $this->redirectToRoute('web_site_tasks_finite');
        }

        return [
            'task' => $task,
            'form' => $from->createView()
        ];
    }

    /**
     * @Route("/approved/{id}", name="web_site_tasks_approved")
     * @param Task $task
     * @param Request $request
     * @ParamConverter("id", class="CommonBundle:Task")
     * @Template()
     * @return array|RedirectResponse
     * @throws \Exception
     */
    public function approved(Request $request, Task $task)
    {
        $this->checkUserMeeting($task->getQuestion()->getMeeting());

        if (
            $task->getStatus() != Task::TASK_STATUS_TESTING
            || $task instanceof RepeatTask
        ) {
            throw new \Exception('Wrong status task');
        }

        $manager = $this->get('common.manager.task_manager');
        $manager->approved($task);

        return $this->redirect($request->server->get('HTTP_REFERER'));
    }

    /**
     * @Route("/edit/{id}", name="web_site_tasks_edit")
     * @param Task $task
     * @param Request $request
     * @ParamConverter("id", class="CommonBundle:Task")
     * @Template()
     * @return array|RedirectResponse
     * @throws \Exception
     */
    public function edit(Request $request, Task $task)
    {
        /** @var $manager TaskManager*/
        $manager = $this->get('common.manager.task_manager');

        $form = $this->createForm($manager->getEditType($task), $task);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $message = $form->get('message')->getData();

            $manager->save($task, $message);

            $manager->copyForUsers($task, $form->get('addUser')->getData());

            $this->addFlash('notice','NOTICE_SUCCESS');

            if ($task->getUser()->getId() == $this->getUser()->getId()) {
                if ($task->getType() == Task::TASK_TYPE_REPEAT) {

                    return $this->redirectToRoute('web_site_tasks_repeat');
                } else {

                    return $this->redirectToRoute('web_site_tasks_finite');
                }
            } else {

                return $this->redirectToRoute('web_site_tasks_all');
            }
        }

        return [
            'form' => $form->createView(),
            'task' => $task
        ];
    }


    /**
     * @Route("/to-repeat/{id}", name="web_site_tasks_to_repeat")
     * @param FiniteTask $finiteTask
     * @param Request $request
     * @ParamConverter("id", class="CommonBundle:Task")
     * @Template()
     * @return array|RedirectResponse
     * @throws \Exception
     */
    public function toRepeatAction(Request $request, FiniteTask $finiteTask)
    {
        /** @var $manager TaskManager*/
        $manager = $this->get('common.manager.task_manager');

        $form = $this->createForm(ToTaskRepeatType::class, null,[
            'finiteTask' => $finiteTask
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $manager->saveAsRepeatTask($finiteTask, $form->getData(), $form->get('message')->getData());

            $this->addFlash('notice', 'NOTICE_SUCCESS_TASK');

            return $this->redirectToRoute('web_site_tasks_repeat');
        }

        return [
            'form' => $form->createView(),
            'task' => $finiteTask
        ];
    }

    /**
     * @Route("/to-finite/{id}", name="web_site_tasks_to_finite")
     * @param RepeatTask $repeatTask
     * @param Request $request
     * @ParamConverter("id", class="CommonBundle:Task")
     * @Template()
     * @return array|RedirectResponse
     * @throws \Exception
     */
    public function toFiniteAction(Request $request, RepeatTask $repeatTask)
    {
        /** @var $manager TaskManager*/
        $manager = $this->get('common.manager.task_manager');

        $form = $this->createForm(ToTaskFiniteType::class, null,[
            'repeatTask' => $repeatTask
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $manager->saveAsFiniteTask($repeatTask, $form->getData(), $form->get('message')->getData());

            $this->addFlash('notice', 'NOTICE_SUCCESS_TASK');

            return $this->redirectToRoute('web_site_tasks_finite');
        }

        return [
            'form' => $form->createView(),
            'task' => $repeatTask
        ];
    }

    /**
     * @Route("/notifications/{id}", name="web_site_tasks_notifications")
     * @param Task $task
     * @param Request $request
     * @ParamConverter("id", class="CommonBundle:Task")
     * @Template()
     * @return array|RedirectResponse
     * @throws NotFoundHttpException
     */
    public function notifications(Request $request, Task $task)
    {
        if (
            $task->getUser()->getId() != $this->getUser()->getId()
        ) {
            throw new NotFoundHttpException();
        }

        /** @var TaskNotificationManager $manager*/
        $manager = $this->get('common.manager.task_notification_manager');

        /** @var TaskHistoryNotificationManager $notificationHistoryManager*/
        $notificationHistoryManager = $this->get('common.manager.task_notification_history_manager');

        $form = $this->createForm(TaskNotificationType::class, $task, [
            'notification_history_manager' => $notificationHistoryManager
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $manager->save($task);

            $this->addFlash('notice', 'NOTICE_SUCCESS');

            return $this->redirectToRoute('web_site_tasks_notifications', ['id' => $task->getId()]);
        }

        return [
            'task' => $task,
            'form' => $form->createView()
        ];
    }

    /**
     * @Route("/viewed/{id}", name="web_site_tasks_viewed")
     * @param Task $task
     * @ParamConverter("id", class="CommonBundle:Task")
     * @return array |RedirectResponse
     */
    public function taskViewed(Task $task)
    {
        /** @var  TaskManager $manager*/
        $manager = $this->get('common.manager.task_manager');
        $manager->viewed($task);

        return $this->redirectToRoute('web_site_tasks_view', ['id' => $task->getId()]);
    }


    /**
     * @Route("/mayor-created/{id}", name="web_site_tasks_mayor_processing")
     * @param Task $task
     * @param Request $request
     * @ParamConverter("id", class="CommonBundle:Task")
     * @Template()
     * @return array|RedirectResponse
     */
    public function mayorProcessing(Request $request, Task $task)
    {
        /** @var TaskManager $manager*/
        $manager = $this->get('common.manager.task_manager');

        $form = $this->createForm(TaskLogCommentType::class, null, [
            'action' => $this->generateUrl('web_site_tasks_mayor_processing', ['id' => $task->getId()])
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $task->setUserStatusView(false);

            $manager->processing($task, $form->getData());

            $this->addFlash('notice','NOTICE_SUCCESS');

            return $this->redirect($request->server->get('HTTP_REFERER'));
        }

        return [
            'form' => $form->createView(),
            'task' => $task
        ];
    }

    private function checkUserMeeting(Meeting $meeting)
    {
        $user = $this->getUser();
        if (!$user->hasRole('ROLE_ADMIN')) {
            $isUserMeeting = $this->get('common.manager.meeting_manager')->isMeetingUser($user, $meeting);

            if (!$isUserMeeting) {
                throw new NotFoundHttpException();
            }
        }
    }
}
