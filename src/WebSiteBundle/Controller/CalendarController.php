<?php
namespace WebSiteBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\JsonResponse;
use CommonBundle\Manager\CalendarManager;

/**
 * @Route("/calendar")
 * @Security("has_role('ROLE_USER')")
 */
class CalendarController extends Controller
{
    /**
     * @Route("/{year}", name="web_site_calendar_index", requirements={
     *     "year": "\d{4}"
     * })
     * @Template()
     */
    public function indexAction($year = null)
    {
        if (!$year) {
            $year = (new \DateTime('now'))->format('Y');
        }

        return [

            'currentYear' => $year
        ];
    }

    /**
     * @Route("/json_meetings/{year}", name="web_site_calendar_json_meetings", requirements={
     *     "year": "\d{4}"
     * })
     * @return JsonResponse
     */
    public function jsonMeetingsAction($year)
    {
        /**@var $manager CalendarManager*/
        $manager = $this->get('common.manager.calendar_manager');
        $meetings = $manager->getMeetingsByYearAndUser($year, $this->getUser());

        return new JsonResponse(array('meetings' => $meetings));
    }

    /**
     * @Route("/json_meeting_date/{date}", name="web_site_calendar_json_meeting_date", requirements={
     *     "date": "^\d{4}\-\d{2}\-\d{2}$"
     * })
     * @Template()
     */
    public function jsonMeetingDateAction($date)
    {
        $manager = $this->get('common.manager.calendar_manager');

        $meetings = $manager->getMeetingsByDateAndUser(new \DateTime($date), $this->getUser());

        return [
            'meetings' => $meetings
        ];
    }
}