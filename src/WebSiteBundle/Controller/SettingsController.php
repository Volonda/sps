<?php
namespace WebSiteBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use WebSiteBundle\Form\Type\SettingsType;
use CommonBundle\Manager\SettingsManager;
use WebSiteBundle\Form\Type\ScheduleType;
use CommonBundle\Manager\WorkCalendarManager;
use WebSiteBundle\Form\Type\WorkCalendarType\WorkCalendarType;

/**
 * @Route("/settings")
 * @Security("has_role('ROLE_USER')")
 */
class SettingsController extends Controller
{
    /**
     * @Route("", name="web_site_settings")
     * @Template()
     * @param Request $request
     * @return array
     */
    public function indexAction(Request $request)
    {
        /** @var $manager SettingsManager*/
        $manager = $this->get('common.manager.settings_manager');

        $settings = $manager->getSettings();

        $form = $this->createForm(SettingsType::class, $settings);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $manager->saveSettings($form->getData());

            $this->addFlash('notice','NOTICE_SUCCESS');
        }

        return [
            'form' => $form->createView()
        ];
    }

    /**
     * @Route("/work-calendar/{year}", name="web_site_settings_work_calendar", requirements={
     *     "year": "\d+"
     * })
     * @Template()
     * @param Request $request
     * @param integer|null $year
     * @return array
     */
    public function workCalendarAction(Request $request, $year = null)
    {
        $currentYear = ($year)
            ? $year
            : date('Y', time())
        ;

        /** @var WorkCalendarManager $manager*/
        $manager = $this->get('common.manager.work_calendar_manager');
        $calendar = $manager->getForYear($currentYear);

        $form = $this->createForm(WorkCalendarType::class, $calendar, [
            'currentYear' => $currentYear
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted()) {

            $manager->save($form->getData());

            return $this->redirectToRoute('web_site_settings_work_calendar', [
                'year' => $currentYear
            ]);
        }

        return [
            'currentYear' => $currentYear,
            'form' => $form->createView()
        ];
    }
}