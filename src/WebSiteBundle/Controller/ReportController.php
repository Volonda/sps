<?php
namespace WebSiteBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use WebSiteBundle\Filter\Filter;
use CommonBundle\Manager\ReportManager;

/**
 * @Route("/reports")
 * @Security("has_role('ROLE_ADMIN')")
 */
class ReportController extends Controller
{
    const PAGE_LIMIT = 20;

    /**
     * @Route("/tasks", name="web_site_reports")
     * @Template()
     * @param Request $request
     * @return array
     */
    public function indexAction(Request $request)
    {
        /** @var $manager ReportManager*/
        $manager = $this->get('common.manager.report_manager');

        /* @var $filter Filter */
        $filter = $this->get('web_site.filter')->getFilter($request, $manager->getListTaskFilters(), true);

        $pagination = $manager->getListTaskPaginate($request->query->get('page', 1), self::PAGE_LIMIT, $filter);

        return [
            'pagination' => $pagination,
            'filters' => $filter->getFiltersValue()
        ];
    }

    /**
     * @Route("/tasks/print", name="web_site_reports_print")
     * @Template()
     * @param Request $request
     * @return array
     */
    public function printAction(Request $request)
    {
        /** @var $manager ReportManager*/
        $manager = $this->get('common.manager.report_manager');

        /* @var $filter Filter */
        $filter = $this->get('web_site.filter')->getFilter($request, $manager->getListTaskFilters(), true);

        $pagination = $manager->getListTask($filter);

        return [
            'pagination' => $pagination,
            'filters' => $filter->getFiltersValue()
        ];
    }
}