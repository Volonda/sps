<?php
namespace WebSiteBundle\Controller;

use CommonBundle\Entity\Group;
use CommonBundle\Manager\GroupManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use WebSiteBundle\Filter\Filter;
use WebSiteBundle\Form\Type\GroupType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\JsonResponse;
use UserBundle\Entity\User;
/**
 * @Route("/groups")
 * @Security("has_role('ROLE_ADMIN')")
 */
class GroupController extends Controller
{
    const PAGE_LIMIT = 20;

    /**
     * @Route("/index", name="web_site_groups_index")
     * @Template()
     * @return array
     */
    public function indexAction(Request $request)
    {
        /** @var $manager GroupManager */
        $manager = $this->get('common.manager.group_manager');

        /* @var $filter Filter */
        $filter = $this->get('web_site.filter')->getFilter($request, $manager->getListFilters(), true);

        $pagination = $manager->getListPaginate($request->query->get('page', 1), self::PAGE_LIMIT, $filter);

        return [
            'pagination' => $pagination,
            'filters' => $filter->getFiltersValue()
        ];
    }

    /**
     * @Route("/create", name="web_site_groups_create")
     * @Route("/edit/{id}", name="web_site_groups_edit", requirements={
     *     "id": "\d+"
     * })
     * @ParamConverter("group", class="CommonBundle:Group")
     * @param  Request $request
     * @param Group|null $group
     * @Template()
     * @return array|RedirectResponse
     */
    public function create(Request $request, Group $group = null)
    {
        /** @var $manager GroupManager */
        $manager = $this->get('common.manager.group_manager');

        $form = $this->createForm(GroupType::class, $group);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {
            $manager->save($form->getData());

            $this->addFlash('notice', 'NOTICE_SUCCESS');

            return $this->redirectToRoute('web_site_groups_index');
        }

        return [
            'form' => $form->createView()
        ];
    }

    /**
     * @Route("/delete/{id}", name="web_site_groups_delete", requirements={
     *     "id": "\d+"
     * })
     * @ParamConverter("id", class="CommonBundle:Group")
     * @param Group $group
     * @return RedirectResponse
     */
    public function deleteAction(Group $group)
    {
        /** @var $manager GroupManager */
        $manager = $this->get('common.manager.group_manager');

        $this->addFlash('notice', 'NOTICE_SUCCESS');

        $manager->delete($group);

        return $this->redirectToRoute('web_site_groups_index');
    }

    /**
     * @Route("/json_get_users/{id}", name="web_site_groups_json_get_users", requirements={
     *     "id": "\d+"
     * })
     * @ParamConverter("id", class="CommonBundle:Group")
     * @param Group $group
     * @return JsonResponse
     */
    public function jsonGetUsersAction(Group $group)
    {
        $users = [];

        foreach($group->getUsers() as $user) {
            $users[] = $user->getId();
        }

        return new JsonResponse(array('users' => $users));
    }

}
