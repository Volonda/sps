<?php
namespace WebSiteBundle\Controller;

use UserBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;
use WebSiteBundle\Filter\Filter;
use CommonBundle\Manager\UserManager;

/**
 * @Route("/users")
 * @Security("has_role('ROLE_USER')")
 */
class UserController extends Controller
{
    const PAGE_LIMIT = 20;

    /**
     * @Route("/view/{id}", name="web_site_users_view", requirements={
     *     "id": "\d+"
     * })
     * @ParamConverter("id", class="UserBundle:User")
     * @param User $user
     * @Template()
     * @return array
     */
    public function viewAction(User $user)
    {
        return [
            'user' => $user
        ];
    }

    /**
     * @Route("/index", name="web_site_users_index")
     * @Template()
     * @Security("has_role('ROLE_ADMIN')")
     * @return array
     */
    public function indexAction(Request $request)
    {
        /** @var UserManager $manager*/
        $manager = $this->get('common.manager.user_manager');

        /* @var $filter Filter */
        $filter = $this->get('web_site.filter')->getFilter($request, $manager->getListFilters(), true);

        $pagination = $manager->getListPaginate($request->query->get('page', 1), self::PAGE_LIMIT, $filter);

        return [
            'pagination' => $pagination,
            'filters' => $filter->getFiltersValue()
        ];
    }

}
