<?php

namespace WebSiteBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Задачи участника
 * @Route("user-tasks")
 * @Security("has_role('ROLE_USER')")
 */
class UserTaskController extends Controller
{

    /**
     * Текущие задачи
     *
     * @Route("/current", name="web_site_user_tasks_current")
     * @Template()
     */
    public function currentAction()
    {
        return ['active_tab_route' => 'current'];
    }

    /**
     * Постоянные задачи
     *
     * @Route("/regular", name="web_site_user_tasks_regular")
     * @Template()
     */
    public function regularAction()
    {
        return ['active_tab_route' => 'regular'];
    }

    /**
     * Выполненные задачи
     *
     * @Route("/completed", name="web_site_user_tasks_completed")
     * @Template()
     */
    public function completedAction()
    {
        return ['active_tab_route' => 'completed'];
    }

}