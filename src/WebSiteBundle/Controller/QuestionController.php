<?php

namespace WebSiteBundle\Controller;

use CommonBundle\Entity\Meeting;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use CommonBundle\Entity\Question;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use WebSiteBundle\Form\Type\QuestionType;
use WebSiteBundle\Form\Type\TaskLogType;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use WebSiteBundle\Filter\Filter;
use WebSiteBundle\Form\Type\TaskType\TaskCreateType;
use CommonBundle\Manager\TaskManager;
use Symfony\Component\HttpFoundation\RedirectResponse;
use CommonBundle\Manager\UserManager;
use CommonBundle\Manager\QuestionManager;

/**
 * @Route("/questions")
 * @Security("has_role('ROLE_USER')")
 */
class QuestionController extends Controller
{
    const PAGE_LIMIT = 20;

    /**
     * @Security("has_role('ROLE_USER')")
     * @Route("/by-question", name="web_site_questions_by_question_list")
     * @param Request $request
     * @Template()
     * @return array
     */
    public function listQuestionAction(Request $request)
    {
        /** @var QuestionManager $manager*/
        $manager = $this->get('common.manager.question_manager');
        $user = $this->getUser();

        /* @var $filter Filter */
        $filter = $this->get('web_site.filter')->getFilter($request, $manager->getListByQuestionFilters(), true);

        if ($user->hasRole('ROLE_ADMIN')) {

            $pagination = $manager->getListByQuestionPaginate($request->query->get('page', 1), self::PAGE_LIMIT, $filter);

        } else {

            $pagination = $manager->getListByQuestionUserPaginate($user, $request->query->get('page', 1), self::PAGE_LIMIT, $filter);
        }

        return [
            'pagination' => $pagination,
            'filters' => $filter->getFiltersValue()
        ];
    }


    /**
     * @Security("has_role('ROLE_USER')")
     * @Route("/by-task", name="web_site_questions_by_task_list")
     * @param Request $request
     * @Template()
     * @return array
     */
    public function listTaskAction(Request $request)
    {
        /** @var QuestionManager  $manager*/
        $manager = $this->get('common.manager.question_manager');

        /* @var $filter Filter */
        $filter = $this->get('web_site.filter')->getFilter($request, $manager->getListByTaskFilters(), true);

        $pagination = $manager->getListByTaskPaginate($request->query->get('page', 1), self::PAGE_LIMIT, $filter);

        return [
            'pagination' => $pagination,
            'filters' => $filter->getFiltersValue()
        ];
    }


    /**
     * @Security("has_role('ROLE_ADMIN')")
     * @Route("/edit/{id}", name="web_site_questions_edit", requirements={
     *     "id": "\d+"
     * })
     * @Template()
     * @ParamConverter("id", class="CommonBundle:Question")
     * @param Request $request
     * @param Question $question
     * @return array|RedirectResponse
     */
    public function editAction(Request $request, Question $question)
    {
        /**@var QuestionManager $manager*/
        $manager= $this->get('common.manager.question_manager');

        /**@var UserManager $userManager*/
        $userManager = $this->get('common.manager.user_manager');

        $form = $this->createForm(QuestionType::class, $question, [
            'mayor_choices' =>  $userManager->getQuestionMayorChoices($question),
            'user_manager' =>  $userManager
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $manager->save($form->getData());

            $this->addFlash('notice', 'NOTICE_SUCCESS_QUESTION');

            return $this->redirectToRoute('web_site_meetings_questions',['id' => $question->getMeeting()->getId()]);
        }

        return [
            'question' => $question,
            'meeting' => $question->getMeeting(),
            'form' => $form->createView()
        ];
    }

    /**
     * @Security("has_role('ROLE_USER')")
     * @Route("/view/{id}", name="web_site_questions_view", requirements={
     *     "id": "\d+"
     * })
     * @Template()
     * @ParamConverter("id", class="CommonBundle:Question")
     * @param Question $question
     * @return array
     */
    public function viewAction(Question $question)
    {
        /** @var QuestionManager $manager*/
        $manager = $this->get('common.manager.question_manager');

        return [
            'question' => $question,
            'log' => $manager->getLog($question)
        ];
    }


    /**
     * @Security("has_role('ROLE_ADMIN')")
     * @Route("/create-task/{id}", name="web_site_questions_create_task", requirements={
     *     "id": "\d+"
     * })
     * @Template()
     * @ParamConverter("id", class="CommonBundle:Question")
     * @param Question $question
     * @param Request $request
     * @return array|RedirectResponse
     */
    public function createTaskAction(Request $request, Question $question)
    {
        $form = $this->createForm(TaskCreateType::class);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            /** @var TaskManager $manager*/
            $manager = $this->get('common.manager.task_manager');
            $manager->createTaskListForQuestion($form->getData(), $question, $form->get('users')->getData());

            $this->addFlash('notice', 'NOTICE_SUCCESS_TASKS_CREATED');

            return $this->redirectToRoute('web_site_questions_view', ['id' => $question->getId()]);
        }

        return [
            'form' => $form->createView()
        ];
    }
}
