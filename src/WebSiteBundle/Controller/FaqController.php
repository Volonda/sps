<?php
namespace WebSiteBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
 * @Route("/faq")
 * @Security("has_role('ROLE_USER')")
 */
class FaqController extends Controller
{
    /**
     * @Route("/task", name="web_site_faq_task", requirements={
     *     "year": "\d{4}"
     * })
     * @Template()
     */
    public function taskAction()
    {
        return [];
    }
}