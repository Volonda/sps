<?php

namespace WebSiteBundle\Controller;

use CommonBundle\Entity\Meeting;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use WebSiteBundle\Form\Type\MeetingEmailNotificationType;
use WebSiteBundle\Form\Type\MeetingType\MeetingCreateType\MeetingCreateType;
use WebSiteBundle\Form\Type\MeetingType\MeetingType;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use WebSiteBundle\Filter\Filter;
use Symfony\Component\HttpFoundation\Response;
use CommonBundle\Manager\GroupManager;
use CommonBundle\Manager\MeetingManager;
use CommonBundle\Event\MeetingNotificationEvent;
use Symfony\Component\HttpFoundation\RedirectResponse;
use CommonBundle\Manager\UserManager;
use WebSiteBundle\Form\Type\QuestionType;

/**
 * @Route("/meetings")
 * @Security("has_role('ROLE_USER')")
 */
class MeetingController extends Controller
{
    const PAGE_LIMIT = 20;

    /**
     * @Route("", name="web_site_meetings_list")
     * @Template()
     */
    public function listAction(Request $request)
    {
        /** @var MeetingManager $manager*/
        $manager = $this->get('common.manager.meeting_manager');
        $user = $this->getUser();

        /* @var $filter Filter */
        $filter = $this->get('web_site.filter')->getFilter($request, $manager->getListFilters(), true);

        if ($user->hasRole('ROLE_ADMIN')) {

            $pagination = $manager->getListPaginate($request->query->get('page', 1), self::PAGE_LIMIT, $filter);

        } else {

            $pagination = $manager->getListUserPaginate($user, $request->query->get('page', 1), self::PAGE_LIMIT, $filter);

        }

        return [
            'pagination' => $pagination,
            'filters' => $filter->getFiltersValue()
        ];
    }

    /**
     * @Route("/view/{id}", name="web_site_meetings_view", requirements={
     *     "id": "\d+"
     * })
     * @param Meeting $meeting
     * @ParamConverter("id", class="CommonBundle:Meeting")
     * @Template()
     * @return array
     */
    public function viewAction(Meeting $meeting)
    {
        return [
             'meeting' => $meeting
        ];
    }

    /**
     * @Security("has_role('ROLE_ADMIN')")
     * @Route("/create", name="web_site_meetings_create")
     * @Route("/edit/{id}", name="web_site_meetings_edit", requirements={
     *     "id": "\d+"
     * })
     * @Template()
     * @param Request $request
     * @param int $id
     * @return array|RedirectResponse
     * @throws \Exception
     */
    public function createAction(Request $request, $id = null)
    {
        /**@var MeetingManager $manager*/
        $manager = $this->get('common.manager.meeting_manager');

        /**@var $meeting Meeting*/
        $meeting = $manager->getById($id);

        if ($meeting && $meeting->getStatus() != Meeting::MEETING_STATUS_CREATED) {
            throw new \Exception('Wrong status meeting');
        }

        $form = $this->createForm(MeetingCreateType::class, $meeting, [
            'user' => $this->getUser()
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            //create Group
            if ($form->get('createUserGroup')->getData()) {

                /** @var $groupManager GroupManager*/
                $groupManager = $this->get('common.manager.group_manager');

                $groupManager->createForMembers($form->get('group')->getData(), $form->getData()->getMembers());

                $this->addFlash('notice', 'NOTICE_SUCCESS_GROUP');
            }

            //create MeetingPlace
            if ($form->get('createPlace')->getData()) {
                $this->addFlash('notice', 'NOTICE_SUCCESS_PLACE');
            }

            $manager->create($form->getData());

            $this->addFlash('notice', 'NOTICE_SUCCESS_MEEIING');

            return $this->redirectToRoute('web_site_meetings_list');
        }

        return [
            'form' => $form->createView(),
            'meeting' => $meeting,
            'type' => ($id)
                ? 'edit'
                : 'create'
        ];
    }

    /**
     * @Security("has_role('ROLE_ADMIN')")
     * @Route("/start/{id}", name="web_site_meetings_start", requirements={
     *     "id": "\d+"
     * })
     * @ParamConverter("id", class="CommonBundle:Meeting")
     * @param Meeting $meeting
     * @return array|RedirectResponse
     */
    public function startAction(Meeting $meeting)
    {
        $manager = $this->get('common.manager.meeting_manager');

        $manager->start($meeting);

        return $this->redirectToRoute('web_site_meetings_list');
    }

    /**
     * @Security("has_role('ROLE_ADMIN')")
     * @Route("/close/{id}", name="web_site_meetings_close", requirements={
     *     "id": "\d+"
     * })
     * @ParamConverter("id", class="CommonBundle:Meeting")
     * @param Meeting $meeting
     * @return array|RedirectResponse
     */
    public function closeAction(Meeting $meeting)
    {
        $manager = $this->get('common.manager.meeting_manager');

        $manager->close($meeting);

        return $this->redirectToRoute('web_site_meetings_list');
    }

    /**
     * @Route("/{id}/questions", name="web_site_meetings_questions", requirements={
     *     "id": "\d+"
     * })
     * @Template()
     * @ParamConverter("id", class="CommonBundle:Meeting")
     * @param Meeting $meeting
     * @return array
     */
    public function questionsAction(Meeting $meeting)
    {
        return [
            'meeting' => $meeting
        ];
    }

    /**
     * @Security("has_role('ROLE_USER')")
     * @Route("/{id}/members", name="web_site_meetings_view_members", requirements={
     *     "id": "\d+"
     * })
     * @Template()
     * @ParamConverter("id", class="CommonBundle:Meeting")
     * @param Meeting $meeting
     * @return array
     */
    public function viewMembersAction(Meeting $meeting)
    {
        return [
            'meeting' => $meeting
        ];
    }

    /**
     * @Security("has_role('ROLE_ADMIN')")
     * @Route("/{id}/edit/members", name="web_site_meetings_edit_members", requirements={
     *     "id": "\d+"
     * })
     * @Template()
     * @ParamConverter("id", class="CommonBundle:Meeting")
     * @param Meeting $meeting
     * @param Request $request
     * @return array
     * @throws \Exception
     */
    public function editMembersAction(Request $request, Meeting $meeting)
    {
        if ($meeting->getStatus() != Meeting::MEETING_STATUS_OPENED) {
            throw new \Exception('Wrong status meeting');
        }

        $manager = $this->get('common.manager.meeting_manager');

        $form = $this->createForm(MeetingType::class, $meeting);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $manager->save($form->getData());

            $this->addFlash('notice', 'NOTICE_SUCCESS');
        }

        return [
            'form' => $form->createView(),
            'meeting' => $meeting
        ];
    }

    /**
     * @Security("has_role('ROLE_ADMIN')")
     * @Route("/delete/{id}", name="web_site_meetings_delete", requirements={
     *     "id": "\d+"
     * })
     * @ParamConverter("id", class="CommonBundle:Meeting")
     * @param Meeting $meeting
     * @return array|RedirectResponse
     * @throws NotFoundHttpException
     */
    public function deleteAction(Meeting $meeting)
    {
        /** @var MeetingManager $manager*/
        $manager = $this->get('common.manager.meeting_manager');
        $manager->delete($meeting);

        return $this->redirectToRoute('web_site_meetings_list');
    }

    /**
     * @Route("/pdf/{id}", name="web_site_meetings_pdf", requirements={
     *     "id": "\d+"
     * })
     * @ParamConverter("id", class="CommonBundle:Meeting")
     * @param Meeting $meeting
     * @return Response
     * @throws NotFoundHttpException
     */
    public function pdf(Meeting $meeting)
    {
        $dompdf = $this->get('web_site.dompdf');

        $data = $this->render('@WebSite/Meeting/pdf/protocol.html.twig', ['meeting' => $meeting]);
        $dompdf->loadHtml($data->getContent());

        $dompdf->render();
        $dompdf->stream('meeting_' . $meeting->getId() . '', array('Attachment' => 0));

        return new Response();
    }

    /**
     * @Route("/email-notification/{id}", name="web_site_meetings_notification", requirements={
     *     "id": "\d+"
     * })
     * @ParamConverter("id", class="CommonBundle:Meeting")
     * @param Meeting $meeting
     * @param Request $request
     * @return array|RedirectResponse
     * @Template()
     * @throws NotFoundHttpException
     */
    public function emailNotificationAction(Request $request, Meeting $meeting)
    {
        /** @var MeetingManager $manager*/
        $manager = $this->get('common.manager.meeting_manager');


        $form = $this->createForm(MeetingEmailNotificationType::class, null,[
            'users' => $manager->getUsers($meeting)
        ]);


        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $this->get('event_dispatcher')
                ->dispatch(
                    'meeting_notification',
                    new MeetingNotificationEvent($meeting, $form->get('users')->getData())
                );

            $this->addFlash('notice', 'NOTICE_SUCCESS_MEETING_NOTIFICATION');

            return $this->redirectToRoute('web_site_meetings_list');
        }

        return [
            'form' => $form->createView(),
            'meeting' => $meeting
        ];
    }

    /**
     * @param Meeting $meeting
     * @throws NotFoundHttpException
    */
    private function checkUserMeeting(Meeting $meeting)
    {
        $user = $this->getUser();
        if (!$user->hasRole('ROLE_ADMIN')) {
            $isUserMeeting = $this->get('common.manager.meeting_manager')->isMeetingUser($user, $meeting);

            if (!$isUserMeeting) {
                throw new NotFoundHttpException();
            }
        }
    }


    /**
     * @Security("has_role('ROLE_ADMIN')")
     * @Route("/{meetingId}/add-open-meeting-question", name="web_site_meetings_add_open_meeting_question", requirements={
     *     "meetingId": "\d+"
     * })
     * @ParamConverter("meeting", class="CommonBundle:Meeting", options={"id" = "meetingId"})
     * @Template("WebSiteBundle:Question:edit.html.twig")
     * @param Meeting $meeting
     * @param Request $request
     * @return array|RedirectResponse
     */
    public function addOpenMeetingQuestionAction(Request $request, Meeting $meeting)
    {
        /**@var UserManager $userManager*/
        $userManager = $this->get('common.manager.user_manager');

        /**@var MeetingManager $meetingManager*/
        $meetingManager = $this->get('common.manager.meeting_manager');

        $form = $this->createForm(QuestionType::class, null, [
            'user_manager' =>  $userManager
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $meetingManager->addQuestionToOpenMeeting($meeting, $form->getData());

            $this->addFlash('notice', 'NOTICE_SUCCESS_QUESTION_ADDED');

            return $this->redirectToRoute('web_site_meetings_questions', [
                'id' => $meeting->getId()
            ]);
        }

        return [
            'form' => $form->createView(),
            'meeting' => $meeting
        ];

    }
}