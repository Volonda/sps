<?php
namespace WebSiteBundle\Controller;

use CommonBundle\Entity\File;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Route("/files")
 * @Security("has_role('ROLE_USER')")
 */
class FileController extends Controller
{
    /**
     * @Route("/download/{id}", name="web_site_files_download",  requirements={
     *     "id": "\d+"
     * }))
     * @ParamConverter("id", class="CommonBundle:File")
     * @param File $file
     * @return array
     */
    public function downloadAction(File $file)
    {
        $path = File::FILE_ROOT_DIR . '/' . $file->getPath() . '/' . $file->getName();

        $content = file_get_contents($path);

        return new Response(
            $content,
            200,
            [
                'Content-Type' => $file->getMimeType() . '; charset=utf-8',
                'Content-Disposition' => 'attachment;filename=' .  $file->getOriginalName()
            ]
        );
    }
}