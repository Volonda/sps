<?php
namespace WebSiteBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

/**
 * @Security("has_role('ROLE_USER')")
*/
class DefaultController extends Controller
{
    /**
     * @Route("/", name="web_site_index")
     */
    public function indexAction()
    {
        return $this->render('WebSiteBundle:Default:index.html.twig');
    }
}
