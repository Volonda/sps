/* Notifications */
function shownotificaitons(target) {
    $('#notifications').show();
    $('#tasks').hide();
    $('.item-bullet').addClass("bullet-active");
    $('.item-task').removeClass("notification-item-active");
    $('.item-notification').addClass("notification-item-active");
    $('.i-speaker').addClass("i-speaker-active");
}

function showtasks(target) {
    $('#tasks').show();
    $('#notifications').hide();
    $('.item-bullet').removeClass("bullet-active");
    $('.item-task').addClass("notification-item-active");
    $('.item-notification').removeClass("notification-item-active");
    $('.i-speaker').removeClass("i-speaker-active");
}

function hide(target) {
    document.getElementById(target).style.display = 'none';
    $('.item-bullet').removeClass("bullet-active");
    $('.item-task').removeClass("notification-item-active");
    $('.item-notification').removeClass("notification-item-active");
    $('.i-speaker').removeClass("i-speaker-active");
}

$(document).on('click', function(e) {
    if (!$(e.target).closest(".nav-bar-main").length) {
        $('.notification-container').hide();
        $('.item-bullet').removeClass("bullet-active");
        $('.item-task').removeClass("notification-item-active");
        $('.item-notification').removeClass("notification-item-active");
    }
    e.stopPropagation();
});


(function ($) {
    // DESCRIPTION
    // Use this function if you would like an input element to resize on focus.
    // To use just simply add the attribute "resize-width" with a value that you wish to resize the element to.

    var el;
    var original_width = 2;

    $("input[resize-width]").focus(function () {
        el = $(this);
        original_width = el.width();
        var new_width = el.attr('resize-width');

        // Resize input width.
        el.stop().animate({
            width: new_width
        }, 'fast');
    }).blur(function () {

        // Restore orginal width.
        el.stop().animate({
            width: original_width
        }, 'fast');
    });
})(jQuery);


$(document).ready(function(){
    /* BxSlider*/
    $('.bxslider').bxSlider({
        pagerCustom: '#bx-pager',
        options: 'fade'
    });

    /* File upload button */
    $('#uploadBtn').change(function(){
        $("#uploadFile").val(this.value);
    })
});

//mainPageCalendar
(function ($) {
    var $calendarElement = $("#calendar-datepicker");
    var meetingDays = $calendarElement.data('meeting-days');

    $calendarElement.datepicker({
        inline: true,
        dateFormat: 'yy-mm-dd',
        beforeShowDay: function(date) {

            if (typeof meetingDays == 'object' && meetingDays.length > 0){

                var stringDate = dateToString(date);

                var meetings = $.grep(meetingDays, function (day) {
                    return day.date == stringDate;
                });

                if (meetings.length > 0) {

                    return [
                        true,
                        'meeting-day day-' + stringDate
                    ];

                }
            }
            return [false];

        },
        onSelect: function (d, inst) {

            var $shownPopover =  $('.day-' + d)
                .children('.popover');

            var $currentDayElement =  $('.day-' + d, $calendarElement)
                .children('a');

            if (
                $shownPopover.length == 0
                || $shownPopover.attr('id') != $currentDayElement.attr('aria-describedby')
            ){
                $.ajax({
                    url:'/calendar/json_meeting_date/' + d,
                    type: 'get',
                    dataType:'html',
                    success: function(response) {
                        var $popoverElement = $('.day-' + d, $calendarElement).children();

                        $popoverElement
                            .attr('data-toggle', 'popover')
                            .attr('data-content', response)
                            .attr('data-trigger','click')
                            .attr('data-html', true)
                            .attr('data-placement', 'right')
                            .popover('show')
                            .on('shown.bs.popover', function () {

                                var $shownPopover =  $('.day-' + d)
                                    .children('.popover');

                                $('a',$shownPopover).click(function(){
                                    window.location.href = $(this).attr('href');
                                })

                            });

                    },
                    error: function() {
                        alert('Возникла ошибка');
                    }
                })
            }

        },
        minDate: new Date()
    });
})(jQuery);


function dateToString(date)
{
    var result = date.getFullYear() +
    '-' +
    (date.getMonth() < 9 ? "0" + (date.getMonth() + 1) : date.getMonth() + 1) +
    '-' +
    (date.getDate() < 10 ? "0" + (date.getDate()) : date.getDate())

    return result;
}

function  getTimeIntervals(intervalStart, intervalEnd, interval)
{
    intervalStart = intervalStart.getTime() / 1000;
    intervalEnd = intervalEnd.getTime() / 1000;

    var intervals = Math.ceil((intervalEnd - intervalStart) / interval);

    var times = [];

    for(var i = 0; i <= intervals; i ++ ) {
        var intervalDate = new Date((intervalStart + i * interval) * 1000);
        times.push(intervalDate);
    }

    return times;
}

function datePickerInit()
{
    var defaultOptions = {
        dateFormat: 'dd.mm.yy'
    };

    $('input[data-toggle="datepicker"]').each(function (num, element) {
        var options = {};

        if ($(this).data('min-date')) {
            options.minDate = new Date(Date.parse($(this).data('min-date')));
        }

        $(element)
            .datepicker($.extend(defaultOptions, options))
            .attr('type','text');
    });
}

function dateTimePickerInit()
{
    var defaultOptions = {
        format: 'd.m.Y H:i'
    };

    $('input[data-toggle="datetimepicker"]').each(function(num, element){

        var options = {};

        if ($(this).data('min-date')) {
            options.minDate = $(this).data('min-date');
        }

        if ($(this).data('max-date')) {
            options.maxDate = $(this).data('max-date');
        }

        if (
            $(this).data('time-interval')
            && $(this).data('time-interval-start')
            && $(this).data('time-interval-end')

        ) {
            var intervalStart = new Date(Date.parse($(this).data('time-interval-start')));
            var intervalEnd = new Date(Date.parse($(this).data('time-interval-end')));
            var interval = parseInt($(this).data('time-interval'));

            options.allowTimes = getTimeIntervals(intervalStart, intervalEnd, interval);
        }

        $(element).datetimepicker(
            $.extend(defaultOptions, options)
        );

    })
}

//replace date widget
(function ($) {
    datePickerInit();
    dateTimePickerInit();
})(jQuery);
