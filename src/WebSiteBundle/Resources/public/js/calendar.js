$(document).ready(function(){

    var $datapickerElement = $('#datepicker-calendar-group');
    var defaultDate = new Date();
    var showCurrentAtPos = 6;

    $.ajax({
        url: $datapickerElement.data('url'),
        success: function(response) {
            datapickerRender(response.meetings);
        },
        error:function(){
            alert('Не удается подключиться к серверу');
        }
    });


    function datapickerRender(meetings) {

        if (defaultDate.getFullYear() != $datapickerElement.data('current-year')) {
            defaultDate = new Date($datapickerElement.data('current-year'), 6, 31)
        }

        if(showCurrentAtPos  > defaultDate.getMonth()) {
            showCurrentAtPos = defaultDate.getMonth()
        }

        $datapickerElement.datepicker({
            showCurrentAtPos: showCurrentAtPos,
            numberOfMonths: 12,
            minDate: new Date($datapickerElement.data('current-year'), 0, 1),
            maxDate: new Date($datapickerElement.data('current-year'), 11, 31),
            defaultDate: defaultDate,
            onSelect: function (d, inst) {
                inst.inline = false;
            },
            beforeShowDay: function (day) {

                var dayMeetings = $.grep(meetings, function (meeting) {
                    var meetingDate = new Date(Date.parse(meeting.date));

                    return (meetingDate.getFullYear() == day.getFullYear()
                        && meetingDate.getMonth() == day.getMonth()
                        && meetingDate.getDate() == day.getDate());

                });

                if (dayMeetings.length > 0) {

                    return [true,'meeting-day'];
                }
                return [false];
            }
        });

        //datepicker width fix
        $('.ui-datepicker-multi', $datapickerElement).attr('style', null);

        //active day fix
        $('.ui-state-active', $datapickerElement).removeClass('ui-state-active');

        //popover init
        $('.meeting-day', $datapickerElement).each(function(index){
            var html = $('.ui-state-default', $(this)).html();
            $('.ui-state-default', $(this)).remove();

            $('<span>')
                .attr('class', 'ui-state-default ui-state-hover a')
                .attr('data-toggle', 'popover')
                .attr('data-content', '<div class="brace"></div>')
                .attr('data-trigger','click')
                .attr('data-html', true)
                .attr('data-placement', 'bottom')
                .html(html)
                .appendTo($(this))
            ;

        });

        $('[data-toggle="popover"]', $datapickerElement)
            .popover()
            .on('show.bs.popover', function(){

                var popover = $(this);
                var date = new Date(
                    $(this).parent().data('year'),
                    $(this).parent().data('month'),
                    $(this).parent().children('.ui-state-default').html()
                );

                var dateString = dateToString(date);

                $.ajax({
                    url:'/calendar/json_meeting_date/' + dateString,
                    type: 'get',
                    dataType:'html',
                    success: function(response) {
                        $('.popover-content',popover.parent()).html(response);
                    }
                })

            })
            .on('shown.bs.popover', function(){
                $('a', $(this).parent()).click(function(){
                    window.location = $(this).attr('href')
                })
            })
    }

})