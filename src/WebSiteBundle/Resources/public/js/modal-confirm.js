 (function($){
    $('[data-toggle="modal-confirm"]').click(function(){

        $('#confirm-modal .modal-body')
            .html($(this).data('modal-html'))

        var href = $(this).data('href');
        $('#confirm-modal')
            .on('show.bs.modal', function(){
                $('#confirm-modal .confirm').unbind().click(function(){
                    window.location.href = href;
                })
            })
            .modal('show')

        ;

        return false;
    })
})(jQuery);