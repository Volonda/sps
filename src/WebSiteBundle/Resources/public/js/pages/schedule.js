$(document).ready(function(){
    var $datapickerElement = $('#datepicker-schedule-group');
    var $changeListElement = $('#work_calendar_changes');
    var defaultDate = new Date();
    var showCurrentAtPos = 6;

    function datapickerRender(meetings) {

        if (defaultDate.getFullYear() != $datapickerElement.data('current-year')) {
            defaultDate = new Date($datapickerElement.data('current-year'), 6, 31)
        }

        if (showCurrentAtPos > defaultDate.getMonth()) {
            showCurrentAtPos = defaultDate.getMonth()
        }
        $datapickerElement.datepicker({
            showCurrentAtPos: showCurrentAtPos,
            numberOfMonths: 12,
            minDate: new Date($datapickerElement.data('current-year'), 0, 1),
            maxDate: new Date($datapickerElement.data('current-year'), 11, 31),
            defaultDate: defaultDate,
            onSelect: function (d, inst) {
                var $dayElement = getDayElement(
                    inst.selectedDay,
                    inst.selectedMonth,
                    inst.selectedYear
                );
                var date = new Date(inst.selectedYear, inst.selectedMonth, inst.selectedDay);


                if ($dayElement.hasClass('ui-datepicker-day-off')) {
                    setWorkDay(d, date)
                } else {
                    setDayOff(d, date)
                }

                $dayElement.toggleClass('ui-datepicker-day-off');

                inst.inline = false;
            },
            beforeShowDay: function (day) {
                var isDayOff = false;
                var date = '' + day.getDate();
                var month = '' + (day.getMonth() + 1);
                var year =  '' + day.getFullYear();

                var d = ((date.length < 2) ? '0' + date : date) + '.';
                d = d + ((month.length < 2) ? '0' + month : month) + '.';
                d = d + year;


                if (day.getDay()  == 6 || day.getDay() == 0) {
                    isDayOff = true
                }

                var $changes = $('input[data-group="' + d + '"]', $changeListElement);
                if ($changes.length > 0) {
                    var dayOffValue = $('input[data-group="' + d + '"].day-off', $changeListElement).val();
                    //var dayValue = $('input[data-group="' + d + '"].day', $changeListElement).val();

                    if (dayOffValue) {
                        isDayOff = true
                    } else {
                        isDayOff = false;
                    }
                }


                if (isDayOff) {
                   return [true, 'ui-datepicker-day-off']
                } else {
                    return [true];
                }
            }
        });

        //datepicker width fix
        $('.ui-datepicker-multi', $datapickerElement).attr('style', null);

        //active day fix
        $('.ui-state-active', $datapickerElement).removeClass('ui-state-active');
    }

    function getDayElement(selectedDay, selectedMonth, selectedYear)
    {
        return $('[data-month="' + selectedMonth + '"][data-year="' + selectedYear + '"]:eq('+ (selectedDay -1) +')', $datapickerElement);
    }

    function setDayOff(value, date)
    {
        $('input[data-group="' + value + '"]', $changeListElement).remove();

        if (date.getDay() != 0 && date.getDay() != 6) {
            addWidget(value, '1');
        }
    }

    function setWorkDay(value, date)
    {
        $('input[data-group="' + value + '"]',$changeListElement).remove();

        if (date.getDay() == 0 || date.getDay() == 6) {
            addWidget(value, '0');
        }

    }

    function addWidget(dayValue, dayOffValue)
    {
        var newWidget = '<input type="hidden" data-group="' + dayValue + '" value="' + dayValue + '" name="work_calendar[changes][' + dayValue.replace(/\./g,'') + '][day]" />'
            + '<input type="hidden" data-group="' + dayValue + '" value="' + dayOffValue + '" name="work_calendar[changes][' + dayValue.replace(/\./g,'') + '][dayOff]" />';

        $($changeListElement).append(newWidget);
    }

    datapickerRender();

});