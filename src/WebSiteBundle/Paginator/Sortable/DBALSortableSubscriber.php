<?php
namespace WebSiteBundle\Paginator\Sortable;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Knp\Component\Pager\Event\ItemsEvent;
use Doctrine\DBAL\Query\QueryBuilder;

class DBALSortableSubscriber implements EventSubscriberInterface
{
    public function items(ItemsEvent $event){

        if ($event->target instanceof QueryBuilder) {

            if (isset($_GET[$event->options['sortFieldParameterName']])) {
                $dir = isset($_GET[$event->options['sortDirectionParameterName']]) && strtolower($_GET[$event->options['sortDirectionParameterName']]) === 'asc' ? 'asc' : 'desc';
                $sortFields =  array_map('trim', explode(',', $_GET[$event->options['sortFieldParameterName']]));
                $fields = $event->target->getQueryPart('select');

                $explodedFields = [ ];
                foreach($fields as $field)
                {
                    $explodedFields = array_merge($explodedFields, explode(',', $field));
                }
                $explodedFields = array_map('trim', $explodedFields);

                foreach($sortFields as $key=>$sortField) {

                    $regExp = '#^((?:' . $sortField . '[ ]+(?:[aA][sS][ ]+)?[\w\.]+)|(?:[\w\.]+[ ]+[aA][sS][ ]+' . $sortField . ')|' . $sortField . ')$#';
                    if (preg_grep($regExp, $explodedFields)) {

                        if ($key == 0) {
                            $event->target->orderBy($sortField, $dir);
                        } else {
                            $event->target->addOrderBy($sortField, $dir);
                        }
                    }
                }

            }
        }
    }

    public static function getSubscribedEvents()
    {
        return array(
            'knp_pager.items' => array('items', 12)
        );
    }
}