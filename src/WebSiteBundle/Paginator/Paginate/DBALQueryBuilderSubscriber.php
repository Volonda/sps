<?php

namespace WebSiteBundle\Paginator\Paginate;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Knp\Component\Pager\Event\ItemsEvent;
use Doctrine\DBAL\Query\QueryBuilder;

class DBALQueryBuilderSubscriber implements EventSubscriberInterface
{
    public function items(ItemsEvent $event)
    {
        if ($event->target instanceof QueryBuilder) {
            /** @var $target QueryBuilder */
            $target = $event->target;

            $countQb = clone $target;
            $countQb
                ->resetQueryPart('orderBy')
                ->select('COUNT(*)')
            ;

            $event->count = $countQb
                ->execute()
                ->fetchColumn(0)
            ;

            // if there is results
            $event->items = array();
            if ($event->count) {
                $qb = clone $target;
                $qb
                    ->setFirstResult($event->getOffset())
                    ->setMaxResults($event->getLimit())
                ;

                $event->items = $qb
                    ->execute()
                    ->fetchAll()
                ;
            }

            $event->stopPropagation();
        }
    }

    public static function getSubscribedEvents()
    {
        return array(
            'knp_pager.items' => array('items', 11)
        );
    }
}