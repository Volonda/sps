<?php
namespace WebSiteBundle\Twig;

use CommonBundle\Manager\SettingsManager;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use CommonBundle\Entity\Settings;

class SettingsExtension extends \Twig_Extension
{
    /** @var SettingsManager*/
    private $manager;

    public function __construct(SettingsManager $manager)
    {
        $this->manager = $manager;
    }

    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction('settings', array($this, 'getSettings')),
        );
    }

    /**
     * @return Settings
     */
    function getSettings()
    {
        return $this->manager->getSettings();
    }

    public function getName()
    {
        return 'settings_extension';
    }
}