<?php
namespace WebSiteBundle\Twig;

use CommonBundle\Entity\Meeting;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

class WebSiteExtension extends \Twig_Extension
{
    public function getFilters()
    {
        return [
            new \Twig_SimpleFilter('bool_to_string', array($this, 'boolToString'), array('is_safe' => array('html'))),
            new \Twig_SimpleFilter(
                'apply_filter',
                array($this, 'applyFilter'),
                array(
                    'needs_environment' => true,
                    'needs_context' => true,
                )
            ),
            new \Twig_SimpleFilter('date_or_null', array($this, 'getDateOrNull'), array('needs_environment' => true))
        ];
    }

    public function boolToString($value)
    {
        if ($value) {

            return 'Да';
        } else {

            return 'Нет';
        }
    }

    public function applyFilter(\Twig_Environment $env, $context = array(), $value, $filters)
    {
        $name = 'apply_filter_' . md5($filters);

        $template = sprintf('{{ %s|%s }}', $name, $filters);

        $template = $env->createTemplate($template);

        $context[$name] = $value;

        return $template->render($context);
    }

    public function getDateOrNull(\Twig_Environment $env, $date, $format=null, $timezone = null)
    {
        if (! $date instanceof \DateTime) {
            return '';
        }
        else {
            return twig_date_format_filter($env,$date,$format,$timezone);
        }
    }

    public function getName()
    {
        return 'web_site_extension';
    }
}