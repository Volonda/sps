<?php
namespace WebSiteBundle\Twig;

use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Twig_Environment;

class BootstrapExtension extends \Twig_Extension
{
    /**
     * @param Twig_Environment $twig
    */
    private $twig;

    public function __construct(Twig_Environment $twig)
    {
        $this->twig = $twig;
    }

    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction('bootstrapDropdown', array($this, 'bootstrapDropdown'), array('is_safe' => array('html'))),
            new \Twig_SimpleFunction('addBootstrapDropdown', array($this, 'addBootstrapDropdown')),
        );
    }

    /**
     * $data [
     *   'title',
     *   'path'
     * ]
     * @param array $data
     * @param string $type
     * @return html
    */
    function bootstrapDropdown(array $data, $type = 'primary')
    {
        return $this->twig->render('WebSiteBundle:Twig:Bootstrap.html.twig', [
            'links' => $data,
            'type' => $type
        ]);
    }

    /**
     * @param array $link
     * @param array $links
     * @return array
     */
    function addBootstrapDropdown(array $link, array $links = [])
    {
        $links[] = $link;

        return $links;
    }

    public function getName()
    {
        return 'bootstrap_extension';
    }
}