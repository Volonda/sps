<?php
namespace WebSiteBundle\Twig;


class DateExtension extends \Twig_Extension
{
    public function getFilters()
    {
        return [
            new \Twig_SimpleFilter('date_intl', array($this, 'date'))
        ];
    }


    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('dateDifference', array($this, 'getDateDifference')),
            new \Twig_SimpleFunction('dateDayYear', array($this, 'getDateDayYear')),
        ];
    }

    public function getDateDifference($start, $end)
    {
        if(! $start instanceof \DateTime){
            $start = new \DateTime($start);
        }
        if(! $end instanceof \DateTime){
            $end = new \DateTime($end);
        }

        return $start->diff($end) ;
    }

    /**
     * @param \Datetime $datetime
     * @param string $pattern
     * @param string $lang
     * @return string
     */
    public function date( $datetime, $pattern = 'd. MMMM Y', $lang = 'ru_RU')
    {
        if (!$datetime instanceof \DateTime) {
            $datetime = new \DateTime($datetime);
        }

        $formatter = new \IntlDateFormatter($lang, \IntlDateFormatter::FULL, \IntlDateFormatter::FULL);
        $formatter->setPattern($pattern);
        return $formatter->format($datetime);
    }

    /**
     * @param integer $dayOfYear
     * @param integer $year
     * @return string
    */
    public function getDateDayYear($dayOfYear, $year)
    {
        if (strpos($dayOfYear, '.') === false) {
            $date = \DateTime::createFromFormat('z.Y', $dayOfYear . '.' . $year);
            return $date->format('d.m.Y');
        }

        return $dayOfYear;
    }

    public function getName()
    {
        return 'date_extension';
    }
}


