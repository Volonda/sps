<?php
namespace WebSiteBundle\Twig;

use CommonBundle\Entity\Meeting;
use CommonBundle\Manager\MeetingManager;

class MeetingExtension extends \Twig_Extension
{
    /** @var MeetingManager $meetingManager*/
    private $meetingManager;

    public function __construct(MeetingManager $meetingManager)
    {
        $this->meetingManager = $meetingManager;
    }

    /**
     * @return array
     */
    public function getFilters()
    {
        return [
            new \Twig_SimpleFilter('meeting_status_label', array($this, 'statusLabel'), array('is_safe' => array('html'))),
            new \Twig_SimpleFilter('meeting_status_name', array($this, 'getStatusName')),
            new \Twig_SimpleFilter('meeting_status_name_colored', array($this, 'getStatusNameColored'), array('is_safe' => array('html'))),
            new \Twig_SimpleFilter('meeting_status_user_name', array($this, 'getStatusUserName')),
            new \Twig_SimpleFilter('meeting_number', array($this, 'getMeetingNumber')),
            new \Twig_SimpleFilter('meeting_number_or_date', array($this, 'getMeetingNumberOrDate')),
            new \Twig_SimpleFilter('meeting_protocol_title', array($this, 'getMeetingProtocolTitle'))

        ];
    }

    /**
     * @return array
     */
    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction('meetingOrderedMembers', array($this, 'getMeetingOrderedMembers')),
        );
    }

    /**
     * @param string $value
     * @return string
     */
    public function statusLabel($value)
    {
        $pattern = '<span class="label label-%s">%s</span>';
        $statuses = Meeting::getStatuses();

        switch($value) {
            case Meeting::MEETING_STATUS_CREATED:

                return sprintf($pattern, 'primary', $statuses[Meeting::MEETING_STATUS_CREATED]);
                break;
            case Meeting::MEETING_STATUS_OPENED:

                return sprintf($pattern, 'success', $statuses[Meeting::MEETING_STATUS_OPENED]);
                break;
            case Meeting::MEETING_STATUS_CLOSED:

                return sprintf($pattern, 'default', $statuses[Meeting::MEETING_STATUS_CLOSED]);
                break;
            default:

                return $value;
                break;
        }
    }

    /**
     * @param string $status
     * @return string
     */
    public function getStatusUserName($status)
    {
        $statuses = Meeting::getUserStatuses();

        if (isset($statuses[$status])) {
            return $statuses[$status];
        }

        return $status;

    }

    /**
     * @param string $status
     * @return string
     */
    public function getStatusName($status)
    {
        $statuses = Meeting::getStatuses();
        if (isset($statuses[$status])) {
            return $statuses[$status];
        }

        return $status;
    }

    /**
     * @param Meeting $meeting
     * @return string
    */
    public function getMeetingNumberOrDate(Meeting $meeting)
    {
        if ($meeting->getNum()) {

            return '№ ' . $meeting->getNum();
        } else {

            return $meeting->getDatetimeString();
        }
    }

    /**
     * @param string $number
     * @return string
     */
    public function getMeetingNumber($number)
    {
        if (!empty($number)) {

            return  $number;
        }

        return 'б/н';
    }

    /**
     * @param string $status
     * @return string
     */
    public function getMeetingProtocolTitle($status)
    {
        if (
            $status == Meeting::MEETING_STATUS_CREATED
        ) {

            return 'Повестка совещания';
        } else {

            return 'Протокол совещания';
        }
    }

    public function getStatusNameColored($status)
    {
        $statuses = Meeting::getStatuses();
        $pattern = '<span class="meeting-status color status-%s">%s</span>';

        if (isset($statuses[$status])) {
            return sprintf($pattern, $status, $statuses[$status]);
        }

        return $status;
    }

    /**
     * @param Meeting $meeting
     * @param boolean|null $presence
     * @return array
    */
    public function getMeetingOrderedMembers(Meeting $meeting, $presence = null)
    {
        return $this->meetingManager->getOrderedMembers($meeting, $presence);
    }

    /**
     * @return string
    */
    public function getName()
    {
        return 'meeting_extension';
    }
}