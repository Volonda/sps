<?php
namespace WebSiteBundle\Twig;

use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use CommonBundle\Manager\CalendarManager;

class CalendarExtension extends \Twig_Extension
{
    /** @var TokenStorage*/
    private $tokenStorage;

    /** @var CalendarManager*/
    private $manager;

    /**
     * @param TokenStorage  $tokenStorage
     * @param CalendarManager $manager
     */
    public function __construct(TokenStorage  $tokenStorage, CalendarManager $manager)
    {
        $this->tokenStorage = $tokenStorage;
        $this->manager = $manager;
    }

    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('calendarNextMeetings', array($this, 'getCalendarNextMeetings')),
        ];
    }

    public function getCalendarNextMeetings()
    {
        $user = $this->tokenStorage->getToken()->getUser();

        return $this->manager->getNextMeetingsByUser($user);
    }

    public function getName()
    {
        return 'calendar_extension';
    }
}


