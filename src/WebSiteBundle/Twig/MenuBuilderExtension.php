<?php
namespace WebSiteBundle\Twig;

use WebsiteBundle\MenuBuilder\MenuBuilderFactory;
use WebsiteBundle\MenuBuilder\MenuBuilderInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use UserBundle\Entity\User;

class MenuBuilderExtension extends \Twig_Extension
{
    /** @var MenuBuilderFactory*/
    private $factory;

    /** @var TokenStorage*/
    private $tokenStorage;

    public function __construct(MenuBuilderFactory $factory,  TokenStorage $tokenStorage)
    {
        $this->factory = $factory;
        $this->tokenStorage = $tokenStorage;
    }

    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction('webSiteBuildMenu', array($this, 'buildMenu')),
            new \Twig_SimpleFunction('webSiteBuildLeftMenu', array($this, 'buildLeftMenu')),
        );
    }

    public function buildMenu()
    {
        /** @var User $user*/
        $user = $this->tokenStorage->getToken()->getUser();
        $menuBuilder = $this->factory->getMenuBuilder($user);

        /** @var $menuBuilder MenuBuilderInterface*/
        return $menuBuilder->buildMenu();
    }

    public function buildLeftMenu()
    {
        /** @var User $user*/
        $user = $this->tokenStorage->getToken()->getUser();
        $menuBuilder = $this->factory->getMenuBuilder($user);

        /** @var $menuBuilder MenuBuilderInterface*/
        return $menuBuilder->buildLeftMenu();
    }

    public function getName()
    {
        return 'menu_builder_extension';
    }
}