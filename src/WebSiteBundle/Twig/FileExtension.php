<?php
namespace WebSiteBundle\Twig;

use CommonBundle\Entity\File;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class FileExtension extends \Twig_Extension
{
    /** @var Router*/
    private $router;

    public  function __construct(Router $router)
    {
        $this->router = $router;
    }

    /**
     * @return array
    */
    public function getFilters()
    {
        return [
            new \Twig_SimpleFilter('file_icon_link', [$this, 'getFileIconLink'], ['is_safe' => ['html']]),
            new \Twig_SimpleFilter('file_download_link', [$this, 'getFileDownloadLink'], ['is_safe' => ['html']]),
        ];
    }

    /**
     * @param File $file
     * @return string
    */
    public function getFileIconLink(File $file)
    {

        switch($file->getMimeType())
        {
            case 'application/msword':
            case 'application/vnd.oasis.opendocument.text':
            case 'application/vnd.openxmlformats-officedocument.wordprocessingml.document':
                $iconName = 'fa-file-word-o';
                break;
            case 'application/vnd.ms-excel':
            case 'application/vnd.oasis.opendocument.spreadsheet':
                $iconName = 'fa-file-excel-o';
                break;
            case 'application/pdf':
                $iconName = 'fa-file-pdf-o';
                break;
            case 'application/zip':
            case 'application/gzip':
            case 'application/x-rar-compressed':
                $iconName = 'fa-file-archive-o';
                break;
            case 'image/jpeg':
            case 'image/png':
            case 'image/bmp':
            case 'image/pjpeg':
            case 'image/tif':
            case 'image/gif':
            case 'image/vnd.wap.wbmp':
            case 'image/svg+xml':
                $iconName = 'fa-file-image-o';
                break;
            case 'text/plain':
            case 'text/csv':
            case 'text/html':
            case 'text/xml':
                $iconName = 'fa-file-text';
                break;

            default:
                $iconName = 'fa-file';
                break;
        }

        return '<a href="' . $this->router->generate('web_site_files_download',['id' => $file->getId()]) . '">
            <i class="fa ' . $iconName . '" aria-hidden="true"></i> ' . $file->getOriginalName(). '</a>';
    }

    /**
     * @param File $file
     * @param int $absolute
     * @return string
     */
    public function getFileDownloadLink(File $file, $absolute = null)
    {
        if ($absolute){
            $url = $this->router->generate('web_site_files_download', ['id' => $file->getId()], UrlGeneratorInterface::ABSOLUTE_URL);
        } else {
            $url = $this->router->generate('web_site_files_download', ['id' => $file->getId()]);
        }
        $a = '<a href="' . $url . '">' . $file->getOriginalName(). '</a>';

        return $a;
    }

    /**
     * @return string
    */
    public function getName()
    {
        return 'file_extension';
    }
}