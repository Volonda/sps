<?php
namespace WebSiteBundle\Twig;

use CommonBundle\Entity\RepeatTask;
use CommonBundle\Entity\Task;
use UserBundle\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use CommonBundle\Manager\TaskManager;
use Doctrine\Common\Collections\ArrayCollection;
use CommonBundle\Entity\TaskPlaned;

class TaskExtension extends \Twig_Extension
{
    /** @var TaskManager*/
    private $manager;

    /**
     * @param TaskManager $manager
     */
    public function __construct(TaskManager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * @return array
     */
    public function getFilters()
    {
        return [
            new \Twig_SimpleFilter('task_status_label', array($this, 'statusLabel'), array('is_safe' => array('html'))),
            new \Twig_SimpleFilter('task_status_colored', array($this, 'statusColored'), array('is_safe' => array('html'))),
            new \Twig_SimpleFilter('task_status_name', array($this, 'getStatusName')),
            new \Twig_SimpleFilter('task_type_name', array($this, 'getTaskTypeName')),
            new \Twig_SimpleFilter('task_deadline_interval_formatted', array($this, 'getTaskDeadlineIntervalFormatted'), array('is_safe' => array('html'))),
            new \Twig_SimpleFilter('task_deadline_interval_string', array($this, 'getTaskDeadlineIntervalString')),
            new \Twig_SimpleFilter('task_repeat_period', array($this, 'getTaskRepeatPeriod'), array('is_safe' => array('html'))),
            new \Twig_SimpleFilter('task_type_icon', array($this, 'getTaskTypeIcon'), array('is_safe' => array('html'))),
        ];
    }

    /**
     * @return array
     */
    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction('getCountTaskCreatedByUser', array($this, 'getCountTaskCreatedByUser')),
            new \Twig_SimpleFunction('taskDeadlineInterval', array($this, 'getTaskDeadlineInterval')),
            new \Twig_SimpleFunction('taskPopUpNotifications', array($this, 'getTaskPopUpNotifications')),
        );
    }

    /**
     * @param string $value
     * @return string
     */
    public function statusLabel($value)
    {
        $pattern = '<span class="label label-%s">%s</span>';
        $statuses = Task::getStatuses();

        switch($value) {
            case Task::TASK_STATUS_CREATED:

                return sprintf($pattern, 'primary', $statuses[Task::TASK_STATUS_CREATED]);
                break;
            case Task::TASK_STATUS_PROCESSING:

                return sprintf($pattern, 'default', $statuses[Task::TASK_STATUS_PROCESSING]);
                break;
            case Task::TASK_STATUS_COMPLETED:

                return sprintf($pattern, 'default', $statuses[Task::TASK_STATUS_COMPLETED]);
                break;

            case Task::TASK_STATUS_APPROVED:
                return sprintf($pattern, 'success', $statuses[Task::TASK_STATUS_APPROVED]);
                break;

            case Task::TASK_STATUS_TESTING:
                return sprintf($pattern, 'warning', $statuses[Task::TASK_STATUS_TESTING]);
                break;
            default:

                return $value;
                break;
        }
    }

    /**
     * @param User $user
     * @return integer
     */
    public function getCountTaskCreatedByUser(User $user)
    {
        return $this->manager->getCountTaskCreatedByUser($user);
    }

    /**
     * @param string $status
     * @return string
     */
    public function getStatusName($status)
    {
        $statuses = Task::getStatuses();
        if (isset($statuses[$status])) {
            return $statuses[$status];
        }

        return $status;
    }

    /**
     * @param string $type
     * @return string
     */
    public function getTaskTypeName($type)
    {
        $types = Task::getTypeNames();
        if (isset($types[$type])) {
            return $types[$type];
        }

        return $type;
    }

    /**
     * @param \DateTime $deadline
     * @return \DateInterval
     */
    public function getTaskDeadlineInterval(\DateTime $deadline)
    {
        $now = new \DateTime(date('Y-m-d', time()));
        $deadline = new \DateTime($deadline->format('Y-m-d'));

        $interval = $now->diff($deadline);

        return $interval;
    }


    /**
     * @param \DateTime $deadline
     * @return string
     */
    public function getTaskDeadlineIntervalString(\DateTime $deadline)
    {
        $now = new \DateTime(date('Y-m-d', time()));
        $deadline = new \DateTime($deadline->format('Y-m-d'));

        $interval = $now->diff($deadline);

        if ($interval->days == 0) {

            return '( сегодня )';
        } else {

            return '('.(($interval->invert && $interval->days) ? ' - ' : '') .  $interval->days . ' дн.)';
        }

    }

    /**
     * @param \DateTime $deadline
     * @return string
     */
    public function getTaskDeadlineIntervalFormatted(\DateTime $deadline,  $task = null)
    {
        if ($this->idShowDeadline($task)) {
            $now = new \DateTime(date('Y-m-d', time()));
            $deadline = new \DateTime($deadline->format('Y-m-d'));

            $interval = $now->diff($deadline);

            if ($interval->days == 0) {

                return '<span class = "task-deadline-interval task-deadline-today"> ( сегодня ) </span>';
            } else {

                if ($interval->invert) {
                    $class = 'task-deadline-interval task-deadline-delay';
                } else {
                    $class = 'task-deadline-interval task-deadline-reserve';
                }

                return '<span class = "' . $class . '"> ( ' . (($interval->invert && $interval->days) ? ' - ' : '') . $interval->days . ' дн.) </span>';
            }
        }
    }

    /**
     * @param Task|array $task
     * @param string $labelType
     * @return string
     */
    public function getTaskRepeatPeriod($task, $labelType = 'icon' )
    {
        if ($task instanceof RepeatTask || $task instanceof TaskPlaned) {

            $period = $task->getPeriod();
            $periodTypeName = $task->getPeriodTypeChoiceName();

        }  elseif(is_array($task)) {

            $choices = RepeatTask::getPeriodTypeChoices();
            $period = $task['period'];

            if (isset($choices[$task['period_type']])) {
                $periodTypeName = $choices[$task['period_type']];
            }
        }

        if (isset($period) && isset($periodTypeName)) {
            if ( $labelType == 'hidden') {

                return  $period . ' ' . $periodTypeName;
            } elseif($labelType == 'icon') {

                return '<i class="fa fa-recycle" aria-hidden="true"></i> ' . $period .' ' . $periodTypeName;
            } elseif($labelType == 'string') {

                return 'повторять через ' . $period .' ' . $periodTypeName;
            }
        }

        return '';
    }


    /**
     * @param User $user
     * @return ArrayCollection
    */
    public function getTaskPopUpNotifications(User $user)
    {
        $groupedTasks = [];

        if ($user->hasRole('ROLE_ADMIN')) {

            $tasks = $this->manager->getAdminPopUpNotifications($user);
        } else {

            $tasks = $this->manager->getUserPopUpNotifications($user);
        }

        /** @var Task $task*/
        foreach($tasks as $task) {

            if ($task['admin_status_view'] === false) {
                $groupedTasks['admin'][] = $task;
            }

            if ($task['user_status_view'] === false) {
                $groupedTasks['user'][] = $task;
            }

            if ($task['mayor_status_view'] === false) {
                $groupedTasks['mayor'][] = $task;
            }
        }

        return $groupedTasks;
    }

    public function statusColored($value)
    {
        $pattern = '<span class="color-%s">%s</span>';
        $statuses = Task::getStatuses();

        switch($value) {
            case Task::TASK_STATUS_CREATED:

                return sprintf($pattern, $value, $statuses[Task::TASK_STATUS_CREATED]);
                break;
            case Task::TASK_STATUS_PROCESSING:

                return sprintf($pattern, $value, $statuses[Task::TASK_STATUS_PROCESSING]);
                break;
            case Task::TASK_STATUS_COMPLETED:

                return sprintf($pattern, $value, $statuses[Task::TASK_STATUS_COMPLETED]);
                break;

            case Task::TASK_STATUS_APPROVED:
                return sprintf($pattern, $value, $statuses[Task::TASK_STATUS_APPROVED]);
                break;

            case Task::TASK_STATUS_TESTING:
                return sprintf($pattern, $value, $statuses[Task::TASK_STATUS_TESTING]);
                break;
            default:

                return $value;
                break;
        }
    }

    public function getTaskTypeIcon($type)
    {
        switch($type){
            case Task::TASK_TYPE_FINITE:
                return '<i class="fa fa-clock-o" aria-hidden="true"></i>';
                break;
            case Task::TASK_TYPE_REPEAT:
                return '<i class="fa fa-recycle" aria-hidden="true"></i>';
                break;
        }
    }

    private function idShowDeadline($task = null) {
        $taskStatus=true;
        $taskStatusValue = null;

        if(is_string($task)){
            $taskStatusValue = $task;
        }elseif(is_array($task) && isset($task['status'])){
            $taskStatusValue = $task['status'];
        } elseif($task instanceof Task){
            $taskStatusValue = $task->getStatus();
        }

        if ($taskStatusValue && $taskStatusValue == Task::TASK_STATUS_COMPLETED) {
            $taskStatus = false;
        }

        return $taskStatus;
    }


    /**
     * @return string
    */
    public function getName()
    {
        return 'task_extension';
    }
}