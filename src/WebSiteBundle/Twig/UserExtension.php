<?php
namespace WebSiteBundle\Twig;

use UserBundle\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Bundle\FrameworkBundle\Routing\Router;

class UserExtension extends \Twig_Extension
{
    /** @var TokenStorage*/
    private $tokenStorage;

    /** @var Router*/
    private $router;

    /**
     * @param TokenStorage  $tokenStorage
     * @param Router $router
    */
    public function __construct(TokenStorage  $tokenStorage, Router $router)
    {
        $this->tokenStorage = $tokenStorage;
        $this->router = $router;
    }

    /**
     * @return array
    */
    public function getFilters()
    {
        return [
            new \Twig_SimpleFilter('user_name', [$this, 'getUserName'],['is_safe' => ['html']]),
            new \Twig_SimpleFilter('string_user_name', [$this, 'getStringUserName'], ['is_safe' => ['html']]),
            new \Twig_SimpleFilter('user_roles', [$this, 'getRoles'], ['is_safe' => ['html']]),
            new \Twig_SimpleFilter('user_group_roles', [$this, 'getGroupRoles'], ['is_safe' => ['html']]),
            new \Twig_SimpleFilter('user_photo', [$this, 'getUserPhoto'], ['is_safe' => ['html']]),
            new \Twig_SimpleFilter('user_status', [$this, 'getUserStatus'], ['is_safe' => ['html']]),
            new \Twig_SimpleFilter('user_position', [$this, 'getUserPosition'], ['is_safe' => ['html']]),

        ];
    }

    /**
     * @return array
     */
    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction('getUserNotifications', array($this, 'getUserNotifications')),
        );
    }

    /**
     * @param User $user
     * @param  bool $link
     * @return string
    */
    public function getUserName(User $user, $link = false)
    {
        $postfix = '';

        if ($this->tokenStorage->getToken()->getUser()->getId() == $user->getId()) {
            $postfix = ' (Вы)';
        }


        if ($link) {
            $result = '<a href="' . $this->router->generate('web_site_users_view', ['id' => $user->getId()]) . '">' .  $user->getName() . $postfix . '</a>';
        } else {
            $result = $user->getName() . $postfix;
        }

        return $result;
    }

    /**
     * @param string $username
     * @param int $id
     * @param bool $link
     * @return string
     */
    public function getStringUserName($username, $id, $link = false)
    {
        $postfix = '';

        if ($this->tokenStorage->getToken()->getUser()->getId() == $id) {

            $postfix = ' (Вы)';
        }

        if ($link) {
            $result = '<a href="' . $this->router->generate('web_site_users_view', ['id' => $id]) . '">' .  $username . $postfix . '</a>';
        } else {
            $result = $username . $postfix;
        }

        return $result;
    }

    /**
     * @param User $user
     * @return string
    */
    public function getRoles(User $user)
    {
        $roles = $user->getRoles();

        $rolesName = User::getRolesName();
        $userRoles = array_values(array_intersect_key($rolesName, array_flip($roles)));
        $result = implode(', ', $userRoles);

        return $result;
    }

    /**
     * @param User $user
     * @return string
     */
    public function getGroupRoles(User $user)
    {
        $rolesName = User::getRolesName();

        if (
            $user->hasRole('ROLE_ADMIN')
            || $user->hasRole('ROLE_SECRETARY')
            || $user->hasRole('ROLE_CHAIRMAN')
        ) {
            $allUserRoles = $user->getRoles();

            unset($allUserRoles[array_search('ROLE_USER', $allUserRoles)]);

            $roles = $allUserRoles;
        } else {
            $roles[] = 'ROLE_USER';
        }

        $userRoles = array_values(array_intersect_key($rolesName, array_flip($roles)));
        return implode(', ', $userRoles);
    }

    /**
     * @param User $user
     * @return string
    */
    public function getUserPhoto(User $user)
    {
        if ($user->getData() && $user->getData()->getPhoto()) {
            $photo = stream_get_contents($user->getData()->getPhoto());

            $photo = base64_encode($photo);

            return '<img src="data:image/jpeg;base64,' . $photo .'"/>';
        }

        return '';
    }

    /**
     * @param string $status
     * @return string
     */
    public function getUserStatus($status)
    {
        $statuses = User::getStatusNames();

        $pattern = '<span class="user-status %s">%s</span>';

        if (isset($statuses[$status])) {

            if ($status == User::STATUS_ENABLED) {

                return sprintf($pattern,'status-enabled', $statuses[$status]);
            } elseif ($status == User::STATUS_DISABLED) {

                return sprintf($pattern,'status-disabled', $statuses[$status]);
            }
        }

        return $status;
    }

    /**
     * @param User $user
     * @return string
    */
    public function getUserPosition(User $user)
    {
        $result =  $user->getPosition();

        if ($user->getDepartmentVisibility()){
            $result .= ' ' . $user->getDepartmentParent();
        }

        return $result;
    }

    /**
     * @return string
    */
    public function getName()
    {
        return 'user_extension';
    }
}