<?php
namespace WebSiteBundle\DomPdf;
use Dompdf\Dompdf;
use Dompdf\Options;

class DomPdfWrapper extends Dompdf
{
    public function __construct()
    {
        $options = new Options();
        $options->set('fontCache', __DIR__ . '/../../../app/dompdf');
        $options->set('fontDir',  __DIR__ . '/../../../app/dompdf');
        $options->setDefaultFont('arial');

        parent::__construct($options);
        $this->setPaper('A4', 'portrait');
    }
}