<?php
namespace WebSiteBundle\Form\Transformer;

use Symfony\Component\Form\DataTransformerInterface;
use Doctrine\Common\Collections\ArrayCollection;

class CollectionTypeTransformer  implements DataTransformerInterface
{
    public function transform($value)
    {
        if ($value) { return $value; }
        return new ArrayCollection();
    }

    public function reverseTransform($value)
    {
        return $value;
    }
}