<?php
namespace WebSiteBundle\Form\Transformer;

use Symfony\Component\Form\DataTransformerInterface;

class StringDateToDayYear  implements DataTransformerInterface
{
    /** @var integer*/
    private $currentYear;

    public function __construct($currentYear)
    {
        $this->currentYear = $currentYear;
    }

    public function transform($value)
    {
        if (!empty($value)) {
            $day = $value - 2;
            $date = \DateTime::createFromFormat('z.Y', $day . '.' . $this->currentYear);

            return $date->format('d.m.Y');
        }

        return $value;

    }

    public function reverseTransform($value)
    {
        $date = \DateTime::createFromFormat('d.m.Y', '01.01.2016');

        if(strpos('.', $value) === false) {
            $date = \DateTime::createFromFormat('d.m.Y', $value);

            return $date->format('z') + 1;
        }

        return $value;
    }
}