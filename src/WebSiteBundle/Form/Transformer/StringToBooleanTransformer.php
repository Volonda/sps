<?php
namespace WebSiteBundle\Form\Transformer;

use Symfony\Component\Form\DataTransformerInterface;

class StringToBooleanTransformer  implements DataTransformerInterface
{

    public function transform($value)
    {
        return $value;
    }

    public function reverseTransform($value)
    {
        if (
            $value == '0'
            || $value == 'false'
        ) {
            return false;
        } elseif(
            $value == '1'
            || $value == 'true'
        ) {
            return true;
        } else {
            return $value;
        }

    }
}