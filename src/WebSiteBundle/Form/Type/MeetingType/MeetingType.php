<?php
namespace WebSiteBundle\Form\Type\MeetingType;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use WebSiteBundle\Form\Type\MeetingMemberType;
class MeetingType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('members', CollectionType::class, [
            'entry_type' => MeetingMemberType::class,
            'label' => false,
            'delete_empty' => true,
            'allow_add' => true,
            'allow_delete' => true,
            'prototype' => true,
        ]);
    }

    /**
     * @param OptionsResolver $resolver
    */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'CommonBundle\Entity\Meeting',
            'validation_groups' => ['MeetingMembersEditFrom' ],
            'cascade_validation' => true
        ));
    }
}