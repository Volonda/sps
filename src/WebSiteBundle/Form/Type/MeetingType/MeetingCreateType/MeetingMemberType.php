<?php
namespace WebSiteBundle\Form\Type\MeetingType\MeetingCreateType;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use UserBundle\Repository\UserRepository;

class MeetingMemberType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('user', EntityType::class, [
            'label' => 'Участник',
            'class' => 'UserBundle\Entity\User',
            'choice_label' => 'name',
            'placeholder' => 'Не выбран',
            'attr' => [
                'class' => 'chosen',
            ],
            'query_builder' => function (UserRepository $er) {
                return $er->getEnabled();
            },

        ])->add('presence', CheckboxType::class, [
            'label' => 'Присутствие',
            'required' => false,
            'label_attr' => ['role' => 'presence', 'data-target' => '#meeting_members___name___cause'],
        ])->add('cause', TextType::class, [
            'label' => 'Причина',
            'attr' => [
                'data-validation' => 'required',
                'data-step' => 3
            ]
        ])
       ;


        $builder->addEventListener(FormEvents::POST_SET_DATA, function(FormEvent $event){
            if ($event->getData() == null) {
                $event->getForm()->get('presence')->setData(true);
            }
        });
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'CommonBundle\Entity\MeetingMember',
            'cascade_validation' => true
        ));
    }
}