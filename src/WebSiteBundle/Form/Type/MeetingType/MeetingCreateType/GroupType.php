<?php
namespace WebSiteBundle\Form\Type\MeetingType\MeetingCreateType;

use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class GroupType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('title', TextType::class, [
            'label' => 'Название',
            'attr' => [
                'data-step' => 3,
                'data-validation' => 'required'
            ]
        ])->add('description', TextareaType::class, [
            'label' => 'Описание',
            'attr' => [
                'data-step' => 3,
                'data-validation' => 'required'
            ]
        ]);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'CommonBundle\Entity\Group'
        ));
    }
}