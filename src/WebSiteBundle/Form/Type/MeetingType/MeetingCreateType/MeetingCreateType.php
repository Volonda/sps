<?php
namespace WebSiteBundle\Form\Type\MeetingType\MeetingCreateType;

use UserBundle\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Validator\Constraints\NotNull;
use CommonBundle\Validator\Constraints\ValidGroupAware;
use WebSiteBundle\Form\Transformer\CollectionTypeTransformer;
use Symfony\Component\Form\Extension\Core\DataTransformer\DateTimeToStringTransformer;
use WebSiteBundle\Form\Type\MeetingPlaceType;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Doctrine\ORM\EntityRepository;
use UserBundle\Repository\UserRepository;

class MeetingCreateType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $now = new \DateTime('now');

        $secretary = null;
        if (
            $builder->getData() &&
            $builder->getData()->getSecretary() instanceof User) {
            $secretary = $builder->getData()->getSecretary();
        } elseif($options['user'] instanceof User) {
            $secretary = $options['user'];
        }

        $builder->add('datetime', TextType::class, [
            'label' => 'Дата',
            'attr' => [
                'data-toggle' => 'datetimepicker',
                'data-time-interval' => 30 * 60,
                'data-time-interval-start' => $now->format('Y-m-d 08:00:00'),
                'data-time-interval-end' => $now->format('Y-m-d 20:59:59'),
                'data-validation' => 'required',
                'data-step' => 1
            ],
        ])
        ->add('chairman', EntityType::class,[
            'class' => 'UserBundle:User',
            'label' => 'Председатель',
            'choice_label' => 'name',
            'placeholder' => 'Не выбран',
            'attr' => [
                'class' => 'chosen',
                'data-placeholder'=> 'Председатель'
            ],
            'query_builder' => function (UserRepository $er) {
                return $er->getEnabled();
            },
        ])
        ->add('secretary', EntityType::class,[
            'class' => 'UserBundle:User',
            'label' => 'Секретарь',
            'choice_label' => 'name',
            'placeholder' => 'Не выбран',
            'data' => $secretary,
            'query_builder' => function (UserRepository $er) {
                return $er->getEnabled();
            },
            'attr' => [
                'class' => 'chosen',
                'data-placeholder'=> 'Секретарь'
            ]
        ])
        ->add('place', EntityType::class, [
            'label' => 'Место',
            'class' => 'CommonBundle\Entity\MeetingPlace',
            'choice_label' => 'title',
            'placeholder' => 'Выберете место',
            'query_builder' => function (EntityRepository $er) {
                return $er->createQueryBuilder('p')
                    ->orderBy('p.id', 'ASC');
            },
            'attr' => [
                'data-validation' => 'required',
                'data-step' => 1
            ]
        ])
        ->add('newPlace', MeetingPlaceType::class, [
            'label' => false,
            'mapped' => false,
            'constraints' => [
                new ValidGroupAware(['groups' => 'CreateMeetingPlace']),
                new NotNull(['groups' => 'CreateMeetingPlace', 'message' => 'Необходимо ввести информацию о месте']),
            ]
        ])
        ->add('createPlace', CheckboxType::class, [
            'required' => false,
            'label' => 'Создать новое место',
            'mapped' => false,
        ])
        ->add('questions', CollectionType::class, [
            'entry_type' => QuestionType::class,
            'label' => false,
            'delete_empty' => true,
            'allow_add' => true,
            'allow_delete' => true,
            'prototype' => true,
            'prototype_name' => '__question_name__'
        ])
        ->add('selectGroup', EntityType::class, [
            'required' => false,
            'class' => 'CommonBundle\Entity\Group',
            'mapped' => false,
            'choice_label' => 'title',
            'label' => 'Группа',
            'placeholder' => 'Группа не выбрана'
        ])
        ->add('createUserGroup', CheckboxType::class, [
            'required' => false,
            'mapped' => false,
            'label' => 'Создать группу для участников совещания',
        ])
        ->add('group', GroupType::class, [
            'required' => false,
            'mapped' => false,
            'label' => 'Создать группу для участников совещания',
            'error_bubbling' => false,
            'constraints' => [
                new ValidGroupAware(
                    ['groups' => 'createUserGroup']
                ),
                new NotNull(['groups' => 'createUserGroup', 'message' => 'Необходимо ввести информацию о группе']),
            ],
        ])
        ->add('members', CollectionType::class, [
            'entry_type' => MeetingMemberType::class,
            'label' => false,
            'delete_empty' => true,
            'allow_add' => true,
            'allow_delete' => true,
            'prototype' => true,
        ]);

        $builder->get('questions')->addModelTransformer(new CollectionTypeTransformer());
        $builder->get('members')->addModelTransformer(new CollectionTypeTransformer());
        $builder->get('datetime')->addModelTransformer(new DateTimeToStringTransformer(null, null, $format = 'd.m.Y H:i'));

        $builder->addEventListener(FormEvents::SUBMIT, function (FormEvent $event) {
            $meeting = $event->getData();
            $form = $event->getForm();

            if ($form->get('createPlace')->getData()) {
                $meeting->setPlace($form->get('newPlace')->getData());
                $event->setData($meeting);
            }
        });
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'CommonBundle\Entity\Meeting',
            'user' => null,
            'cascade_validation' => true,
            'validation_groups' => function(FormInterface $from){

                $groups = ['MeetingCreateForm' ];

                if ($from->get('createPlace')->getData()){
                    $groups[]= 'CreateMeetingPlace';
                }

                if ($from->get('createUserGroup')->getData()){
                    $groups[]= 'createUserGroup';
                }

                return $groups;
            }
        ));
    }
}