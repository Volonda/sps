<?php
namespace WebSiteBundle\Form\Type\MeetingType\MeetingCreateType;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use UserBundle\Repository\UserRepository;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use CommonBundle\Validator\Constraints\FileCollection;
use CommonBundle\Entity\File;

class QuestionType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('text', TextareaType::class, [
            'label' => 'Вопрос',
            'attr' => [
                'data-validation' => 'required',
                'data-step' => 2,
                'rows' => 5
            ]
        ])->add('addFiles', CollectionType::class,[
            'entry_type' => FileType::class,
            'label' => 'Файлы',
            'delete_empty' => true,
            'entry_options' => [
                'data' => null,
                'attr' => [
                    'data-validation' => 'mime size required',
                    'data-validation-max-size'=>'20M',
                    'data-validation-allowing'=>'jpg, png, gif, pdf, docx, doc, xls, xlsx',
                    'data-step' => 2
                ]
            ],
            'allow_add' => true,
            'allow_delete' => false,
            'prototype' => true,
            'prototype_name' => '__question_file_name__',
            'constraints' => [
                new FileCollection([
                    'mimeTypes' => File::getMimeTypes(),
                    'maxSize' => '10M',
                    'mimeTypesMessage' => 'Недопустимый формат файла'
                ])
            ]
        ])->add('deleteFiles', CollectionType::class,[
            'entry_type' => HiddenType::class,
            'prototype' => true,
            'allow_add' => true,
            'prototype_name' => '__question_delete_file_name__',
        ])->add('users', EntityType::class, [
            'label' => 'Докладчик(и)',
            'class' => 'UserBundle\Entity\User',
            'multiple' => true,
            'choice_label' => 'name',
            'query_builder' => function (UserRepository $er) {
               return $er->getEnabled();
            },
            'attr' => [
                'class' => 'chosen',
                'data-placeholder' => 'Докладчики'
            ],
        ])
       ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'CommonBundle\Entity\Question',
            'cascade_validation' => true,
            'validation_groups' => ['Default'],
        ));
    }
}