<?php
namespace WebSiteBundle\Form\Type;

use CommonBundle\Manager\UserManager;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use UserBundle\Repository\UserRepository;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use CommonBundle\Validator\Constraints\FileCollection;
use CommonBundle\Entity\File;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;

class QuestionType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('text', TextareaType::class, [
            'label' => 'Вопрос',
            'attr' => [
                'data-validation' => 'required',
                'rows' => 5
            ]
            ])->add('answer', TextareaType::class, [
                'label' => 'Решение',
                'required' => false,
                'attr' => [
                    'rows' => 3
                ]
            ])->add('description', TextareaType::class, [
                'label' => 'Пояснения к решению',
                'required' => false,
                'attr' => [
                    'rows' => 5
                ]
            ])->add('mayor', EntityType::class, [
                'class' => 'UserBundle\Entity\User',
                'multiple' => false,
                'label' => 'Мэр вопроса',
                'choice_label' => 'name',
                'mapped' => true,
                'placeholder' => 'Мэр не выбран',
                'choices' => $options['mayor_choices'],
                'query_builder' => function (UserRepository $er) {
                    return $er->getEnabled();
                }
            ])->add('users', EntityType::class, [
                'label' => 'Докладчик(и)',
                'required' => false,
                'class' => 'UserBundle\Entity\User',
                'multiple' => true,
                'choice_label' => 'name',
                'query_builder' => function (UserRepository $er) {
                    return $er->getEnabled();
                },
                'attr' => [
                    'class' => 'chosen',
                    'data-placeholder' => 'Докладчики'
                ],
            ])->add('tasksPlaned', CollectionType::class, [
                'entry_type' => QuestionTaskPlanedType::class,
                'label' => false,
                'delete_empty' => true,
                'allow_add' => true,
                'allow_delete' => true,
                'prototype' => true,
            ])->add('addFiles', CollectionType::class,[
                'entry_type' => FileType::class,
                'label' => false,
                'delete_empty' => true,
                'entry_options' => [
                    'data' => null
                ],
                'allow_add' => true,
                'allow_delete' => false,
                'prototype' => true,
                'constraints' => [
                    new FileCollection([
                        'mimeTypes' => File::getMimeTypes(),
                        'maxSize' => '15M',
                        'mimeTypesMessage' => 'Недопустимый формат файла'
                    ])
                ]
            ])->add('deleteFiles', CollectionType::class,[
                'entry_type' => HiddenType::class,
                'prototype' => true,
                'allow_add' => true,
                'label' => false
            ]);


        $builder->addEventListener(FormEvents::PRE_SET_DATA, function(FormEvent $event) use ($options){

            if (!empty($_POST)) {
                $users = [];
                $form = $event->getForm();

                if (isset($_POST['question']['users'])) {
                    $users = $_POST['question']['users'];
                }

                if (isset($_POST['question']['tasksPlaned'])) {
                    foreach ($_POST['question']['tasksPlaned'] as $tasksPlaned) {
                        $users = array_merge($users, $tasksPlaned['users']);
                    }
                }

                array_unique($users);

                /** @var UserManager $userManager*/
                $userManager = $options['user_manager'];

                $choiceUsers = $userManager->getById($users);

                $form->add('mayor', EntityType::class, [
                    'class' => 'UserBundle\Entity\User',
                    'multiple' => false,
                    'label' => 'Мэр вопроса',
                    'choice_label' => 'name',
                    'mapped' => true, 
                    'placeholder' => 'Мэр не выбран',
                    'choices' => $choiceUsers,
                ]);

            }
        });

    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'CommonBundle\Entity\Question',
            'validation_groups' =>  function(){
                return ['QuestionEditForm', 'TaskFiniteType'];
            },
            'mayor_choices' => [],
            'user_manager' => null
        ));
    }
}