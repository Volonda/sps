<?php
namespace WebSiteBundle\Form\Type;

use UserBundle\Entity\User;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\CallbackTransformer;


class UserType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        if ($builder->getData() && $builder->getData()->getId()) {
            $edit = true;
        } else {
            $edit = false;
        }

        if (!$edit) {
            $builder->add('username', TextType::class, [
                'label' => 'Логин'
            ]);
        }

        $builder->add('name', TextType::class,[
            'label' =>'Имя'
        ])->add('email', EmailType::class, [
            'label' => 'Email'
        ])->add('plainPassword', PasswordType::class, [
            'required' => false,
            'label' => 'Пароль'
        ])->add('enabled', CheckboxType::class, [
            'label' => 'Активен',
            'required' => false,
            'data' =>  ($edit)
                ? $builder->getData()->isEnabled()
                : true
        ])->add('roles', ChoiceType::class, [
            'label' => 'Роль',
            'choices' => array_flip(User::getRolesName()),
            'data' => ($edit)
                ? $builder->getData()->getRoles()[0]
                : null
        ]);
 
        $builder->get('roles')
            ->addModelTransformer(new CallbackTransformer(
                function ($value) {
                    return $value;
                },
                function ($value) {
                    return (array) $value;
                }
            ));
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'UserBundle\Entity\User'
        ));
    }

}