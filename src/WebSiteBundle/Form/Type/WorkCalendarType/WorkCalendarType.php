<?php
namespace WebSiteBundle\Form\Type\WorkCalendarType;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;

class WorkCalendarType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('changes', CollectionType::class, [
            'label' => false,
            'entry_type' => WorkCalendarChangeType::class,
            'entry_options' => [
               'currentYear' => $options['currentYear']
            ],
            'delete_empty' => true,
            'allow_add' => true,
            'allow_delete' => true,
            'prototype' => true,
        ])
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'CommonBundle\Entity\WorkCalendar',
            'currentYear' => null
        ));;
    }
}