<?php
namespace WebSiteBundle\Form\Type\WorkCalendarType;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use WebSiteBundle\Form\Transformer\StringDateToDayYear;
use WebSiteBundle\Form\Transformer\StringToBooleanTransformer;

class WorkCalendarChangeType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('day', HiddenType::class);
        $builder->add('dayOff', HiddenType::class);

        $builder->get('dayOff')->addModelTransformer(new StringToBooleanTransformer());
        $builder->get('day')->addModelTransformer(new StringDateToDayYear($options['currentYear']));

    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'CommonBundle\Entity\WorkCalendarChange',
            'currentYear' => null
        ));;
    }
}