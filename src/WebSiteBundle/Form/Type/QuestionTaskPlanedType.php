<?php
namespace WebSiteBundle\Form\Type;

use CommonBundle\Entity\TaskPlaned;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\DataTransformer\DateTimeToStringTransformer;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use UserBundle\Repository\UserRepository;

class QuestionTaskPlanedType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $now = new \DateTime('now');

        $builder->add('text', TextType::class, [
            'label' => 'Задача',
            'required' => true
        ])
        ->add('description', TextareaType::class, [
            'label' => 'Пояснение к задаче',
            'required' => false,
            'attr' => [
                'rows' => '5'
            ]
        ])
        ->add('users', EntityType::class, [
            'class' => 'UserBundle\Entity\User',
            'required' => false,
            'multiple' => true,
            'label' => 'Ответственый(е)',
            'choice_label' => 'name',
            'mapped' => true,
            'query_builder' => function (UserRepository $er) {
                return $er->getEnabled();
            },
            'attr' => [
                'class' => 'chosen question-users',
                'data-placeholder' => 'Ответсвенные'
            ]
        ])
        ->add('type', ChoiceType::class,[
            'label' => 'Тип',
            'required' => true,
            'choices' => array_flip(TaskPlaned::getTypeNames()),
            'expanded' => true
        ])
        ->add('deadlineDatetime', TextType::class, [
            'label' => 'Дедлайн',
            'required' => true,
            'attr' => [
                'data-toggle' => 'datetimepicker',
                'data-time-interval' => 30 * 60,
                'data-time-interval-start' => $now->format('Y-m-d 00:00:00'),
                'data-time-interval-end' => $now->format('Y-m-d 23:29:59'),
                'data-min-date' => $now->format('Y-m-d'),
            ],
        ])
        ->add('periodType', ChoiceType::class, [
            'choices' =>  array_flip(TaskPlaned::getPeriodTypeChoices()),
            'required' => false
        ])
        ->add('period', IntegerType::class, [
            'required' => false,
            'attr' => [
                'min' => 1,
                'max' => 100
            ]
        ])
        ;

        $builder->get('deadlineDatetime')->addModelTransformer(new DateTimeToStringTransformer(null, null, $format = 'd.m.Y H:i'));
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'CommonBundle\Entity\TaskPlaned',
        ));
    }
}