<?php
namespace WebSiteBundle\Form\Type\TaskNotificationType;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use CommonBundle\Validator\Constraints\UniqueFieldCollection;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TaskNotificationType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('singleNotifications', CollectionType::class, [
                'label' => false,
                'entry_type' => TaskSingleNotificationType::class,
                'delete_empty' => true,
                'allow_add' => true,
                'allow_delete' => true,
                'prototype' => true,
                'entry_options' => [
                    'notification_history_manager' => $options['notification_history_manager']
                ],
                'constraints' => [
                    new UniqueFieldCollection(['field'=>'date'])
                ]
            ]
        )->add(
            'repeatNotifications', TaskRepeatNotificationType::class, [
                'label' => false
            ]
        );
    }


    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'notification_history_manager' => null,
        ));
    }
}