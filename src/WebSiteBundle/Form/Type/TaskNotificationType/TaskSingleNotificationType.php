<?php
namespace WebSiteBundle\Form\Type\TaskNotificationType;

use CommonBundle\Manager\TaskNotificationHistoryManager;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

class TaskSingleNotificationType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $now = new \DateTime('now');
        /** @var TaskNotificationHistoryManager $notificationHistoryManager*/
        $notificationHistoryManager = $options['notification_history_manager'];

        $builder->add('date', DateType::class, [
            'label' => 'Дата',
            'widget' => 'single_text',
            'attr' => [
                'data-toggle' => 'datepicker',
                'data-min-date' => $now->format('Y-m-d')
            ],
            'format' => 'dd.MM.y'
        ]);

        $builder->addEventListener(FormEvents::POST_SET_DATA, function (FormEvent $event) use ($now, $notificationHistoryManager) {
            $data = $event->getData();
            if ($data){
                $form = $event->getForm();
                $options = $form->get('date')->getConfig()->getOptions();

                if (
                    $data->getDate() <= $now &&
                    $notificationHistoryManager->isNotified($data->getTask(), $data->getDate())
                ) {
                    $options['attr'] = [
                        'class' => 'hidden'
                    ];
                }

                $form->add('date', DateType::class, $options);
            }
        });
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'CommonBundle\Entity\TaskSingleNotification',
            'notification_history_manager' => null
        ));
    }
}