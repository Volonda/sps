<?php
namespace WebSiteBundle\Form\Type\TaskNotificationType;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class TaskRepeatNotificationType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('days', ChoiceType::class, [
            'label' => 'Дни недели',
            'choices' => [
                'пн' => 'Monday', 'вт' => 'Tuesday', 'ср' => 'Wednesday' , 'чт' => 'Thursday' , 'пт' => 'Friday', 'сб' => 'Saturday', 'вс' => 'Sunday'
            ],
            'multiple' => true,
            'expanded' => true
        ]);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'CommonBundle\Entity\TaskRepeatNotification',
        ));
    }
}