<?php
namespace WebSiteBundle\Form\Type\TaskType;

use CommonBundle\Entity\Task;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\DataTransformer\DateTimeToStringTransformer;
use Symfony\Component\Validator\Constraints\NotBlank;
use UserBundle\Repository\UserRepository;

class TaskFiniteEditType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $now = new \DateTime('now');

        $builder->add('status', ChoiceType::class, [
            'label' => 'Статус',
            'choices' => array_flip(Task::getFiniteStatuses())
        ])
        ->add('text', TextType::class, [
            'label' => 'Задача',
            'required' => false,
            'attr' => [
                'readonly' => 'readonly'
            ]
        ])
        ->add('description', TextareaType::class, [
            'label' => 'Пояснение к задаче',
            'required' => false,
            'attr' => [
                'rows' => '5'
            ]
        ])
        ->add('user', EntityType::class, [
            'class' => 'UserBundle\Entity\User',
            'multiple' => false,
            'label' => 'Ответственый(е)',
            'choice_label' => 'name',
            'attr' => [
                'class' => 'chosen',
                'data-placeholder' => ' '
            ],
            'query_builder' => function (UserRepository $er) {
                return $er->getEnabled();
            },
        ])
        ->add('addUser', EntityType::class, [
            'required' => false,
            'class' => 'UserBundle\Entity\User',
            'multiple' => true,
            'label' => 'Добавить ответственных',
            'choice_label' => 'name',
            'mapped' => false,
            'attr' => [
                'class' => 'chosen',
                'data-placeholder' => ' '
            ],
            'query_builder' => function (UserRepository $er) {
                return $er->getEnabled();
            },
        ])
        ->add('deadlineDatetime', TextType::class, [
            'label' => 'Дедлайн',
            'attr' => [
                'data-toggle' => 'datetimepicker',
                'data-time-interval' => 30 * 60,
                'data-time-interval-start' => $now->format('Y-m-d 00:00:00'),
                'data-time-interval-end' => $now->format('Y-m-d 23:29:59'),
                'data-min-date' => $now->format('Y-m-d'),
            ],
        ])->add('message', TextareaType::class, [
            'label' => 'Причина изменения',
            'mapped' => false,
            'constraints' => new NotBlank()
        ])
        ;

        $builder->get('deadlineDatetime')->addModelTransformer(new DateTimeToStringTransformer(null, null, $format = 'd.m.Y H:i'));
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'CommonBundle\Entity\FiniteTask',
            'validation_groups' => 'AdminEdit'
        ));
    }
}