<?php
namespace WebSiteBundle\Form\Type\TaskType;

use CommonBundle\Entity\Task;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use CommonBundle\Entity\FiniteTask;
use UserBundle\Repository\UserRepository;
use Symfony\Component\Validator\Constraints\NotBlank;
use CommonBundle\Entity\RepeatTask;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;

class ToTaskRepeatType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
    */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var FiniteTask $finiteTask*/
        $finiteTask = $options['finiteTask'];

        $now = new \DateTime('now');

        $builder->add('status', ChoiceType::class, [
            'label' => 'Статус',
            'choices' => array_flip(Task::getRepeatStatuses()),
            'data' => $finiteTask->getStatus()
        ])
        ->add('text', TextType::class, [
            'label' => 'Задача',
            'required' => false,
            'attr' => [
                'readonly' => 'readonly'
            ],
            'data' => $finiteTask->getText()
        ])
        ->add('description', TextareaType::class, [
            'label' => 'Пояснение к задаче',
            'required' => false,
            'attr' => [
                'rows' => '5'
            ],
            'data' => $finiteTask->getDescription()
        ])
        ->add('user', EntityType::class, [
            'class' => 'UserBundle\Entity\User',
            'multiple' => false,
            'label' => 'Ответственый(е)',
            'choice_label' => 'name',
            'attr' => [
                'class' => 'chosen',
                'data-placeholder' => ' '
            ],
            'query_builder' => function (UserRepository $er) {
                return $er->getEnabled();
            },
            'data' => $finiteTask->getUser()
        ])
        ->add('deadlineDatetime', DateTimeType::class, [
            'label' => 'Дедлайн',
            'widget' => 'single_text',
            'format' => 'dd.MM.yyyy HH:mm',
            'attr' => [
                'data-toggle' => 'datetimepicker',
                'data-time-interval' => 30 * 60,
                'data-time-interval-start' => $now->format('Y-m-d 00:00:00'),
                'data-time-interval-end' => $now->format('Y-m-d 23:29:59'),
                'data-min-date' => $now->format('Y-m-d'),
            ],
            'data' => $finiteTask->getDeadlineDatetime()
        ])
        ->add('periodType', ChoiceType::class, [
            'choices' =>  array_flip(RepeatTask::getPeriodTypeChoices()),
            'required' => false
        ])
        ->add('period', IntegerType::class, [
            'required' => false,
            'attr' => [
                'min' => 1,
                'max' => 100
            ]
        ])
        ->add('message', TextareaType::class, [
            'label' => 'Причина изменения',
            'mapped' => false,
            'constraints' => new NotBlank()
        ])
        ;
    }

    /**
     * @param OptionsResolver $resolver
    */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'CommonBundle\Entity\RepeatTask',
            'finiteTask' => null
        ));
    }
}