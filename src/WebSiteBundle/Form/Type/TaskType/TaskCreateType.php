<?php
namespace WebSiteBundle\Form\Type\TaskType;

use CommonBundle\Entity\Task;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\DataTransformer\DateTimeToStringTransformer;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Validator\Constraints\Count;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\FormInterface;
use UserBundle\Repository\UserRepository;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use CommonBundle\Entity\RepeatTask;
use Symfony\Component\Validator\Constraints\GreaterThan;

class TaskCreateType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $now = new \DateTime('now');

        $builder
        ->add('text', TextType::class, [
            'label' => 'Задача',
            'constraints' => [
                new NotBlank()
            ]
        ])
        ->add('description', TextareaType::class, [
            'label' => 'Пояснение к задаче',
            'required' => false,
            'attr' => [
                'rows' => '5'
            ]
        ])
        ->add('users', EntityType::class, [
            'class' => 'UserBundle\Entity\User',
            'multiple' => true,
            'label' => 'Ответственый(е)',
            'choice_label' => 'name',
            'attr' => [
                'data-placeholder' => 'Ответсвенные'
            ], 'required' => false,
            'constraints' => [
                new Count([
                    'min' => 1,
                    'minMessage' => 'Должен быть выбран хотябы 1 ответственный'
                ])
            ],
            'query_builder' => function (UserRepository $er) {
                return $er->getEnabled();
            },
        ])
        ->add('type', ChoiceType::class, [
            'label' => 'Тип',
            'choices' => array_flip(Task::getTypeNames()),
            'expanded' => true,
            'data' => Task::TASK_TYPE_FINITE
        ])
        ->add('periodType', ChoiceType::class, [
            'choices' =>  array_flip(RepeatTask::getPeriodTypeChoices()),
            'required' => false
        ])
        ->add('period', IntegerType::class, [
            'required' => false,
            'attr' => [
                'min' => 1,
                'max' => 100
            ]
        ])
        ->add('deadlineDatetime', TextType::class, [
            'label' => 'Дедлайн',
            'required'=> true,
            'attr' => [
                'data-toggle' => 'datetimepicker',
                'data-time-interval' => 30 * 60,
                'data-time-interval-start' => $now->format('Y-m-d 00:00:00'),
                'data-time-interval-end' => $now->format('Y-m-d 23:29:59'),
                'data-min-date' => $now->format('Y-m-d'),
            ],
            'constraints' => [
                new NotBlank(),
                new GreaterThan("+0 hours")
            ]
        ])
        ;

        $builder->get('deadlineDatetime')->addModelTransformer(new DateTimeToStringTransformer(null, null, $format = 'd.m.Y H:i'));
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'validation_groups' => function(FormInterface $form){
                $groups = ['Default'];
                if ($form->get('type')->getData() == Task::TASK_TYPE_FINITE) {
                    $groups[] = 'FiniteTaskType';
                }

                return $groups;
            }
        ));
    }
}