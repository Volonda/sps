<?php
namespace WebSiteBundle\Form\Type;

use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormView;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Validator\Constraints\Count;

class MeetingEmailNotificationType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $choiceUsers = [];
        $users = $options['users'];

        foreach($users as $user) {
            $choiceUsers[$user['name']] = $user['id'];
        }

        $data = null;
        if (!$builder->getData()) {
            foreach($choiceUsers as $choiceUser) {
                if (!$this->idEmptyUserEmail($users, $choiceUser)) {
                    $data[] = $choiceUser;
                }
            }
        } else {
            $data = $builder->getData();
        }

        $builder->add('users', ChoiceType::class, [
            'choices' => $choiceUsers,
            'expanded' => true,
            'multiple' => true,
            'data' => $data,
            'constraints' => [
                new Count(['min' => 1,'minMessage'=> 'Должен быть выбран хотя бы 1 адресат'])
            ],
            'choice_attr'  => function($val, $key, $index) use ($users) {

                if ($this->idEmptyUserEmail($users, $val)) {

                    return ['disabled' => 'disabled'];
                }

                return [];

            },
        ]);
    }

    private function idEmptyUserEmail($users, $id)
    {
        foreach($users as $user){
            if ($user['id'] == $id && empty($user['email'])) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param FormView $view
     * @param FormInterface $form
     * @param array $options
     */
    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        $view->vars['users'] = $options['users'];
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'users' => []
        ));
    }
}