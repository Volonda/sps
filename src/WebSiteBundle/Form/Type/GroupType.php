<?php
namespace WebSiteBundle\Form\Type;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use UserBundle\Repository\UserRepository;

class GroupType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('title', TextType::class, [
            'label' => 'Название группы'
        ])->add('description', TextareaType::class, [
            'label' => 'Описание группы'
        ])->add('users', EntityType::class, [
            'label' => 'Пользователи',
            'class' => 'UserBundle\Entity\User',
            'choice_label' => 'name',
            'multiple' => true,
            'attr' => ['data-placeholder' => 'Ответсвенные'],
            'query_builder' => function (UserRepository $er) {
                return $er->getEnabled();
            }
        ]);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'CommonBundle\Entity\Group'
        ));
    }
}