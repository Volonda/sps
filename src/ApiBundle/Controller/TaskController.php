<?php

namespace ApiBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\HttpFoundation\JsonResponse;

class TaskController extends Controller
{
    /**
     * @Route("/task/list/json")
     */
    public function listJsonAction()
    {
        $taskManager = $this->get('common.task');

        $encoders = array( new JsonEncoder());
        $normalizers = array(new ObjectNormalizer());

        $list = $taskManager->getList(true);


        $serializer = new Serializer($normalizers, $encoders);
        $jsonContent = $serializer->serialize($list, 'json');

        return new JsonResponse( $jsonContent );

        /*
        return $this->render('WebsiteBundle:Default:index.html.twig');*/
    }

    /**
     * @Route("/task/add")
     */
    public function addAction()
    {
        $taskManager = $this->get('common.task');


        return $this->render('ApiBundle:Default:index.html.twig');
    }


}
