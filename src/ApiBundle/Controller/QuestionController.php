<?php

namespace ApiBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\HttpFoundation\JsonResponse;

class QuestionController extends Controller
{
    /**
     * @Route("/question/list/json")
     */
    public function listJsonAction()
    {
        $questionManager = $this->get('common.question');

        $encoders = array( new JsonEncoder());
        $normalizers = array(new ObjectNormalizer());

        $list = $questionManager->getList(true);
        
        $serializer = new Serializer($normalizers, $encoders);
        $jsonContent = $serializer->serialize($list, 'json');

        return new JsonResponse( $jsonContent );

        /*
        return $this->render('WebsiteBundle:Default:index.html.twig');*/
    }

    /**
     * @Route("/question/add")
     */
    public function addAction()
    {
        $questionManager = $this->get('common.question');


        return $this->render('ApiBundle:Default:index.html.twig');
    }


}
