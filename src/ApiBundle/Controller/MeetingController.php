<?php

namespace ApiBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\HttpFoundation\JsonResponse;

class MeetingController extends Controller
{
    /**
     * @Route("/meeting/list/json")
     */
    public function listJsonAction()
    {
        $meetingManager = $this->get('common.meeting');

        $encoders = array( new JsonEncoder());
        $normalizers = array(new ObjectNormalizer());

        $list = $meetingManager->getList(true);


        $serializer = new Serializer($normalizers, $encoders);
        $jsonContent = $serializer->serialize($list, 'json');

        return new JsonResponse( $jsonContent );

        /*
        return $this->render('WebsiteBundle:Default:index.html.twig');*/
    }

    /**
     * @Route("/meeting/add")
     */
    public function addAction()
    {
        $meetingManager = $this->get('common.meeting');


        return $this->render('ApiBundle:Default:index.html.twig');
    }


}
