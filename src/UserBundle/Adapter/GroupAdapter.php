<?php
namespace UserBundle\Adapter;

class GroupAdapter
{
    private  $map;

    public function __construct(array $map = [])
    {
        $this->map = $map;
    }

    public function getMap()
    {
        return $this->map;
    }

    public function toRole($group)
    {
        $map = array_flip($this->map);

        return $map[$group];
    }

    public function toGroup($role)
    {
        return $this->map[$role];
    }

    public function getRoles()
    {
        return array_keys($this->map);
    }

    public function getGroups()
    {
        return array_values($this->map);
    }
}