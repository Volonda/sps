<?php
namespace UserBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use UserBundle\Manipulator\UserManipulator;
use UserBundle\Entity\User;
use UserBundle\Manipulator\DataManipulator;

class UserDataUpdateCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('user:data:update')
            ->setDescription('Update user data from AD ')
            ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $container = $this->getContainer();

        /** @var UserManipulator $userManipulator*/
        $userManipulator = $container->get('user.manipulator.user_manipulator');

        /** @var DataManipulator $dataManipulator*/
        $dataManipulator = $container->get('user.manipulator.data_manipulator');

        $users = $userManipulator->getUsers();

        /** @var User $user*/
        foreach($users as $user) {
            $dataManipulator->updateUser($user);
        }
    }
};