<?php
namespace UserBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use UserBundle\Manipulator\UserManipulator;
use UserBundle\Entity\User;
use UserBundle\Manipulator\RoleManipulator;

class UserRoleUpdateCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('user:role:update')
            ->setDescription('Update user roles from AD ')
            ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $container = $this->getContainer();

        /** @var UserManipulator $userManipulator*/
        $userManipulator = $container->get('user.manipulator.user_manipulator');

        /**@var RoleManipulator $roleManipulator */
        $roleManipulator = $container->get('user.manipulator.role_manipulator');

        $users = $userManipulator->getUsers();

        /** @var User $user*/
        foreach($users as $user) {

            $userRoles = $roleManipulator->getUserRoles($user);

            $userManipulator->setRoles($user, $userRoles);
        }

        $userManipulator->refresh();
    }
};