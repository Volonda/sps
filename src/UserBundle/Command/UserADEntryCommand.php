<?php
namespace UserBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use UserBundle\Ldap\LdapUserClient;

class UserADEntryCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('user:ad:entry')
            ->setDescription('Reload users from intranet')
            ->addArgument('username', InputArgument::REQUIRED, 'The username of the user.')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var LdapUserClient $userClient*/
        $userClient = $this->getContainer()->get('user.ldap_user_client');

        $entry = $userClient->getEntryByUserName($input->getArgument('username'), true);

        var_dump($entry);
    }
};