<?php
namespace UserBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use UserBundle\Manipulator\UserManipulator;

class UserUpdateCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('user:update')
            ->setDescription('Reload users from intranet')
            ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var UserManipulator $userManipulator*/
        $userManipulator = $this->getContainer()->get('user.manipulator.user_manipulator');

        $userManipulator->refresh();
    }
};