<?php
namespace UserBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('user');

        $rootNode
            ->children()
                ->arrayNode('security')
                    ->children()
                        ->arrayNode('groups')
                            ->children()
                                ->scalarNode('ROLE_SECRETARY')->end()
                                ->scalarNode('ROLE_CHAIRMAN')->end()
                                ->scalarNode('ROLE_ADMIN')->end()
                            ->end()
                        ->end()
                        ->variableNode('default_roles')->end()
                    ->end()
                ->end()
            ->end()
        ;

        return $treeBuilder;
    }
}