<?php
namespace UserBundle\DependencyInjection;

use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Symfony\Component\Config\FileLocator;

class UserExtension extends Extension
{
    public function load(array $configs, ContainerBuilder $container)
    {
        $loader = new YamlFileLoader( $container, new FileLocator( __DIR__ . '/../Resources/config' ) );
        $loader->load( 'services.yml' );

        $configuration = new Configuration();

        $config = $this->processConfiguration($configuration, $configs);

        $adapter = $container->getDefinition('user.adapter.group_adapter');

        $adapter->addArgument($config['security']['groups']);

        if(!empty($config['security']['default_roles']) && is_array($config['security']['default_roles'])) {

            $userRoles = $container->getDefinition('user.manipulator.role_manipulator');

            $userRoles->addArgument($config['security']['default_roles']);
        }

    }
}