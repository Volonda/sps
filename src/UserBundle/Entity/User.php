<?php

namespace UserBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Security\Core\User\UserInterface;
use CommonBundle\Entity\Question;
use CommonBundle\Entity\Task;
use CommonBundle\Entity\Group;

/**
 * User
 *
 * @ORM\Table(name="users")
 * @ORM\Entity(readOnly=true, repositoryClass="UserBundle\Repository\UserRepository")
 */
class User implements UserInterface
{
    const STATUS_ENABLED = 'enabled';
    const STATUS_DISABLED = 'disabled';

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="users_id_seq", allocationSize=1, initialValue=1)
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="text", nullable=true)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="username", type="text", nullable=false)
     */
    private $username;

    /**
     * @var string
     *
     * @ORM\Column(name="position", type="text", nullable=true)
     */
    private $position;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="text", nullable=false)
     */
    private $name;

    /**
     * @var Collection
     *
     * @ORM\ManyToMany(targetEntity="CommonBundle\Entity\Question", mappedBy="users", cascade={"all"})
     */
    private $questions;

    /**
     * @var Collection
     *
     * @ORM\OneToMany(targetEntity="CommonBundle\Entity\Task", mappedBy="user")
     */
    private $tasks;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="CommonBundle\Entity\MeetingMember", mappedBy="user")
     */
    private $meetings;

    /**
     * @ORM\ManyToMany(targetEntity="CommonBundle\Entity\Group", mappedBy="users")
     */
    protected $groups;

    /**
     * @var string
     *
     * @ORM\Column(name="roles", type="text", nullable=false)
     */
    private $roles;

    /**
     * @var integer
     *
     * @ORM\Column(name="status", type="user_status_type", nullable=false)
     */
    private $status;

    /**
     * @var UserData
     *
     * @ORM\OneToOne(targetEntity="UserBundle\Entity\UserData", mappedBy="user")
     */
    private $data;

    /**
     * @var string
     *
     * @ORM\Column(name="department", type="text", nullable=true)
     */
    private $department;

    /**
     * @var string
     *
     * @ORM\Column(name="department_visibility", type="boolean", nullable=false)
     */
    private $departmentVisibility;

    /**
     * @var boolean
     *
     * @ORM\Column(name="department_parent", type="text", nullable=true)
     */
    private $departmentParent;

    /**
     * @var string
     *
     * @ORM\Column(name="sort_order", type="text", nullable=true)
     */
    private $sortOrder;

    /**
     * Constructor
     */
    public function __construct()
    {  
        $this->setQuestions(new ArrayCollection());
        $this->setTasks(new ArrayCollection());
        $this->setGroups(new ArrayCollection());
        $this->setRoles([]);
        $this->setData(new UserData($this));
    }

    public static function getRolesName()
    {
        return [
            'ROLE_ADMIN' => 'Администратор',
            'ROLE_SECRETARY' => 'Секретарь',
            'ROLE_CHAIRMAN' => 'Председатель',
            'ROLE_USER' => 'Участник',
        ];
    }

    public static function getStatusNames()
    {
        return [
            self::STATUS_ENABLED => 'Активен',
            self::STATUS_DISABLED => 'Не активен'
        ];
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Set position
     *
     * @param string $position
     *
     * @return User
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position
     *
     * @return string
     */
    public function getPosition()
    {
        return $this->position;
    }


    /**
     * Add question
     *
     * @param Question $question
     *
     * @return User
     */
    public function addQuestion(Question $question)
    {
        $this->getQuestions()->add($question);

        return $this;
    }

    /**
     * Remove questions
     *
     * @param Question $question
     */
    public function removeQuestion(Question $question)
    {
        $this->getQuestions()->removeElement($question);
    }

    /**
     * Get questions
     *
     * @return Collection
     */
    public function getQuestions()
    {
        return $this->questions;
    }

    /**
     * Set questions
     * @param Collection $questions
     * @return User
     */
    public function setQuestions(Collection $questions)
    {
        $this->questions = $questions;

        return $this;
    }

    /**
     * Add task
     *
     * @param Task $task
     *
     * @return User
     */
    public function addTask(Task $task)
    {
        $this->tasks->add($task);

        return $this;
    }

    /**
     * Remove task
     *
     * @param Task $task
     */
    public function removeTask(Task $task)
    {
        $this->tasks->removeElement($task);
    }

    /**
     * Get task
     *
     * @return Collection
     */
    public function getTask()
    {
        return $this->tasks;
    }

    /**
     * @param Collection $tasks
     */
    public function setTasks(Collection $tasks)
    {
        $this->tasks = $tasks;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return ArrayCollection
     */
    public function getMeetings()
    {
        return $this->meetings;
    }

    /**
     * @param ArrayCollection $meetings
     */
    public function setMeetings($meetings)
    {
        $this->meetings = $meetings;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return ArrayCollection
     */
    public function getGroups()
    {
        return $this->groups;
    }

    /**
     * @param ArrayCollection $groups
     */
    public function setGroups(ArrayCollection $groups)
    {
        $this->groups = $groups;
    }

    /**
     * @param Group $group
     */
    public function addGroup(Group $group)
    {
        $this->getGroups()->add($group);
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param string $username
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }

    /**
     * @return array
     */
    public function getRoles()
    {
        $roles = json_decode($this->roles);

        if (!is_array($roles)) {
            $roles = [];
        }

        $roles[] = 'ROLE_USER';

        return $roles;
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param text $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return UserData
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param UserData $data
     */
    public function setData(UserData $data)
    {
        $this->data = $data;
    }

    public function eraseCredentials()
    {

    }

    /**
     * @param array $roles
     */
    public function setRoles(array $roles)
    {
        $this->roles = json_encode($roles);
    }

    public function hasRole($role)
    {
        return in_array(strtoupper($role), $this->getRoles(), true);
    }

    public function getPassword()
    {
        return null;
    }

    public function getStatusName()
    {
        $statuses = self::getStatusNames();

        if( isset($statuses[$this->getStatus()])) {

            return $statuses[$this->getStatus()];
        }

        return $this->getStatus();
    }

    public function getSalt()
    {
        return null;
    }

    /**
     * @return string
     */
    public function getDepartment()
    {
        return $this->department;
    }

    /**
     * @param string $department
     */
    public function setDepartment($department)
    {
        $this->department = $department;
    }

    /**
     * @return string
     */
    public function getDepartmentParent()
    {
        return $this->departmentParent;
    }

    /**
     * @param string $departmentParent
     */
    public function setDepartmentParent($departmentParent)
    {
        $this->departmentParent = $departmentParent;
    }

    /**
     * @return string
     */
    public function getSortOrder()
    {
        return $this->sortOrder;
    }

    /**
     * @param string $sortOrder
     */
    public function setSortOrder($sortOrder)
    {
        $this->sortOrder = $sortOrder;
    }

    /**
     * @return string
     */
    public function getDepartmentVisibility()
    {
        return $this->departmentVisibility;
    }

    /**
     * @param string $departmentVisibility
     */
    public function setDepartmentVisibility($departmentVisibility)
    {
        $this->departmentVisibility = $departmentVisibility;
    }

}