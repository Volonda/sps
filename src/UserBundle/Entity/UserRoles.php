<?php

namespace UserBundle\Entity;


use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * UserRoles
 *
 * @ORM\Table(name="user_roles")
 * @ORM\Entity()
 */
class UserRoles
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="user_roles_id_seq", allocationSize=1, initialValue=1)
     */
    protected $id;

    /**
     * @ORM\OneToOne(targetEntity="UserBundle\Entity\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;

    /**
     * @ORM\Column(name="roles", type="text", nullable=false)
    */
    private $roles;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser(User $user)
    {
        $this->user = $user;
    }

    /**
     * @return mixed
     */
    public function getRoles()
    {
        return json_decode($this->roles);
    }

    /**
     * @param mixed $roles
     */
    public function setRoles(array $roles = [])
    {
        $this->roles = json_encode($roles);
    }
}