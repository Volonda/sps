<?php
namespace UserBundle\Ldap;

use UserBundle\Entity\User;
use Symfony\Component\Ldap\Entry;
use Symfony\Component\Ldap\LdapClient;

class LdapUserClient
{
    /** @var array*/
    private $options;

    /** @var LdapClient*/
    private  $ladap;

    /**
     * @param LdapClient $ldap
     * @param array $options
     */
    public function __construct(LdapClient $ldap, array $options)
    {
        $this->ladap = $ldap;
        $this->options = $options;
    }

    /**
     * @param User $user
     * @param boolean $all
     * @return null|Entry
    */
    public function getEntryByUser(User $user, $all = false) {

       return $this->getEntryByUserName($user->getUsername(), $all);
    }

    /**
     * @param string $username
     * @param boolean $all
     * @return null|Entry
     */
    public function getEntryByUserName($username, $all = false) {

        $this->ladap->bind(
            $this->getOptions('ldap_username') . '@' . $this->getOptions('ldap_account_domain_name'),
            $this->getOptions('ldap_password')
        );

        $filter = str_replace('{username}', $username, $this->getOptions('ldap_filter'));

        $query =  $this->ladap->query($this->getOptions('ldap_base_dn'), $filter);
        $result = $query->execute();
        $data = $result->toArray();

        if ($all) {

            return $data;
        } elseif (isset($data[0])) {

            return $data[0];
        }
    }

    /**
     * @param string $name
     * @return string|null
    */
    public function getOptions($name) {
        if (isset($this->options[$name])) {

            return $this->options[$name];
        }

        return null;
    }
}