<?php
namespace UserBundle\Security;

use Symfony\Component\Security\Core\User\LdapUserProvider as BaseLdapUserProvider;
use Symfony\Component\Ldap\Entry;
use UserBundle\Entity\User;
use Symfony\Component\Ldap\LdapInterface;
use UserBundle\Manipulator\UserManipulator;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;

class LdapUserProvider extends BaseLdapUserProvider
{
    /** @var UserManipulator*/
    private $userManipulator;

    public function __construct(
        UserManipulator $userManipulator,
        LdapInterface $ldap,
        $baseDn,
        $searchDn,
        $searchPassword,
        array $defaultRoles,
        $uidKey = 'sAMAccountName',
        $filter = '({uid_key}={username})',
        $passwordAttribute = null
    ) {
        $this->userManipulator = $userManipulator;

        parent::__construct($ldap, $baseDn, $searchDn, $searchPassword, $defaultRoles, $uidKey, $filter, $passwordAttribute);
    }

    public function supportsClass($class)
    {
        return $class === 'UserBundle\Entity\User';
    }

    public function refreshUser(UserInterface $user)
    {
        if (!$user instanceof User) {
            throw new UnsupportedUserException(sprintf('Instances of "%s" are not supported.', get_class($user)));
        }

        $user = $this->userManipulator->getByUsername($user->getUsername());

        return $user;
    }

    /**
     * @param string $username
     * @param Entry $entry
     * @return User
    */
    protected function loadUser($username, Entry $entry)
    {
        $user = $this->userManipulator->getByUsername($username);

        return $user;
    }
}