<?php
namespace UserBundle\Security;

use UserBundle\Entity\User;
use UserBundle\Manipulator\UserManipulator;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;

class RemoteUserProvider implements UserProviderInterface
{
    /** @var UserManipulator*/
    private $userManipulator;

    public function __construct(UserManipulator $userManipulator) {
        $this->userManipulator = $userManipulator;
    }

    public function loadUserByUsername($username)
    {
        $username = explode('@',$username);
        $username = current($username);

        $user = $this->userManipulator->getByUsername($username);

        if (!$user) {
            throw new UsernameNotFoundException(
                sprintf('Username "%s" does not exist.', $username)
            );
        }

        return $user;
    }

    public function refreshUser(UserInterface $user)
    {
        if (!$user instanceof User) {
            throw new UnsupportedUserException(
                sprintf('Instances of "%s" are not supported.', get_class($user))
            );
        }

        return $this->loadUserByUsername($user->getUsername());
    }

    public function supportsClass($class)
    {
        return $class === 'UserBundle\Entity\User';
    }
}