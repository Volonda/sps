<?php
namespace UserBundle\Manipulator;

use Symfony\Component\Ldap\Entry;
use UserBundle\Entity\User;
use UserBundle\Entity\UserData;
use UserBundle\Ldap\LdapUserClient;
use Doctrine\ORM\EntityManager;

class DataManipulator
{
    /** @var EntityManager*/
    private $em;

    /** @var LdapUserClient $ldapUserClient*/
    private $ldapUserClient;

    /**
     * @param LdapUserClient $ldapUserClient
     * @param EntityManager $em
     */
    public function __construct(LdapUserClient $ldapUserClient, EntityManager $em)
    {
        $this->ldapUserClient = $ldapUserClient;
        $this->em = $em;
    }

    /**
     * @param User $user
     * @return array
     */
    public function updateUser(User $user)
    {
        $entry = $this->ldapUserClient->getEntryByUser($user);

        // Roles from AD
        if ($entry instanceof Entry) {
            $useData = $user->getData();

            if(!$useData){
                $useData = new UserData($user);
            }

            $photos = $entry->getAttribute('thumbnailphoto');
            if (isset($photos[0]) && !empty(isset($photos[0]))) {
                $useData->setPhoto($photos[0]);
            }

            $position = $entry->getAttribute('title');
            if (isset($position[0]) && !empty($position[0])) {
                $useData->setPosition($position[0]);
            }

            $this->em->persist($useData);
            $this->em->flush($useData);
        }

    }

}