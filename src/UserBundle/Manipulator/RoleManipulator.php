<?php
namespace UserBundle\Manipulator;

use Symfony\Component\Ldap\Entry;
use UserBundle\Entity\User;
use UserBundle\Ldap\LdapUserClient;
use UserBundle\Adapter\GroupAdapter;

class RoleManipulator
{
    /** @vae array*/
    private $defaultUserRoles;

    /** @var GroupAdapter*/
    private $adapter;

    /** @var LdapUserClient $ldapUserClient*/
    private $ldapUserClient;

    /**
     * @param LdapUserClient $ldapUserClient
     * @param array $defaultUserRoles
     * @param GroupAdapter $adapter
    */
    public function __construct(LdapUserClient $ldapUserClient, GroupAdapter $adapter, array $defaultUserRoles = [])
    {
        $this->ldapUserClient = $ldapUserClient;
        $this->adapter = $adapter;
        $this->defaultUserRoles = $defaultUserRoles;
    }

    /**
     * @param User $user
     * @return array
    */
    public function getUserDefaultRoles(User $user)
    {
        $username = $user->getUsername();

        if(
            isset($this->defaultUserRoles[$username])
            && ! empty($this->defaultUserRoles[$username])
            && is_array($this->defaultUserRoles[$username])
        ) {
            return $this->defaultUserRoles[$username];
        }

        return [];
    }

    /**
     * @param User $user
     * @return array
     */
    public function getUserADRoles(User $user)
    {
        $userRoles = [];

        $entry = $this->ldapUserClient->getEntryByUser($user);

        // Roles from AD
        if ($entry instanceof Entry) {
            $memberOf = $entry->getAttribute('memberof');

            if (is_array($memberOf) && !empty($memberOf)) {

                foreach($this->adapter->getMap() as $role => $group){
                    if (count(preg_grep('/' . $group . '/', $memberOf)) > 0) {
                        $userRoles[] = $role;
                    };
                }
            }
        }

        return $userRoles;
    }

    /**
     * @param User $user
     * @return array
    */
    public function getUserRoles(User $user)
    {
        $userAdRoles = $this->getUserADRoles($user);
        $userDefaultRoles = $this->getUserDefaultRoles($user);

        return array_merge($userAdRoles, $userDefaultRoles);
    }
}