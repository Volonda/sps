<?php
namespace UserBundle\Manipulator;

use Doctrine\ORM\EntityManager;
use UserBundle\Entity\User;
use UserBundle\Entity\UserRoles;

class UserManipulator
{
    /** @var EntityManager*/
    private $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * @param string $username
     * @return User
    */
    public function getByUsername($username)
    {
        $query = $this->em->createQueryBuilder()
            ->select('u')
            ->from('UserBundle:User', 'u')
            ->where('LOWER(u.username) = :username')
            ->setParameter('username', strtolower($username))
        ;

        return $query->getQuery()->getOneOrNullResult();
    }

    public function getEnabledByUsername($username)
    {
        $query = $this->em->createQueryBuilder()
            ->select('u')
            ->from('UserBundle:User', 'u')
            ->where('u.status = :status_enabled')
            ->andWhere('LOWER(u.username) = :username')
            ->setParameter('status_enabled', User::STATUS_ENABLED)
            ->setParameter('username', strtolower($username))
            ;

        return $query->getQuery()->getOneOrNullResult();
    }

    public function getUsers()
    {
        return $this->em->getRepository('UserBundle:User')->findAll();
    }

    public function setRoles(User $user, $roles)
    {
        $userRoles = $this->em->getRepository('UserBundle:UserRoles')->findOneBy(['user' => $user]);

        if(!$userRoles) {
            $userRoles = new UserRoles();
            $userRoles->setUser($user);
        }

        $userRoles->setRoles($roles);

        $this->em->persist($userRoles);
        $this->em->flush($userRoles);
    }

    public function refresh()
    {
        $this->em->getConnection()->query('REFRESH MATERIALIZED VIEW users')->execute();
    }
}