#!/bin/bash
set -e

dbPasswrd=$(grep -e "database_password:" app/config/parameters.yml |awk '{print $2}')
dbUser=$(grep "database_user:" app/config/parameters.yml |awk '{print $2}')
dbName=$(grep "database_name:" app/config/parameters.yml |awk '{print $2}')

intranetDbHost=$(grep "mysql_intranet_db_host:" app/config/parameters.yml |awk '{print $2}')
intranetDbPort=$(grep "mysql_intranet_db_port:" app/config/parameters.yml |awk '{print $2}')
intranetDbName=$(grep "mysql_intranet_db_name:" app/config/parameters.yml |awk '{print $2}')
intranetDbUser=$(grep "mysql_intranet_db_user:" app/config/parameters.yml |awk '{print $2}')
intranetDbPassword=$(grep "mysql_intranet_db_password:" app/config/parameters.yml |awk '{print $2}')

set PGPASSWORD=$dbPasswrd
cat app/sql/before_schema_created.sql app/sql/schema.sql app/sql/after_schema_created.sql | psql -U $dbUser -d $dbName -1 \
-v intranetDbPassword="'$intranetDbPassword'" \
-v intranetDbName="'$intranetDbName'" \
-v intranetDbUser="'$intranetDbUser'" \
-v intranetDbHost="'$intranetDbHost'" \
-v intranetDbPort="'$intranetDbPort'" \
-v dbUser="$dbUser" \
-f -

bin/console doctrine:migration:migrate --no-interaction 
#bin/console doctrine:fixtures:load --no-interaction --append
bin/console doctrine:migration:migrate --no-interaction
bin/console user:role:update
bin/console user:data:update

bin/console cache:clear --env=prod


